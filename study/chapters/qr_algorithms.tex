\section{Algorithms for QR factorization}

\subsection{Householder reflectors}

%TODO: reflection
A \textbf{Householder reflector} of a vector $x\in\R^{n}$ is an orthogonal matrix $Q\in\R^{n,n}$ that, when applied on $x$, sets all but the first entry of $x$ to zero. We say that the reflector \textbf{eliminates} or \textbf{zeroes out} those entries.

Note that while there are many equivalent definitions of a Householder reflector, the definition presented here is consistent with the LAPACK routine documentation.

%When using \textbf{Householder reflectors} to construct the QR factorization of a matrix $A$, 
%reflectionIn order to eliminate entries below the main diagonal in A using orthogonal transformations only. %TODO

\begin{definition}\label{def-householder-reflector}
	Let $x\in\R^n$ be a nonzero vector. We define a vector $v\in\R^n$ such that $v_1 = 1$ and $v_k = \frac{x_k}{x_1 + sgn(x_1)|x|}$ for all $k\in\lbrace 2, \dots, n\rbrace$.
	A \textbf{Householder reflector} (or an \textbf{elementary reflector}) of $x$ is then a matrix $Q = I - \tau v v^T$, where $\tau = \frac{2}{|v|^2}$ and $I$ is the identity matrix of order $n$. 
\end{definition}

We also note that $v_k$ for $k>1$ may be set to $\frac{x_k}{x_1 + |x|}$ or $\frac{x_k}{x_1 - |x|}$ and the Householder reflector $Q$ would still hold all properties described in the rest of this section.
In order to achieve better numerical stability, we typically select the vector $v$ with the greater magnitude, which is precisely the vector $v$ presented in Definition \ref{def-householder-reflector} \cite{LA2}.

We can trivially see that every Householder reflector is symmetric.

\begin{theorem}\label{theorem-householder-reflector-orthogonal}
	Let $Q$ be a Householder reflector of $x$. Then $Q$ is orthogonal.
\end{theorem}

\begin{proof}
	Using the symmetry of $Q$, we have $QQ^T = Q^2 = Q^TQ$. Then:
	\begin{align*}
		Q^2 &= (I-\tau v v^T)^2 = I^2 - 2\cdot I\cdot \tau v v^T + (\tau v v^T)^2 = I - 2\tau v v^T + \tau^2 v (v^T v) v^T = \\
		&= I - 2\tau v v^T + \tau^2 |v^2|v v^T = I - 2\tau v v^T + \frac{2}{|v|^2}\tau |v^2|v v^T =
		I - 2\tau v v^T + 2\tau v v^T = I
	\end{align*}
\end{proof}

\begin{theorem}\label{theorem-householder-reflector-main-property}
	Let $Q$ be a Householder reflector of $x$. Then
	$$(Qx)_k = \begin{cases}
		|x| &\text{if }k = 1,\\
		0 &\text{if }k\in\lbrace 2, \dots, n\rbrace.
	\end{cases}$$
\end{theorem}

%TODO: make "Proof." bold
\begin{proof}[Proof (inspired by \cite{Householder})]

	We denote the $k$th row of matrix $A$ as $A_{k,:}$ and the $k$th column of $A$ as $A_{:,k}$. By substituting $Q$ according to its definition, we obtain
	\begin{align*}
		(Qx)_k = Q_{k:}x = (I - \tau v v^T)_{k,:}x = x_k - \tau (v v^T)_{k,:} x = x_k - \tau v_k v^T x
	\end{align*}

	For $k\in\lbrace 2, \dots, n\rbrace$, we then have
	\begin{align*}
		(Qx)_k &= x_k - \tau v_k v^T x = x_k - \tau\frac{x_k}{x_1 + sgn(x_1)|x|} v^T x = x_k - \tau\frac{x_k}{x_1 + sgn(x_1)|x|} \sum_{l=1}^{n} v_l x_l = \\
		&= x_k - \tau\frac{x_k}{x_1 + sgn(x_1)|x|} \Big(v_1 x_1 + \sum_{l=2}^{n} v_l x_l\Big) =\\
		&= x_k - \tau\frac{x_k}{x_1 + sgn(x_1)|x|} \Big(x_1 + \sum_{l=2}^{n} \frac{x_l^2}{x_1 + sgn(x_1)|x|}\Big) = \\
		&= x_k - \tau\frac{x_k}{x_1 + sgn(x_1)|x|} \cdot\frac{x_1(x_1 + sgn(x_1)|x|) + \sum_{l=2}^{n} x_l^2}{x_1 + sgn(x_1)|x|} = \\
		&= x_k - \tau\frac{x_k}{x_1 + sgn(x_1)|x|} \cdot\frac{x_1sgn(x_1)|x| + \sum_{l=1}^{n} x_l^2}{x_1 + sgn(x_1)|x|} = \\
		&= x_k - \tau\frac{x_k}{x_1 + sgn(x_1)|x|} \cdot\frac{x_1^2 + 2x_1sgn(x_1)|x| + sgn(x_1)^2|x|^2 + \sum_{l=2}^{n} x_l^2}{2\big(x_1 + sgn(x_1)|x|\big)} = \\
		&= x_k - \tau\frac{x_k}{x_1 + sgn(x_1)|x|} \cdot\frac{(x_1 + sgn(x_1)|x|)^2 + \sum_{l=2}^{n} x_l^2}{2\big(x_1 + sgn(x_1)|x|\big)} = \\
		&= x_k - \tau\frac{x_k}{x_1 + sgn(x_1)|x|} \cdot\frac{(x_1 + sgn(x_1)|x|) + \sum_{l=2}^{n} (x_1 + sgn(x_1)|x|)v_l^2}{2} = \\
		&= x_k - \tau\cdot x_k \cdot\frac{1 + \sum_{l=2}^{n} v_l^2}{2} = x_k - \tau\cdot x_k \cdot\frac{\sum_{l=1}^{n} v_l^2}{2} =  x_k - \frac{2}{|v|^2}\cdot x_k \cdot\frac{|v|^2}{2} = 0
	\end{align*}

	We have already established in Theorem \ref{theorem-householder-reflector-orthogonal} that $Q$ is orthogonal.
	As multiplication by an orthogonal matrix preserves the Euclidean norm of a vector, we have
	$(Qx)_1^2 = |Qx|^2 - \sum_{l=2}^{n}(Qx)_l^2 = |x|^2$ and thus $(Qx)_1 = |x|$.
\end{proof}

\subsubsection{QR factorization using Householder reflectors}

Starting from this subsection, the expression $A_{i:k,j:l}$ will mark the submatrix of $A$ obtained by removing all rows except for the ones
with indices $i, i + 1, \dots, k$ and all columns except for the ones with indices $j, j+1, \dots, l$.

We can utilize Householder reflectors to construct the QR factorization of a matrix $A\in\R^{m,n}$. First, we introduce a lemma that will prove the correctness of such an algorithm.

\begin{lemma}\label{lemma-block-orthogonal-matrix}
	Let $Q_1\in\R^{m_1,m_1}$ and $Q_2\in\R^{m_2,m_2}$ be orthogonal matrices. The matrix $Q = \begin{pmatrix}
		Q_1& 0\\
		0& Q_2
	\end{pmatrix}$ is then also orthogonal.
\end{lemma}

\begin{proof}
	We will show that every two columns of $Q$ with indices $i,j\in\lbrace 1,\dots,m_1+m_2\rbrace$ are orthonormal. Without loss of generality, we assume $i\ge j$.
	\begin{itemize}
		\item if $m_1 \ge i = j$, then $Q_{:,i}Q_{:,j} = \sum_{k=1}^{m_1+m_2}Q_{k,i}Q_{k,i} = \sum_{k=1}^{m_1}(Q_1)_{k,i}(Q_1)_{k,i} = (Q_1)_{:,i}(Q_1)_{:,i} = 1$
		\item if $i = j > m_1$, then $Q_{:,i}Q_{:,j} = \sum_{k=1}^{m_1+m_2}Q_{k,i}Q_{k,i} = \sum_{k=1}^{m_2}(Q_2)_{k,i}(Q_2)_{k,i} = (Q_2)_{:,i}(Q_2)_{:,i} = 1$
		\item if $m_1 \ge i > j$, then $Q_{:,i}Q_{:,j} = \sum_{k=1}^{m_1+m_2}Q_{k,i}Q_{k,j} = \sum_{k=1}^{m_1}(Q_1)_{k,i}(Q_1)_{k,j} = (Q_1)_{:,i}(Q_1)_{:,j} = 0$
		\item if $i > j > m_1$, then $Q_{:,i}Q_{:,j} = \sum_{k=1}^{m_1+m_2}Q_{k,i}Q_{k,j} = \sum_{k=1}^{m_2}(Q_2)_{k,i}(Q_2)_{k,j} = (Q_2)_{:,i}(Q_2)_{:,j} = 0$
		\item if $i > m_1 \ge j$, then $Q_{:,i}Q_{:,j} = \sum_{k=1}^{m_1+m_2}Q_{k,i}Q_{k,j} = \sum_{k=1}^{m_1}Q_{k,i}\cdot 0 + \sum_{k=m_1+1}^{m_1+m_2}0\cdot Q_{k,j} = 0$
	\end{itemize}
\end{proof}

Householder reflectors can be used to compute the QR factorization of $A$ as follows:

\begin{algorithm} [H]\label{alg-householder-qr-factorization}
	\caption{QR decomposition of $A\in\R^{m,n}$ using Householder reflectors}
	\KwData{matrix $A\in\R^{m,m}$}
	\KwResult{upper trapezoidal $R\in\R^{m,n}$ (in the place of $A$)}
	\For{$i = 1, 2, \dots, \min(m,n)$}{
		construct a Householder reflector $\hat{Q}_i$ of the submatrix $A_{i:m,i}$\\
		$Q_i\gets \begin{pmatrix}
			I_{(i - 1)\times (i - 1)}& 0\\
			0 & \hat{Q}_i
		\end{pmatrix}$\\
		update $A\gets Q_i\cdot A$\label{householder-qr-factorization-update-step}\\
	}
\end{algorithm}

\begin{theorem}
	For any matrix $A\in\R^{m,n}$, Algorithm \ref{alg-householder-qr-factorization} returns an upper trapezoidal matrix $R\in\R^{m,n}$
	that satisfies $A = QR$ for some orthogonal matrix $Q\in\R^{m,m}$.
\end{theorem}

\begin{proof}
	We can show that at the end of the $i$th iteration, the submatrix $A_{1:(i-1),:}$ is upper trapezoidal:
	\begin{itemize}
		\item Supposing that $A_{1:(i-1),1:(i-1)}$ is already upper triangular (by induction), the entries below the main diagonal in the first $i-1$ columns of $A$ remain zero, because $$\begin{pmatrix}
			I_{(i - 1)\times (i - 1)}& 0\\
			0 & \hat{Q}_i
		\end{pmatrix}\cdot \begin{pmatrix}
			A_{1:(i-1),1:(i-1)}&A_{1:(i-1),i:n}\\0&A_{i:n,i:n}
		\end{pmatrix} = \begin{pmatrix}
			A_{1:(i-1),1:(i-1)}&A_{1:(i-1),i:n}\\0&\hat{Q}_iA_{i:n,i:n}
		\end{pmatrix} $$
		\item The entries in the submatrix $A_{(i+1):m,:}$ get zeroed out, as we have shown in Theorem \ref{theorem-householder-reflector-main-property}
	\end{itemize}

	The transformation of $A$ in step \ref{householder-qr-factorization-update-step} is orthogonal due to Lemma \ref{lemma-block-orthogonal-matrix} and the fact that the identity matrix is orthogonal. After the algorithm finishes, the original matrix $A$ is overwritten with
	$$R = Q_{\min(m,n)}\Big(Q_{\min(m,n)-1}\dots \big(Q_2(Q_1 A)\big)\Big) = Q_{\min(m,n)}\big(Q_{\min(m,n)-1}\dots (Q_2Q_1)\big)A = Q'A$$

	We can see that $Q'$ is a product of orthogonal matrices and is therefore also orthogonal. By multiplying the leftmost and rightmost expressions by $Q = (Q')^{T}$ from the left, we obtain
	$QR = A$.
\end{proof}

From now on, the values $v$ and $\tau$ used to construct the Householder reflector $\hat{Q}_i$ in the $i$th iteration of the loop in Algorithm \ref{alg-householder-qr-factorization} will be denoted $v_i$ and $\tau_i$.

Neither the matrices $Q_i$, nor the final matrix $Q'$ are typically saved in memory explicitly. Only the vectors $v_i$ and scalars $\tau_i$ used to construct the elementary reflectors are stored.
In order to minimize additional space usage, we may store the vector $v_i$ in the submatrix $A_{(i+1):m,i}$, whose entries are zeroed out during the $i$th iteration.
Note that the first entry of $v_i$ does not need to be saved at all, as it is by definition always equal to 1 \cite{LAPACK}.

In other words, all information about the vectors $v$ is stored in the entries of $A$ below the main diagonal, while the upper trapezoidal part of $A$ is overwritten with $R$. The only additional memory needed to reconstruct $Q$ is an array of length $k$ storing the values $\tau$.

Using these observations, we can design a QR factorization algorithm that also allows us to reconstruct the matrix $Q$:

\begin{algorithm} [H]\label{alg-implicit-householder-qr-factorization}
	\caption{QR decomposition of $A\in\R^{m,n}$ using implicitly stored Householder reflectors}
	\KwData{matrix $A\in\R^{m,m}$}
	\KwResult{overwritten matrix $A$ (as described above), array $\tau$ of size $\min(m,n)$}
	set up array $\tau$ of size $\min(m,n)$\\
	\For{$i = 1, 2, \dots, \min(m,n)$}{
		$\alpha\gets A_{i,i}$\\
		x\_norm $\gets |A_{i:m,i}|$\\
		$A_{(i+1):m,i}\gets A_{(i+1):m,i} / (\alpha + \sgn(\alpha)\cdot \text{x\_norm})$\\
		v\_norm\_square $\gets |A_{(i+1):m,i}|^2 + 1$\\
		$\tau_i = \frac{2}{\text{v\_norm\_square}}$\\
		update $A_{i:m,(i+1):n}\gets (I-\tau_i\cdot\begin{pmatrix}1\\A_{(i+1):m,i}\end{pmatrix}\cdot\begin{pmatrix}1&A_{(i+1):m,i}^T\end{pmatrix})\cdot A_{i:m,(i+1):n}$\\
	}
\end{algorithm}

\begin{figure}
	\caption{Implicit storage of elementary reflectors in Algorithm \ref{alg-implicit-householder-qr-factorization}.}
	\label{fig-implicit-householder-storage}
	\begin{center}
		\begin{tikzpicture}[scale=0.65]
			\filldraw[green!30!white] (0,0) -- (3,0) -- (3,5) -- (0,5);

			\draw[thick] (0,0) to (3,0);
			\draw[opacity=0.3] (0,1) to (3,1);
			\draw[opacity=0.3] (0,2) to (3,2);
			\draw[opacity=0.3] (0,3) to (3,3);
			\draw[opacity=0.3] (0,4) to (3,4);
			\draw[thick] (0,5) to (3,5);

			\draw[thick] (0,0) to (0,5);
			\draw[opacity=0.3] (1,0) to (1,5);
			\draw[opacity=0.3] (2,0) to (2,5);
			\draw[thick] (3,0) to (3,5);

			\node[green!50!black,scale=1.5] at (1.5, 2.5) {$A$};

			\node[scale=1.5] at (5, 2.5) {$\to$};

			%R

			\filldraw[red!30!white] (7,5) -- (7,6) -- (10,6) -- (10,5);
			\filldraw[red!30!white] (8,4) -- (8,5) -- (10,5) -- (10,4);
			\filldraw[red!30!white] (9,3) -- (9,4) -- (10,4) -- (10,3);

			\filldraw[orange!50!white] (7,1) -- (7,5) -- (8,5) -- (8,1);
			\filldraw[orange!30!white] (8,1) -- (8,4) -- (9,4) -- (9,1);
			\filldraw[orange!10!white] (9,1) -- (9,3) -- (10,3) -- (10,1);

			\draw[thick] (7,1) to (10,1);
			\draw[opacity=0.3] (7,2) to (10,2);
			\draw[opacity=0.3] (7,3) to (10,3);
			\draw[opacity=0.3] (7,4) to (10,4);
			\draw[opacity=0.3] (7,5) to (10,5);
			\draw[thick] (7,6) to (10,6);

			\draw[thick] (7,1) to (7,6);
			\draw[opacity=0.3] (8,1) to (8,6);
			\draw[opacity=0.3] (9,1) to (9,6);
			\draw[thick] (10,1) to (10,6);

			\draw[thick] (7,5) to (8,5);
			\draw[thick] (8,5) to (8,1);
			\draw[thick] (8,4) to (9,4);
			\draw[thick] (9,4) to (9,1);
			\draw[thick] (9,3) to (10,3);

			\node[red!50!black,scale=1.5] at (9.2, 5.2) {$R$};

			%tau
			\filldraw[blue!30!white] (7,-1) -- (7,0) -- (10,0) -- (10,-1);

			\draw[thick] (7,-1) to (10,-1);
			\draw[thick] (7,0) to (10,0);

			\draw[thick] (7,-1) to (7,0);
			\draw[opacity=0.3] (8,-1) to (8,0);
			\draw[opacity=0.3] (9,-1) to (9,0);
			\draw[thick] (10,-1) to (10,0);

			\node[blue!50!black] at (7.5,-0.5) {$\tau_1$};
			\node[blue!50!black] at (8.5,-0.5) {$\tau_2$};
			\node[blue!50!black] at (9.5,-0.5) {$\tau_3$};
			\node[blue!50!black,scale=1.5] at (6.5,-0.5) {$\tau$};

			\node[scale=1.5] at (12, 2.5) {where};

			\node[scale=1.2] at (15, 2.5) {$v_1=$};

			\filldraw[orange!50!white] (16,0) -- (16,4) -- (17,4) -- (17,0);

			\draw[thick] (16,0) to (17,0);
			\draw[opacity=0.3] (16,1) to (17,1);
			\draw[opacity=0.3] (16,2) to (17,2);
			\draw[opacity=0.3] (16,3) to (17,3);
			\draw[thick] (16,4) to (17,4);
			\draw[thick] (16,5) to (17,5);

			\draw[thick] (16,0) to (16,5);
			\draw[thick] (17,0) to (17,5);

			\node[] at (16.5, 4.5) {$1$};

			\node[scale=1.2] at (18, 2.5) {$,v_2=$};

			\filldraw[orange!30!white] (19,0.5) -- (19,3.5) -- (20,3.5) -- (20,0.5);

			\draw[thick] (19,0.5) to (20,0.5);
			\draw[opacity=0.3] (19,1.5) to (20,1.5);
			\draw[opacity=0.3] (19,2.5) to (20,2.5);
			\draw[thick] (19,3.5) to (20,3.5);
			\draw[thick] (19,4.5) to (20,4.5);

			\draw[thick] (19,0.5) to (19,4.5);
			\draw[thick] (20,0.5) to (20,4.5);

			\node[] at (19.5, 4) {$1$};

			\node[scale=1.2] at (21, 2.5) {$,v_3=$};

			\filldraw[orange!10!white] (22,1) -- (22,3) -- (23,3) -- (23,1);

			\draw[thick] (22,1) to (23,1);
			\draw[opacity=0.3] (22,2) to (23,2);
			\draw[thick] (22,3) to (23,3);
			\draw[thick] (22,4) to (23,4);

			\draw[thick] (22,1) to (22,4);
			\draw[thick] (23,1) to (23,4);

			\node[] at (22.5, 3.5) {$1$};

		\end{tikzpicture}
	\end{center}
\end{figure}

The storage of vectors $v_i$ and scalars $\tau_i$ can be seen in Figure \ref{fig-implicit-householder-storage}.

\subsection{Block Householder reflectors}

In this section, we always assume $m\ge n$, so the array $\tau$ is considered to be of length $n$. This section follows \cite{CompactWYT}.

The algorithm for QR decomposition presented in the previous subsection mainly utilizes Level 2 BLAS routines to carry out both the QR factorization and multiplication by $Q$ or $Q^T$ \cite{LAPACK}.

There have been several proposals to modify Algorithm \ref{alg-implicit-householder-qr-factorization} to use Level 3 BLAS routines in order to achieve better performance on modern processors	.
The approach that made its way into LAPACK is the \textbf{compact $WY^T$ representation}.

Instead of storing the scalars $\tau$ in an array of length $n$, we create an upper triangular matrix $T\in\R^{n,n}$ that is constructed in such a way that $$Q = I - YTY^T,$$
where $Y$ is the matrix obtained by taking the lower triangular part of $A$ (as returned by Algorithm \ref{alg-implicit-householder-qr-factorization}) and setting all of the diagonal entries to $1$.

That way, we can construct an algorithm for multiplying by $Q$ or $Q^T$ where the main part of the computation comprises of
matrix multiplications -- that is, Level 3 BLAS routines. As an example, we present the algorithm used to apply $Q$ on $C\in\R^{m,s}$ from the left:

\vspace{0pt}

\begin{algorithm} [H]\label{alg-compact-wyt-application}
	\caption{Application of $Q$ from the left side using compact $WY^T$ representation}
	\KwData{matrix $C\in\R^{m,s}$, lower trapezoidal matrix $Y\in\R^{m,n}$, lower triangular matrix $T\in\R^{n,n}$}
	\KwResult{$Q\cdot C$ (returned in the place of $C$)}
	set up a workspace matrix $work\in\R^{n,s}$\\
	$work\gets Y^T\cdot C$\\
	$work\gets T\cdot work$\quad\tcp{Using the \texttt{TRMM} routine, this operation can be done in-place}
	$C\gets C - Y\cdot work$\\
\end{algorithm}

The matrix $T$ can be generated from the array $\tau$ and matrix $Y$ (the lower trapezoidal part of $A$ after factorization with diagonal entries set to $1$) as follows:

\begin{algorithm} [H]\label{alg-compact-wyt-t-generation}
	\caption{Generation of $T$ compact $WY^T$ representation (inspired by \cite{CompactWYT})}
	\KwData{lower trapezoidal matrix $Y\in\R^{m,n}$, array $\tau$ of size $n$}
	\KwResult{matrix $T\in\R^{n,n}$}
	$T\gets\begin{pmatrix}\tau_1\end{pmatrix}$\\
	\For{$i = 2, \dots, n$}{
		$T\gets\begin{pmatrix}
			T&-T (Y_{:,1:(i-1)})^T Y_{:,i}\tau_i\\
			0&\tau_i
		\end{pmatrix}$\label{compact-wy-t-generation-grow-step}
	}
	\Return{$T$}
\end{algorithm}

\begin{theorem}
	Algorithm \ref{alg-compact-wyt-t-generation} generates a matrix $T$ such that $Q = I - YTY^T$.
\end{theorem}

\begin{proof}[Proof (inspired by \cite{CompactWYT})]

	For all $k\in\lbrace 1, \dots, n\rbrace$, we denote $T_k = T_{1:k,1:k}$ and $Y_k = Y_{:,1:k}$.
	We also denote the $k$th column of $Y$ as $\hat{v}_k$. Note that $v_k v_k^T = \hat{v}_k \hat{v}_k^T$.

	Using induction, we will prove that $Q_1\dots Q_{k-1}Q_k = I - Y_k T_k Y_k^T$ for all $k\in\lbrace 1,\dots, n\rbrace$:
	\begin{description}                                                                                              
		\item[Base step] $Q_1 = I - \tau_1 v_1 v_1^T = I - \tau_1\hat{v}_1\hat{v}_1^T = I - Y_1 T_1 Y_1^T$ 
		\item[Inductive step] Suppose that $Q_1\dots Q_{k-1}  = I - Y_{k-1}T_{k-1}Y_{k-1}^T$. Then:
		\begin{align*}
			I - Y_k T_k Y_k^T &=
			I - \begin{pmatrix}Y_{k-1}&\hat{v}_k\end{pmatrix}\begin{pmatrix}T_{k-1}&-T_{k-1}Y_{k-1}^T\hat{v}_k\tau_k\\0&\tau_k\end{pmatrix}\begin{pmatrix}Y_{k-1}^T\\\hat{v}_k^T\end{pmatrix} = \\
			&= I - \begin{pmatrix}Y_{k-1}T_{k-1}&-Y_{k-1}T_{k-1}Y_{k-1}^T\hat{v}_k\tau_k + \hat{v}_k\tau_k\end{pmatrix}\begin{pmatrix}Y_{k-1}^T\\\hat{v}_k^T\end{pmatrix} = \\
			&= I - Y_{k-1}T_{k-1}Y_{k-1}^T - \big(-Y_{k-1}T_{k-1}Y_{k-1}^T\hat{v}_k\tau_k + \hat{v}_k\tau_k\big)\hat{v}_k^T = \\
			&= I - Y_{k-1}T_{k-1}Y_{k-1}^T - \hat{v}_k\tau_k\hat{v}_k^T + Y_{k-1}T_{k-1}Y_{k-1}^T\hat{v}_k\tau_k\hat{v}_k^T = \\
			&= \big(I - Y_{k-1}T_{k-1}Y_{k-1}^T\big)\big(I - \hat{v}_k\tau_k\hat{v}_k^T\big)
			= \big(I - Y_{k-1}T_{k-1}Y_{k-1}^T\big)\big(I - \tau_k v_k v_k^T\big) = \\
			&= Q_1 \dots Q_{k-1} Q_k
		\end{align*}
	\end{description}

	As such, we have $Q = Q_1\dots Q_n = I - Y_n T_n Y_n^T = I - YTY^T$.
\end{proof}

To avoid creating the array $\tau$ using QR factorization altogether, we may save the computed values $\tau_k$ in the diagonal entries of $T$
and then compute the entries above the diagonal columnwise in a manner similar to Algorithm \ref{alg-compact-wyt-t-generation}.
A visualization of this storage scheme can be seen in Figure \ref{fig-compact-wy-storage}.

\begin{figure}
	\caption{Compact $WY^T$ storage of block Householder reflectors.}
	\label{fig-compact-wy-storage}
	\begin{center}
		\begin{tikzpicture}[scale=0.65]
			\filldraw[green!30!white] (0,0) -- (3,0) -- (3,5) -- (0,5);

			\draw[thick] (0,0) to (3,0);
			\draw[opacity=0.3] (0,1) to (3,1);
			\draw[opacity=0.3] (0,2) to (3,2);
			\draw[opacity=0.3] (0,3) to (3,3);
			\draw[opacity=0.3] (0,4) to (3,4);
			\draw[thick] (0,5) to (3,5);

			\draw[thick] (0,0) to (0,5);
			\draw[opacity=0.3] (1,0) to (1,5);
			\draw[opacity=0.3] (2,0) to (2,5);
			\draw[thick] (3,0) to (3,5);

			\node[green!50!black,scale=1.5] at (1.5, 2.5) {$A$};

			\node[scale=1.5] at (5, 2.5) {$\to$};

			%R

			\filldraw[red!30!white] (7,6) -- (7,7) -- (10,7) -- (10,6);
			\filldraw[red!30!white] (8,5) -- (8,6) -- (10,6) -- (10,5);
			\filldraw[red!30!white] (9,4) -- (9,5) -- (10,5) -- (10,3);

			\filldraw[orange!50!white] (7,2) -- (7,6) -- (8,6) -- (8,2);
			\filldraw[orange!50!white] (8,2) -- (8,5) -- (9,5) -- (9,2);
			\filldraw[orange!50!white] (9,2) -- (9,4) -- (10,4) -- (10,2);

			\draw[thick] (7,2) to (10,2);
			\draw[opacity=0.3] (7,3) to (10,3);
			\draw[opacity=0.3] (7,4) to (10,4);
			\draw[opacity=0.3] (7,5) to (10,5);
			\draw[opacity=0.3] (7,6) to (10,6);
			\draw[thick] (7,7) to (10,7);

			\draw[thick] (7,2) to (7,7);
			\draw[opacity=0.3] (8,2) to (8,7);
			\draw[opacity=0.3] (9,2) to (9,7);
			\draw[thick] (10,2) to (10,7);

			\draw[thick] (7,6) to (8,6);
			\draw[thick] (8,6) to (8,5);
			\draw[thick] (8,5) to (9,5);
			\draw[thick] (9,5) to (9,4);
			\draw[thick] (9,4) to (10,4);

			\node[red!50!black,scale=1.5] at (9.2, 6.2) {$R$};

			%tau
			\filldraw[blue!30!white] (7,0) -- (7,1) -- (10,1) -- (10,0);
			\filldraw[blue!30!white] (8,-1) -- (8,0) -- (10,0) -- (10,-1);
			\filldraw[blue!30!white] (9,-2) -- (9,-1) -- (10,-1) -- (10,-2);

			\draw[opacity=0.3] (8,0) to (8,1);
			\draw[opacity=0.3] (9,-1) to (9,1);
			\draw[thick] (10,-2) to (10,1);

			\draw[thick] (7,1) to (10,1);
			\draw[opacity=0.3] (8,0) to (10,0);
			\draw[opacity=0.3] (9,-1) to (10,-1);

			\draw[thick] (7,1) to (7,0);
			\draw[thick] (7,0) to (8,0);
			\draw[thick] (8,0) to (8,-1);
			\draw[thick] (8,-1) to (9,-1);
			\draw[thick] (9,-1) to (9,-2);
			\draw[thick] (9,-2) to (10,-2);

			\node[blue!50!black] at (7.5,0.5) {$\tau_1$};
			\node[blue!50!black] at (8.5,-0.5) {$\tau_2$};
			\node[blue!50!black] at (9.5,-1.5) {$\tau_3$};
			\node[blue!50!black,scale=1.5] at (7.5,-1.5) {$T$};

			\node[scale=1.5] at (12, 2.5) {where};

			\node[scale=1.2] at (15, 2.5) {$Y=$};

			\filldraw[orange!50!white] (16,0) -- (16,4) -- (17,4) -- (17,0);
			\filldraw[orange!50!white] (17,0) -- (17,3) -- (18,3) -- (18,0);
			\filldraw[orange!50!white] (18,0) -- (18,2) -- (19,2) -- (19,0);

			\draw[thick] (16,0) to (19,0);
			\draw[opacity=0.3] (16,1) to (19,1);
			\draw[opacity=0.3] (16,2) to (19,2);
			\draw[opacity=0.3] (16,3) to (19,3);
			\draw[opacity=0.3] (16,4) to (19,4);
			\draw[thick] (16,5) to (19,5);

			\draw[thick] (16,0) to (16,5);
			\draw[opacity=0.3] (17,0) to (17,5);
			\draw[opacity=0.3] (18,0) to (18,5);
			\draw[thick] (19,0) to (19,5);

			\draw[thick] (16,4) to (17,4);
			\draw[thick] (17,4) to (17,3);
			\draw[thick] (17,3) to (18,3);
			\draw[thick] (18,3) to (18,2);
			\draw[thick] (18,2) to (19,2);

			\node[] at (16.5, 4.5) {$1$};
			\node[] at (17.5, 4.5) {$0$};
			\node[] at (18.5, 4.5) {$0$};
			\node[] at (17.5, 3.5) {$1$};
			\node[] at (18.5, 3.5) {$0$};
			\node[] at (18.5, 2.5) {$1$};
		\end{tikzpicture}
	\end{center}
\end{figure}

In LAPACK, the routines for QR factorization and application of $Q$ using the compact $WY^T$ representation are named \texttt{DGEQRT} and \texttt{DGEMQRT}, respectively \cite{LAPACK}.

\subsubsection{Inner blocking}

The LAPACK routines \texttt{DGEQRT} and \texttt{DGEMQRT} also implement a concept called \textbf{inner blocking}.

When using inner blocking, $T$ no longer has to be upper triangular.
Instead of that, we select a value $ib\in\lbrace1,\dots, n\rbrace$ and then partition $Y$. Then $T$ is created in the following way:

\begin{itemize}
	\item $Y = \begin{pmatrix}
		Y_1 & Y_2 & \dots & Y_{\lceil\frac{n}{ib}\rceil}
	\end{pmatrix}$, where $Y_i\in\R^{m,ib}$ for $i\in\lbrace 1, \dots, \lceil\frac{n}{ib}\rceil - 1\rbrace$ and $Y_{\lceil\frac{n}{ib}\rceil}\in\R^{m,k}$ is an upper trapezoidal matrix
	\item $T = \begin{pmatrix}
		T_1 & T_2 & \dots & T_{\lceil\frac{n}{ib}\rceil}
	\end{pmatrix}$, where $T_i\in\R^{ib,ib}$ for $i\in\lbrace 1, \dots, \lceil\frac{n}{ib}\rceil - 1\rbrace$ are upper triangular matrices and $T_{\lceil\frac{n}{ib}\rceil}\in\R^{ib,k}$ is an upper trapezoidal matrix
\end{itemize}

Note that for $ib=1$, $T$ is a row vector, which matches the elementary reflector storage scheme shown in Figure \ref{fig-implicit-householder-storage}.
For $ib=n$, $T$ is upper triangular, which matches the compact $WY^T$ storage as shown in \ref{fig-compact-wy-storage}.

Using inner blocking, $Q$ is determined as $$Q = (I-Y_1T_1Y_1^T)(I-Y_2T_2Y_2^T)\dots (I-Y_{\lceil\frac{n}{ib}\rceil - 1}T_{\lceil\frac{n}{ib}\rceil - 1}Y_{\lceil\frac{n}{ib}\rceil - 1}^T)(I-Y_{\lceil\frac{n}{ib}\rceil}(T_{\lceil\frac{n}{ib}\rceil})_{1:k,1:k} Y_{\lceil\frac{n}{ib}\rceil}^T)$$

In order to generate $T$, we store the values $\tau_i, i\in\lbrace 1,\dots,n\rbrace$ in the diagonal entries of $T_1, \dots, T_{\lceil\frac{n}{ib}\rceil}$ from left to right.
We then use Algorithm \ref{alg-compact-wyt-t-generation} on every $T_i, i\in\lbrace 1, \dots, \lceil\frac{n}{ib}\rceil\rbrace$ separately.

\subsection{TT and TS kernels}

\begin{theorem}
	Let $A\in\R^{m,n}$ be a blocked matrix $A = \begin{pmatrix}A_1\\A_2\end{pmatrix}$.
	Let $A_1 = Q_1 R_1$ be the QR factorization of $A_1$ and $\begin{pmatrix}R_1\\A_2\end{pmatrix} = QR$ be the QR factorization
	of $\begin{pmatrix}R_1\\A_2\end{pmatrix}$.
	Then there exists a QR decomposition of $A$ where the upper trapezoidal factor is equal to $R$.
\end{theorem}

\begin{proof}
	$$A = \begin{pmatrix}A_1\\A_2\end{pmatrix} = \begin{pmatrix}Q_1 R_1\\A_2\end{pmatrix} = \begin{pmatrix}Q_1& 0\\0& I\end{pmatrix}\begin{pmatrix}R_1\\A_2\end{pmatrix} = \begin{pmatrix}Q_1& 0\\0& I\end{pmatrix}Q R$$

	The matrix $\begin{pmatrix}Q_1& 0\\0& I\end{pmatrix}$ is orthogonal as per Lemma \ref{lemma-block-orthogonal-matrix}, so $\begin{pmatrix}Q_1& 0\\0& I\end{pmatrix}Q$ is orthogonal as well. As such, $A = \begin{pmatrix}Q_1& 0\\0& I\end{pmatrix}QR$ is a QR decomposition of $A$.
\end{proof}

A blocked matrix $A = \begin{pmatrix}A_1\\A_2\end{pmatrix}$, where $A_1\in\R^{n,n}$ is upper triangular, may be factorized using \texttt{DGEQRT}, but that would require the entries in $A_1$
below the diagonal to be zeroed out and accessible.
However, thanks to the fact that $x_k = 0$ implies $v_k = 0$ for all $k\in\lbrace 2, \dots, n\rbrace$
(see Definition \ref{def-householder-reflector}), the entries below the main diagonal of $A_1$ are equal to zero both at the start and at the end
of Algorithm \ref{alg-implicit-householder-qr-factorization}. Thus, we can consider those entries to be implicitly zero and the routines that
factorize this matrix and apply $Q$ or $Q^T$ to other matrices
can be implemented in such a way that they do not access those entries at all.
These routines are called \textbf{triangle-on-top-of-square (TS) kernels}.

They are not implemented in LAPACK (as it is only a single-threaded library), but in PLASMA, the factorization routine is named \texttt{TSQRT} and the application routine is named \texttt{TSMQR} \cite{PLASMA}.

\begin{theorem}
	Let $A\in\R^{m,n}$ be a blocked matrix $A = \begin{pmatrix}A_1\\A_2\end{pmatrix}$.
	Let $A_1 = Q_1 R_1$ be the QR factorization of $A_1$, $A_2 = Q_2 R_2$ be the QR factorization of $A_2$ and 
	$\begin{pmatrix}R_1\\R_2\end{pmatrix} = QR$ be the QR factorization of $\begin{pmatrix}R_1\\R_2\end{pmatrix}$.
	Then there exists a QR decomposition of $A$ where the upper trapezoidal factor is equal to $R$.
\end{theorem}

\begin{proof}
	$$A = \begin{pmatrix}A_1\\A_2\end{pmatrix} = \begin{pmatrix}Q_1 R_1\\Q_2 R_2\end{pmatrix} = \begin{pmatrix}Q_1& 0\\0& Q_2\end{pmatrix}\begin{pmatrix}R_1\\R_2\end{pmatrix} = \begin{pmatrix}Q_1& 0\\0& Q_2\end{pmatrix}Q R$$

	The matrix $\begin{pmatrix}Q_1& 0\\0& Q_2\end{pmatrix}$ is orthogonal, so $A = \begin{pmatrix}Q_1& 0\\0& Q_2\end{pmatrix}QR$ is a QR decomposition of $A$.
\end{proof}

Routines that perform the QR factorization on a matrix $A = \begin{pmatrix}A_1\\A_2\end{pmatrix}$ and the corresponding application of $Q$, where $A_1\in\R^{n,n}$ is upper triangular and $A_2\in\R^{m,n}$ is upper trapezoidal, are called \textbf{triangle-on-top-of-triangle (TT) kernels}.
Unlike \texttt{GEQRT} and \texttt{GEMQRT}, they do not access any entries below the diagonal in either of the blocks.
The routines are not implemented in LAPACK, but in PLASMA, the equivalents of \texttt{GEQRT} and \texttt{GEMQRT} are named \texttt{TTQRT} and \texttt{TTMQR} respectively \cite{PLASMA}. 

\subsection{Elimination schemes}

In this section, we consider the matrix $A\in\R^{m,n}$ to be partitioned into blocks 
$$A = \begin{pmatrix}
	A_{11} & A_{12} & \dots & A_{1s}\\
	A_{21} & A_{22} & \dots & A_{2s}\\
	\vdots & \vdots & \ddots & \vdots\\
	A_{r1} & A_{r2} & \dots & A_{rs}
\end{pmatrix},$$

\noindent where every block (possibly except for blocks $A_{is}, i\in\lbrace 1, \dots, r\rbrace$) has width $nb$ and
every block (possibly except for blocks $A_{rj}, j\in\lbrace 1, \dots, s\rbrace$) has height $nb$. The width of the blocks
$A_{is}, i\in\lbrace 1, \dots, r\rbrace$ and the height of the blocks $A_{is}, i\in\lbrace 1, \dots, r\rbrace$
may not be larger than $nb$.

In order to eliminate all entries below the main diagonal, we have to use the routine \texttt{DGEQRT} on all diagonal blocks
and either \texttt{TSQRT} or \texttt{TTQRT} to eliminate the blocks strictly below the main diagonal.
Note that after every factorization kernel call, the corresponding application/update kernel has to be invoked on the same
combination of rows for all following columns.

According to \cite{INRIA}, we can describe every blocked QR factorization algorithm using an \textbf{elimination list}, which is a stricly
ordered list of triplets in the form $(i,piv(i,k),k)$. In order for an elimination list to be valid, several conditions have to be met \cite{INRIA}:
\begin{itemize}
	\item The values $i$ and $piv(i,k)$ have to be valid row block indices -- $i, piv(i,k)\in\lbrace 1, \dots, r\rbrace$
	\item The value $k$ has to represent a valid column block index -- $k\in\lbrace 1, \dots, s\rbrace$
	\item Blocks strictly above the diagonal may not be used as pivots -- $piv(i,k)\ge k$
	\item Only blocks strictly below the main diagonal may be eliminated -- $i > k$
	\item Every block below the main diagonal must be eliminated exactly once -- the elimination list has to contain exactly
	one triplet $(i,piv(i,k),k)$ for every $k\in\lbrace 1,\dots, \min(r,s)\rbrace, i\in\lbrace k+1, \dots, \min(r,s)\rbrace$
	\item All blocks left of the pivot block and the eliminated block must be already eliminated -- $(i,piv(i,k'),k')$ and
	$(piv(i,k),piv(piv(i,k),k'),k')$ must precede $(i,piv(i,k),k)$ in the elimination list for all $k\in\lbrace 1,\dots, k-1\rbrace$
	\item The pivot may not have already been eliminated -- $(piv(i,k), piv(piv(i,k),k),k)$ has to follow $(i,piv(i,k),k)$ in the
	elimination list
\end{itemize}

Unlike in \cite{INRIA}, we will also require the pivot block to be positioned above the eliminated block for simplicity. 
Formally, $i > piv(i,k)$ has to be true for every triplet $(i,piv(i,k),k)$ in the elimination list.

We have two options to implement the elimination $(i,piv(i,k),k)$:
\begin{enumerate}
	\item factorize $A_{piv(i,k)k}$ using \texttt{DGEQRT} (unless it already has been factorized), call \texttt{DGEMQRT} on blocks in the following columns, invoke \texttt{DTSQRT} on $A_{piv(i,k)k}$ and $A_{ik}$ and update following columns using \texttt{DTSMQR}
	\item factorize both $A_{piv(i,k)k}$ and $A_{ik}$ using \texttt{DGEQRT} (unless they have been already factorized), call \texttt{DGEMQRT} on relevant blocks in the following columns, invoke \texttt{DTTQRT} on $A_{piv(i,k)k}$ and $A_{ik}$ and update following columns using \texttt{DTTMQR}
\end{enumerate}

While the first approach offers better sequential performance (\texttt{DGEQRT} is called only once as opposed to twice), the second approach
is better paralellizable. Optimally, we want to find a balance between those two variables \cite{INRIA}.

We will now introduce several elimination schemes (algorithms) that act as recipes for creating elimination lists for matrices of any size.

The \textbf{flat-tree} scheme uses the elimination list $$\Big((i,k,k)|i=k+1,\dots,r; k=1,2,\dots, \min(r, s)\Big).$$
It only uses the diagonal blocks as pivots for all of the eliminations. As there are no blocks that act as both eliminated and pivoting
in different entries of the elimination list, the flat-tree scheme may be implemented using TS kernels only. It may also, however, be implemented
using only TT kernels or using a combination of TS and TT kernels \cite{INRIA}.

\begin{figure}[H]
	\caption{Example of the flat-tree elimination scheme for $r=5, s=3$.}
	\begin{center}
		\begin{tikzpicture}[scale=0.6]
			\filldraw[green!30!white] (0,0) -- (1,0) -- (1,5) -- (0,5);
			\filldraw[green!30!white] (0,0) -- (2,0) -- (2,4) -- (0,4);
			\filldraw[green!30!white] (0,0) -- (3,0) -- (3,3) -- (0,3);

			\draw[thick] (0,0) to (3,0);
			\draw[opacity=0.3] (0,1) to (3,1);
			\draw[opacity=0.3] (0,2) to (3,2);
			\draw[opacity=0.3] (0,3) to (3,3);
			\draw[opacity=0.3] (0,4) to (3,4);
			\draw[thick] (0,5) to (3,5);

			\draw[thick] (0,0) to (0,5);
			\draw[opacity=0.3] (1,0) to (1,5);
			\draw[opacity=0.3] (2,0) to (2,5);
			\draw[thick] (3,0) to (3,5);

			\draw[thick] (1,5) to (1,4);
			\draw[thick] (1,4) to (2,4);
			\draw[thick] (2,4) to (2,3);
			\draw[thick] (2,3) to (3,3);

			\draw[bend right=20,thick,-latex] (0.5,4.5) to (0.5,3.5);
			\draw[bend right=30,thick,-latex] (0.5,4.5) to (0.5,2.5);
			\draw[bend right=40,thick,-latex] (0.5,4.5) to (0.5,1.5);
			\draw[bend right=50,thick,-latex] (0.5,4.5) to (0.5,0.5);

			\draw[bend right=20,thick,-latex] (1.5,3.5) to (1.5,2.5);
			\draw[bend right=30,thick,-latex] (1.5,3.5) to (1.5,1.5);
			\draw[bend right=40,thick,-latex] (1.5,3.5) to (1.5,0.5);

			\draw[bend right=20,thick,-latex] (2.5,2.5) to (2.5,1.5);
			\draw[bend right=30,thick,-latex] (2.5,2.5) to (2.5,0.5);

			\node at (10,4.5){Elimination list:};
			\node at (10,2){\begin{tabular}{r c l}\Big((2,1,1),&(3,1,1),&(4,1,1),\\(5,1,1),&(3,2,2),&(4,2,2),\\(5,2,2),&(4,3,3),&(5,3,3)\Big)\end{tabular}};
		\end{tikzpicture}
	\end{center}
\end{figure}

The \textbf{greedy} scheme eliminates as many blocks from a certain column as possible, prioritizing the blocks on the bottom.
It pairs the pivots and the eliminated rows in natural order, meaning that in every step, the difference between every eliminated row index and
its corresponding pivot row index stays constant in a particular column \cite{INRIA}.

\begin{figure}[H]
	\caption{Example of the greedy elimination scheme for $r=5, s=3$.}
	\begin{center}
		\begin{tikzpicture}[scale=0.6]
			\filldraw[green!30!white] (0,0) -- (1,0) -- (1,5) -- (0,5);
			\filldraw[green!30!white] (0,0) -- (2,0) -- (2,4) -- (0,4);
			\filldraw[green!30!white] (0,0) -- (3,0) -- (3,3) -- (0,3);

			\draw[thick] (0,0) to (3,0);
			\draw[opacity=0.3] (0,1) to (3,1);
			\draw[opacity=0.3] (0,2) to (3,2);
			\draw[opacity=0.3] (0,3) to (3,3);
			\draw[opacity=0.3] (0,4) to (3,4);
			\draw[thick] (0,5) to (3,5);

			\draw[thick] (0,0) to (0,5);
			\draw[opacity=0.3] (1,0) to (1,5);
			\draw[opacity=0.3] (2,0) to (2,5);
			\draw[thick] (3,0) to (3,5);

			\draw[thick] (1,5) to (1,4);
			\draw[thick] (1,4) to (2,4);
			\draw[thick] (2,4) to (2,3);
			\draw[thick] (2,3) to (3,3);

			\draw[bend left=20,thick,-latex] (0.5,4.5) to (0.5,3.5);
			\draw[bend left=20,thick,-latex] (0.5,3.5) to (0.5,2.5);
			\draw[bend right=30,thick,-latex] (0.5,2.5) to (0.5,0.5);
			\draw[bend right=30,thick,-latex] (0.5,3.5) to (0.5,1.5);

			\draw[bend left=20,thick,-latex] (1.5,3.5) to (1.5,2.5);
			\draw[bend right=30,thick,-latex] (1.5,3.5) to (1.5,1.5);
			\draw[bend right=30,thick,-latex] (1.5,2.5) to (1.5,0.5);

			\draw[bend right=20,thick,-latex] (2.5,2.5) to (2.5,1.5);
			\draw[bend right=20,thick,-latex] (2.5,1.5) to (2.5,0.5);

			\node at (10,4.5){Elimination list:};
			\node at (10,2){\begin{tabular}{r c l}\Big((4,2,1),&(5,3,1),&(3,2,1),\\(2,1,1),&(4,2,2),&(5,3,2),\\(3,2,2),&(5,4,3),&(4,3,3)\Big)\end{tabular}};
		\end{tikzpicture}
	\end{center}
\end{figure}

The \textbf{Fibonacci} scheme puts all blocks below the diagonal from a particular column into groups in such a way that
the blocks are put into the first group whose index is greater than the number of blocks it has. The blocks are taken from top to bottom.
Less formally, the first group contains only the first block below the diagonal, the second group contains the second and third
block below the diagonal, and so on. The last used group may end up containing a lesser number of blocks than its index.

In each step, all $k$ blocks from the last group are eliminated using the $k$ blocks above them, pairing them in natural order \cite{INRIA}.

\begin{figure}[H]
	\caption{Example of the Fibonacci elimination scheme for $r=5, s=3$.}
	\begin{center}
		\begin{tikzpicture}[scale=0.6]
			\filldraw[green!30!white] (0,0) -- (1,0) -- (1,5) -- (0,5);
			\filldraw[green!30!white] (0,0) -- (2,0) -- (2,4) -- (0,4);
			\filldraw[green!30!white] (0,0) -- (3,0) -- (3,3) -- (0,3);

			\draw[thick] (0,0) to (3,0);
			\draw[opacity=0.3] (0,1) to (3,1);
			\draw[opacity=0.3] (0,2) to (3,2);
			\draw[opacity=0.3] (0,3) to (3,3);
			\draw[opacity=0.3] (0,4) to (3,4);
			\draw[thick] (0,5) to (3,5);

			\draw[thick] (0,0) to (0,5);
			\draw[opacity=0.3] (1,0) to (1,5);
			\draw[opacity=0.3] (2,0) to (2,5);
			\draw[thick] (3,0) to (3,5);

			\draw[thick] (1,5) to (1,4);
			\draw[thick] (1,4) to (2,4);
			\draw[thick] (2,4) to (2,3);
			\draw[thick] (2,3) to (3,3);

			\draw[bend left=20,thick,-latex] (0.5,1.5) to (0.5,0.5);
			\draw[bend right=30,thick,-latex] (0.5,3.5) to (0.5,1.5);
			\draw[bend right=30,thick,-latex] (0.5,4.5) to (0.5,2.5);
			\draw[bend left=20,thick,-latex] (0.5,4.5) to (0.5,3.5);

			\draw[bend left=20,thick,-latex] (1.5,3.5) to (1.5,2.5);
			\draw[bend right=30,thick,-latex] (1.5,3.5) to (1.5,1.5);
			\draw[bend right=30,thick,-latex] (1.5,2.5) to (1.5,0.5);

			\draw[bend right=20,thick,-latex] (2.5,2.5) to (2.5,1.5);
			\draw[bend right=20,thick,-latex] (2.5,1.5) to (2.5,0.5);

			\node at (10,4.5){Elimination list:};
			\node at (10,2){\begin{tabular}{r c l}\Big((4,2,1),&(5,3,1),&(3,2,1),\\(2,1,1),&(4,2,2),&(5,3,2),\\(3,2,2),&(5,4,3),&(4,3,3)\Big)\end{tabular}};
		\end{tikzpicture}
	\end{center}
\end{figure}

The \textbf{superblock greedy} scheme combines the high sequential perormance of TS kernels with the paralellizability os TT kernels.
It splits the rows in a single column into ``superblocks'' of constant size. First, columns within each superblock are eliminated using TS kernels
and then TT kernels are used in a manner similar to the greedy scheme to eliminate the first column of each superblock. The size of the superblock
can be adapted to the number of available threads, the number of block rows and the number of block columns in order to achieve the best performance possible.
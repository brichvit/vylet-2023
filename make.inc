include $(TOPDIR)/make_config.inc

SRC = $(TOPDIR)/src
QR_SRC = $(SRC)/qr
LQ_SRC = $(SRC)/lq
PDGE2GB_SRC = $(SRC)/pdge2gb
DGEQRT_SRC = $(SRC)/dgeqrt
DGEMQRT_SRC = $(SRC)/dgemqrt
DTSQRT_SRC = $(SRC)/dtsqrt
DTSMQR_SRC = $(SRC)/dtsmqr
DTTQRT_SRC = $(SRC)/dttqrt
DTTMQR_SRC = $(SRC)/dttmqr
DGELQT_SRC = $(SRC)/dgelqt
DGEMLQT_SRC = $(SRC)/dgemlqt
DTSLQT_SRC = $(SRC)/dtslqt
DTSMLQ_SRC = $(SRC)/dtsmlq
DTTLQT_SRC = $(SRC)/dttlqt
DTTMLQ_SRC = $(SRC)/dttmlq
DLACPY_SRC = $(SRC)/dlacpy
DLARFG_SRC = $(SRC)/dlarfg
COMMON_SRC = $(SRC)/common

TEST = $(TOPDIR)/test
LAPACK = $(TEST)/lapack

BENCHMARK = $(TOPDIR)/benchmark
COMMON_BENCHMARK = $(BENCHMARK)/common
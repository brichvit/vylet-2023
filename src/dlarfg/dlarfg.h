#ifndef DLARFG_H_
#define DLARFG_H_

/**
 * @brief Generates a real elementary reflector H of order n, such
 * that
 *
 *       H * ( alpha ) = ( beta ),   H**T * H = I.
 *           (   x   )   (   0  )
 *
 * where alpha and beta are scalars, and x is an (n-1)-element real
 * vector. H is represented in the form
 *
 *       H = I - tau * ( 1 ) * ( 1 v**T ) ,
 *                     ( v )
 *
 * where tau is a real scalar and v is a real (n-1)-element
 * vector.
 *
 * If the elements of x are all zero, then tau = 0 and H is taken to be
 * the unit matrix.
 * 
 * This is a C reimplementation of the DLARFG routine from LAPACK (see https://netlib.org/lapack/).
 *
 * @param n The order of the elementary reflector. @p n >= 0.
 * @param alpha The pointer to the value alpha. On exit, it is overwritten with the value beta.
 * @param x The array x of size (1 + (n-2) * abs (inc_x)). On exit, it is overwritten with the vector v.
 * @param inc_x The increment between elements of x. @p inc_x > 0.
 * @param tau The value tau.
 * @return int The number of the first argument with an invalid value. If all arguments have a valid value, 0 is returned.
 */
int dlarfg (int n, double* alpha, double* x, int inc_x, double* tau);

#endif /* DLARFG_H_ */

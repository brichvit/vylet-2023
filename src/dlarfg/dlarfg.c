#include "dlarfg.h"
#include "../common/math_utils.h"

#include <math.h>

#if defined (USE_MKL)
#include <mkl_lapacke.h>
#elif defined (USE_ARMPL)
#include <armpl.h>
#endif

int dlarfg (int n, double* alpha, double* x, int inc_x, double* tau) {
	// Check parameter values
	if (n < 0) {
		return 1;
	} else if (inc_x == 0) {
		return 4;
	}

	#if defined (USE_MKL) || defined (USE_ARMPL)
	LAPACKE_dlarfg_work (n, alpha, x, inc_x, tau);
	return 0;
	#endif

	// Calculate the square of the norm of x and exit if it is zero (= all elements of x are zero)
	double x_norm_squared = 0;
	// This is equivalent to:
	//
	//	x_norm_squared = cblas_ddot (n - 1, x, inc_x, x, inc_x);
	for (int i = 0; i < n - 1; i++) {
		x_norm_squared += x[i * inc_x] * x[i * inc_x];
	}

	if (x_norm_squared == 0) {
		*tau = 0;
		return 0;
	}

	// Calculate the norm of
	// ( 1 )
	// ( x )
	double x_ext_norm = sqrt (x_norm_squared + *alpha * *alpha);

	// Calculate basic (unnormalized) reflection vector
	double corrected_sgn_alpha = sgn(*alpha);
	if (corrected_sgn_alpha == 0) {
		corrected_sgn_alpha = 1;
	}
	double reflection_vector_alpha = *alpha + corrected_sgn_alpha * x_ext_norm;

	// Scale the reflection vector so that alpha = 1 and calculate the square of its norm
	double scaled_reflection_vec_norm_squared = 1;
	// This is equivalent to:
	//
	//	cblas_dscal (n - 1, 1 / reflection_vector_alpha, x, inc_x);
	//
	//	scaled_reflection_vec_norm_squared += cblas_ddot (n - 1, x, inc_x, x, inc_x);
	for (int i = 0; i < n - 1; i++) {
		x[i * inc_x] /= reflection_vector_alpha;
		scaled_reflection_vec_norm_squared += x[i * inc_x] * x[i * inc_x];
	}

	// Set tau to 2 / scaled_reflection_vec_norm_squared
	*tau = 2 / scaled_reflection_vec_norm_squared;

	// Calculate beta by implicitly multiplying the reflector matrix
	// I - tau * ( 1 ) * ( 1  v^T )
	//           ( v )
	//
	// by the original vector
	// ( 1 )
	// ( x )
	double beta = *alpha * (1 - *tau);
	// This is equivalent to:
	//
	//	beta -= *tau * reflection_vector_alpha * cblas_ddot (n - 1, x, inc_x, x, inc_x);
	for (int i = 0; i < n - 1; i++) {
		beta -= *tau * x[i * inc_x] * x[i * inc_x] * reflection_vector_alpha;
	}

	// Overwrite alpha with beta
	*alpha = beta;

	return 0;
}

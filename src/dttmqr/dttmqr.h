#ifndef DTTMQR_H_
#define DTTMQR_H_

// TODO: document
void dttmqr_inner_block (char side, char trans, int m1, int n1, int m2, int n2, int k, int additional_V_full_rows, double* A1, int lda1, double* A2, int lda2, double* V, int ldv, double* T, int ldt, double* work);

int dttmqr (char side, char trans, int m1, int n1, int m2, int n2, int k, int nb, double* A1, int lda1, double* A2, int lda2, double* V, int ldv, double* T, int ldt, double* work);

#endif /* DTTMQR_H_ */
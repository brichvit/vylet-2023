#include "dtsqrt.h"
#include "../dtsmqr/dtsmqr.h"
#include "../dlarfg/dlarfg.h"
#include "../common/math_utils.h"

#include <memory.h>

#define A1_ENTRY(i,j) A1[i + (j) * lda1]
#define A2_ENTRY(i,j) A2[i + (j) * lda2]
#define T_ENTRY(i,j) T[i + (j) * ldt]
#define WORK_ENTRY(i) work[i]

#if defined (USE_MKL)
#include <mkl_lapacke.h>
#elif defined (USE_ARMPL)
#include <armpl.h>
#endif

void dtsqrt_inner_block (int m, int n, double* A1, int lda1, double* A2, int lda2, double* T, int ldt, double* work) {
	for (int reflector_index = 0; reflector_index < n; reflector_index++) {
		// Obtain the Householder reflector
		double* A1_diagonal_entry = &A1_ENTRY(reflector_index, reflector_index);
		dlarfg (m + 1, A1_diagonal_entry, &A2_ENTRY(0, reflector_index), 1, &T_ENTRY(reflector_index, reflector_index));

		// Propagate the reflector into the following columns
		double tau = T_ENTRY(reflector_index, reflector_index);
		
		// work := A1[reflector_index,reflector_index+1:n]
		//
		// This is equivalent to:
		//
		//	cblas_dcopy (n - reflector_index - 1, &A1_ENTRY(reflector_index, reflector_index + 1), lda1, work, 1);
		for (int work_index = 0; work_index < n - reflector_index - 1; work_index++) {
			WORK_ENTRY(work_index) = A1_ENTRY(reflector_index, reflector_index + 1 + work_index);
		}

		// A1[reflector_index,reflector_index+1:n] := (1 - tau) * A1[reflector_index,reflector_index+1:n] - tau * v2^T * A2[:,reflector_index+1:n]
		//
		// where v2 = A2[:,reflector_index]
		//
		// This is equivalent to:
		//
		//	cblas_dscal (n - reflector_index - 1, 1.0 - tau, &A1_ENTRY(reflector_index, reflector_index + 1), lda1);
		//
		//	cblas_dgemv (CblasColMajor, CblasTrans, m, n - reflector_index - 1, -tau, &A2_ENTRY(0, reflector_index + 1), lda2, &A2_ENTRY(0, reflector_index), 1, 1.0, &A1_ENTRY(reflector_index, reflector_index + 1), lda1);
		for (int updated_col = reflector_index + 1; updated_col < n; updated_col++) {
			A1_ENTRY(reflector_index, updated_col) *= 1 - tau;
			for (int k = 0; k < m; k++) {
				A1_ENTRY(reflector_index, updated_col) -= tau * A2_ENTRY(k, reflector_index) * A2_ENTRY(k, updated_col);
			}
		}

		// work := work + v2^T * A2[:,reflector_index+1:n]
		//
		// where v2 = A2[:,reflector_index]
		//
		// This is equivalent to:
		//	cblas_dgemv (CblasColMajor, CblasTrans, m, n - reflector_index - 1, 1.0, &A2_ENTRY(0, reflector_index + 1), lda2, &A2_ENTRY(0, reflector_index), 1, 1.0, work, 1);
		for (int work_index = 0; work_index < n - reflector_index - 1; work_index++) {
			for (int k = 0; k < m; k++) {
				WORK_ENTRY(work_index) += A2_ENTRY(k, reflector_index) * A2_ENTRY(k, work_index + reflector_index + 1);
			}
		}

		// A2[:,reflector_index+1:n] := A2[:,reflector_index+1:n] - tau * v2 * work^T
		//
		// where v2 = A2[:,reflector_index]
		//
		// This is equivalent to:
		//
		//	cblas_dger (CblasColMajor, m, n - reflector_index - 1, -tau, &A2_ENTRY(0, reflector_index), 1, work, 1, &A2_ENTRY(0, reflector_index + 1), lda2);
		for (int updated_col = reflector_index + 1; updated_col < n; updated_col++) {
			for (int updated_row = 0; updated_row < m; updated_row++) {
				A2_ENTRY(updated_row, updated_col) -= tau * A2_ENTRY(updated_row, reflector_index) * WORK_ENTRY(updated_col - reflector_index - 1);
			}	
		}
	}

	// Calculate the non-diagonal reflector factors in T
	for (int col = 1; col < n; col++) {
		double tau = T[col * ldt + col];

		// T[0:col,col] := -tau * A2[:,0:col]^T * A2[:,col]
		//
		// This is equivalent to:
		//
		//	cblas_dgemv (CblasColMajor, CblasTrans, m, col, -tau, A2, lda2, &A2_ENTRY(0, col), 1, 0.0, &T_ENTRY(0, col), 1);
		memset (&T_ENTRY(0,col), 0, col * sizeof (double));
		for (int row = 0; row < col; row++) {
			for (int k = 0; k < m; k++) {
				T_ENTRY(row, col) -= tau * A2_ENTRY(k, row) * A2_ENTRY(k, col);
			}
		}

		// T[0:col,col] := T[0:col,0:col] * T[0:col,col]
		//
		// This is equivalent to:
		//
		//	cblas_dtrmv (CblasColMajor, CblasUpper, CblasNoTrans, CblasNonUnit, col, T, ldt, &T_ENTRY(0, col), 1);
		for (int row = 0; row < col; row++) {
			T_ENTRY(row, col) *= T_ENTRY(row, row);

			for (int k = row + 1; k < col; k++) {
				T_ENTRY(row, col) += T_ENTRY(k, col) * T_ENTRY(row, k);
			}
		}
	}
}

int dtsqrt (int m, int n, int nb, double* A1, int lda1, double* A2, int lda2, double* T, int ldt, double* work) {
	// Check parameter values
	if (m < 0) {
		return 1;
	} else if (n < 0) {
		return 2;
	} else if (nb <= 0) {
		return 3;
	} else if (lda1 < max (1, n)) {
		return 5;
	} else if (lda2 < max (1, m)) {
		return 7;
	} else if (ldt < max (1, nb)) {
		return 9;
	}

	#if defined (USE_MKL) || defined (USE_ARMPL)
	if (nb > n) {
		nb = n;
	}

	LAPACKE_dtpqrt_work (LAPACK_COL_MAJOR, m, n, 0, nb, A1, lda1, A2, lda2, T, ldt, work);
	#else
	for (int block_start = 0; block_start < n; block_start += nb) {
		int block_size = min (nb, n - block_start);

		dtsqrt_inner_block (m, block_size, &A1_ENTRY(block_start, block_start), lda1, &A2_ENTRY(0, block_start), lda2, &T_ENTRY(0, block_start), ldt, work);

		if (block_start + block_size < n) {
			dtsmqr ('L', 'T', block_size, n - block_start - block_size, m, n - block_start - block_size, block_size, nb, &A1_ENTRY(block_start, block_start + block_size), lda1, &A2_ENTRY(0, block_start + block_size), lda2, &A2_ENTRY(0, block_start), lda2, &T_ENTRY(0, block_start), ldt, work);
		}
	}
	#endif

	return 0;
}
#include "dtsqrt_task.h"
#include "dtsqrt.h"

#if defined (USE_STARPU)
#include "../common/starpu_matrix_utils.h"

void dtsqrt_func (void* buffers[], void* args) {
	int m = STARPU_COL_MATRIX_GET_NY(buffers[1]);
	int n = STARPU_COL_MATRIX_GET_NX(buffers[0]);

	int ib = STARPU_COL_MATRIX_GET_NY(buffers[3]);

	int lda1 = STARPU_COL_MATRIX_GET_LD(buffers[0]);
	int lda2 = STARPU_COL_MATRIX_GET_LD(buffers[1]);
	int ldt = STARPU_COL_MATRIX_GET_LD(buffers[3]);

	double* A1 = (double*)STARPU_MATRIX_GET_PTR(buffers[0]);
	double* A2 = (double*)STARPU_MATRIX_GET_PTR(buffers[1]);
	double* T = (double*)STARPU_MATRIX_GET_PTR(buffers[3]);

	double** workspace;
	starpu_codelet_unpack_args (args, &workspace);

	double* work = workspace[starpu_worker_get_id ()];
	
	dtsqrt (m, n, ib, A1, lda1, A2, lda2, T, ldt, work);
}

struct starpu_codelet dtsqrt_codelet = {
	.cpu_funcs = { dtsqrt_func },
	.cpu_funcs_name = { "dtsqrt" },
	.nbuffers = 4,
	.modes = { STARPU_RW, STARPU_RW, STARPU_RW, STARPU_W },
	.name = "DTSQRT"
};

void insert_dtsqrt_task (data_handle_t upper_triangle_a1_handle,
						 data_handle_t upper_triangle_a2_handle, data_handle_t lower_triangle_a2_handle,
						 data_handle_t t_handle, double** workspace) {
	starpu_task_insert (
		&dtsqrt_codelet,
		STARPU_RW, upper_triangle_a1_handle,
		STARPU_RW, upper_triangle_a2_handle,
		STARPU_RW, lower_triangle_a2_handle,
		STARPU_W, t_handle,
		STARPU_VALUE, &workspace, sizeof (double**),
		0
	);
}
#elif defined (USE_OPENMP)
#include <omp.h>

void insert_dtsqrt_task (data_handle_t upper_triangle_a1_handle,
						 data_handle_t upper_triangle_a2_handle, data_handle_t lower_triangle_a2_handle,
						 data_handle_t t_handle, double** workspace) {
	// Do not mark lower_triangle_a2_handle as unused
	(void)lower_triangle_a2_handle;

	#pragma omp task \
			depend(inout: *upper_triangle_a1_handle.ptr) \
			depend(inout: *upper_triangle_a2_handle.ptr) \
			depend(inout: *lower_triangle_a2_handle.ptr) \
			depend(out: *t_handle.ptr)
	{
		double* work = workspace[omp_get_thread_num ()];

		dtsqrt (
			upper_triangle_a2_handle.height,
			upper_triangle_a1_handle.width,
			t_handle.height,
			upper_triangle_a1_handle.ptr,
			upper_triangle_a1_handle.leading_dimension,
			upper_triangle_a2_handle.ptr,
			upper_triangle_a2_handle.leading_dimension,
			t_handle.ptr,
			t_handle.leading_dimension,
			work
		);
	}
}
#endif
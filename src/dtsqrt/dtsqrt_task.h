#ifndef DTSQRT_TASK_H_
#define DTSQRT_TASK_H_

#include "../common/runtime_system.h"

void insert_dtsqrt_task (data_handle_t upper_triangle_a1_handle,
						 data_handle_t upper_triangle_a2_handle, data_handle_t lower_triangle_a2_handle,
						 data_handle_t t_handle, double** workspace);

#endif /* DTSQRT_TASK_H_ */
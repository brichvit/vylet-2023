#ifndef DTSMQR_TASK_H_
#define DTSMQR_TASK_H_

#include "../common/runtime_system.h"

void insert_dtsmqr_task (char side, char trans, data_handle_t upper_triangle_v_handle, data_handle_t lower_triangle_v_handle,
						 data_handle_t upper_triangle_a1_handle, data_handle_t lower_triangle_a1_handle,
						 data_handle_t upper_triangle_a2_handle, data_handle_t lower_triangle_a2_handle,
						 data_handle_t t_handle, double** workspace);

#endif /* DTSMQR_TASK_H_ */
#include "dtsmqr_task.h"
#include "dtsmqr.h"

#if defined (USE_STARPU)
#include "../common/starpu_matrix_utils.h"

void dtsmqr_func (void* buffers[], void* args) {
	int m1 = STARPU_COL_MATRIX_GET_NY(buffers[2]);
	int m2 = STARPU_COL_MATRIX_GET_NY(buffers[4]);
	int n1 = STARPU_COL_MATRIX_GET_NX(buffers[2]);
	int n2 = STARPU_COL_MATRIX_GET_NX(buffers[4]);
	int k = STARPU_COL_MATRIX_GET_NX(buffers[0]);

	int ib = STARPU_COL_MATRIX_GET_NY(buffers[6]);

	int ldv = STARPU_COL_MATRIX_GET_LD(buffers[0]);
	int lda1 = STARPU_COL_MATRIX_GET_LD(buffers[2]);
	int lda2 = STARPU_COL_MATRIX_GET_LD(buffers[4]);
	int ldt = STARPU_COL_MATRIX_GET_LD(buffers[6]);

	double* V = (double*)STARPU_MATRIX_GET_PTR(buffers[0]);
	double* A1 = (double*)STARPU_MATRIX_GET_PTR(buffers[2]);
	double* A2 = (double*)STARPU_MATRIX_GET_PTR(buffers[4]);
	double* T = (double*)STARPU_MATRIX_GET_PTR(buffers[6]);

	char side, trans;
	double** workspace;
	starpu_codelet_unpack_args (args, &side, &trans, &workspace);

	double* work = workspace[starpu_worker_get_id ()];

	dtsmqr (side, trans, m1, n1, m2, n2, k, ib, A1, lda1, A2, lda2, V, ldv, T, ldt, work);
}

struct starpu_codelet dtsmqr_codelet = {
	.cpu_funcs = { dtsmqr_func },
	.cpu_funcs_name = { "dtsmqr" },
	.nbuffers = 7,
	.modes = { STARPU_R, STARPU_R, STARPU_RW, STARPU_RW, STARPU_RW, STARPU_RW, STARPU_R },
	.name = "DTSMQR"
};

void insert_dtsmqr_task (char side, char trans, data_handle_t upper_triangle_v_handle, data_handle_t lower_triangle_v_handle,
						 data_handle_t upper_triangle_a1_handle, data_handle_t lower_triangle_a1_handle,
						 data_handle_t upper_triangle_a2_handle, data_handle_t lower_triangle_a2_handle,
						 data_handle_t t_handle, double** workspace) {
	starpu_task_insert (
		&dtsmqr_codelet,
		STARPU_R, upper_triangle_v_handle,
		STARPU_R, lower_triangle_v_handle,
		STARPU_RW, upper_triangle_a1_handle,
		STARPU_RW, lower_triangle_a1_handle,
		STARPU_RW, upper_triangle_a2_handle,
		STARPU_RW, lower_triangle_a2_handle,
		STARPU_R, t_handle,
		STARPU_VALUE, &side, sizeof(char),
		STARPU_VALUE, &trans, sizeof(char),
		STARPU_VALUE, &workspace, sizeof (double**),
		0
	);
}
#elif defined (USE_OPENMP)
#include <omp.h>

void insert_dtsmqr_task (char side, char trans, data_handle_t upper_triangle_v_handle, data_handle_t lower_triangle_v_handle,
						 data_handle_t upper_triangle_a1_handle, data_handle_t lower_triangle_a1_handle,
						 data_handle_t upper_triangle_a2_handle, data_handle_t lower_triangle_a2_handle,
						 data_handle_t t_handle, double** workspace) {
	// Do not mark lower_triangle_v_handle, lower_triangle_a1_handle and lower_triangle_a2_handle as unused
	(void)lower_triangle_v_handle;
	(void)lower_triangle_a1_handle;
	(void)lower_triangle_a2_handle;

	#pragma omp task \
			depend(in: *upper_triangle_v_handle.ptr) \
			depend(in: *lower_triangle_v_handle.ptr) \
			depend(inout: *upper_triangle_a1_handle.ptr) \
			depend(inout: *lower_triangle_a1_handle.ptr) \
			depend(inout: *upper_triangle_a2_handle.ptr) \
			depend(inout: *lower_triangle_a2_handle.ptr) \
			depend(in: *t_handle.ptr)
	{
		double* work = workspace[omp_get_thread_num ()];

		dtsmqr (
			side,
			trans,
			upper_triangle_a1_handle.height,
			upper_triangle_a1_handle.width,
			upper_triangle_a2_handle.height,
			upper_triangle_a2_handle.width,
			upper_triangle_v_handle.width,
			t_handle.height,
			upper_triangle_a1_handle.ptr,
			upper_triangle_a1_handle.leading_dimension,
			upper_triangle_a2_handle.ptr,
			upper_triangle_a2_handle.leading_dimension,
			upper_triangle_v_handle.ptr,
			upper_triangle_v_handle.leading_dimension,
			t_handle.ptr,
			t_handle.leading_dimension,
			work
		);
	}
}
#endif
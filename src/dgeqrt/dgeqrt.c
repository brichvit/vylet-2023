#include "dgeqrt.h"
#include "../dgemqrt/dgemqrt.h"
#include "../common/math_utils.h"
#include "../dlarfg/dlarfg.h"

#include <stdlib.h>
#include <memory.h>

#define A_ENTRY(i,j) A[i + (j) * lda]
#define T_ENTRY(i,j) T[i + (j) * ldt]
#define WORK_ENTRY(i) work[i]

#if defined (USE_MKL)
#include <mkl_lapacke.h>
#elif defined (USE_ARMPL)
#include <armpl.h>
#endif

void dgeqrt_inner_block (int m, int n, double* A, int lda, double* T, int ldt, double* work) {
	for (int reflector_index = 0; reflector_index < min (m, n); reflector_index++) {
		// Obtain the Householder reflector
		double* diagonal_entry = A + reflector_index * lda + reflector_index;
		dlarfg (m - reflector_index, diagonal_entry, diagonal_entry + 1, 1, T + reflector_index * ldt + reflector_index);

		// Propagate the reflector into following columns
		double tau = T[reflector_index * ldt + reflector_index];

		// work := v^T * A[reflector_index:m, (reflector_index+1):n]
		//
		// where v = (                                         1 )
		//           ( A[(reflector_index+1):m, reflector_index] )
		//
		// This is equivalent to:
		//
		//	cblas_dcopy (n - reflector_index - 1, &A_ENTRY(reflector_index, reflector_index + 1), lda, work, 1);
		//
		//	cblas_dgemv (CblasColMajor, CblasTrans, m - reflector_index - 1, n - reflector_index - 1, 1.0, &A_ENTRY(reflector_index + 1, reflector_index + 1), lda, &A_ENTRY(reflector_index + 1, reflector_index), 1, 1.0, work, 1);
		for (int work_index = 0; work_index < n - reflector_index - 1; work_index++) {
			WORK_ENTRY(work_index) = A_ENTRY(reflector_index, reflector_index + 1 + work_index);

			for (int v_index = 1; v_index < m - reflector_index; v_index++) {
				WORK_ENTRY(work_index) += A_ENTRY(reflector_index + v_index, reflector_index) * A_ENTRY(reflector_index + v_index, reflector_index + 1 + work_index);
			}
		}

		// A[reflector_index:m, (reflector_index+1):n] = A[reflector_index:m, (reflector_index+1):n] - tau * v * work^T
		//
		// where v = (                                         1 )
		//           ( A[(reflector_index+1):m, reflector_index] ) 
		//
		// This is equivalent to:
		//
		//	cblas_daxpy (n - reflector_index - 1, -tau, work, 1, &A_ENTRY(reflector_index, reflector_index + 1), lda);
		//
		//	cblas_dger (CblasColMajor, m - reflector_index - 1, n - reflector_index - 1, -tau, &A_ENTRY(reflector_index + 1, reflector_index), 1, work, 1, &A_ENTRY(reflector_index + 1, reflector_index + 1), lda);
		for (int col = 0; col < n - reflector_index - 1; col++) {
			A_ENTRY(reflector_index, reflector_index + 1 + col) -= tau * WORK_ENTRY(col);

			for (int row = 1; row < m - reflector_index; row++) {
				A_ENTRY(reflector_index + row, reflector_index + 1 + col) -= tau * A_ENTRY(reflector_index + row, reflector_index) * WORK_ENTRY(col);
			}
		}
	}

	// Calculate the non-diagonal reflector factors in T
	for (int col = 1; col < n; col++) {
		double tau = T[col * ldt + col];

		// T[0:col,col] := -tau * A[(col+1):m,0:col]^T * A[(col+1):m,col]
		//
		// This is equivalent to:
		//
		//	cblas_dcopy (col, &A_ENTRY(col, 0), lda, &T_ENTRY(0, col), 1);
		//
		//	cblas_dscal (col, -tau, &T_ENTRY(0, col), 1);
		//
		//	cblas_dgemv (CblasColMajor, CblasTrans, m - col - 1, col, -tau, &A_ENTRY(col + 1, 0), lda, &A_ENTRY(col + 1, col), 1, 1.0, &T_ENTRY(0, col), 1);
		for (int row = 0; row < col; row++) {
			T_ENTRY(row, col) = -tau * A_ENTRY(col, row);

			for (int k = col + 1; k < m; k++) {
				T_ENTRY(row, col) -= tau * A_ENTRY(k, row) * A_ENTRY(k, col);
			}
		}

		// T[0:col,col] := T[0:col,0:col] * T[0:col,col]
		//
		// This is equivalent to:
		//
		//	cblas_dtrmv (CblasColMajor, CblasUpper, CblasNoTrans, CblasNonUnit, col, T, ldt, &T_ENTRY(0, col), 1);
		for (int row = 0; row < col; row++) {
			T_ENTRY(row, col) *= T_ENTRY(row, row);

			for (int k = row + 1; k < col; k++) {
				T_ENTRY(row, col) += T_ENTRY(k, col) * T_ENTRY(row, k);
			}
		}
	}
}

int dgeqrt (int m, int n, int nb, double* A, int lda, double* T, int ldt, double* work) {
	// Check parameter values
	if (m < 0) {
		return 1;
	} else if (n < 0) {
		return 2;
	} else if (nb <= 0) {
		return 3;
	} else if (lda < max (1, m)) {
		return 5;
	} else if (ldt < max (1, nb)) {
		return 7;
	}

	#if defined (USE_MKL) || defined (USE_ARMPL)
	if (nb > min (m, n)) {
		nb = min (m, n);
	}

	LAPACKE_dgeqrt_work (LAPACK_COL_MAJOR, m, n, nb, A, lda, T, ldt, work);
	#else
	for (int block_start = 0; block_start < min (m, n); block_start += nb) {
		int block_size = min (nb, min(m, n) - block_start);

		dgeqrt_inner_block (m - block_start, block_size, &A_ENTRY(block_start, block_start), lda, &T_ENTRY(0, block_start), ldt, work);

		if (block_start + block_size < n) {
			dgemqrt_inner_block ('L', 'T', m - block_start, n - block_start - block_size, block_size, &A_ENTRY(block_start, block_start), lda, &T_ENTRY(0, block_start), ldt, &A_ENTRY(block_start, block_start + block_size), lda, work);
		}
	}
	#endif

	return 0;
}
#ifndef DGEQRT_TASK_H_
#define DGEQRT_TASK_H_

#include "../common/runtime_system.h"

void insert_dgeqrt_task (data_handle_t upper_triangle_a_handle, data_handle_t lower_triangle_a_handle,
						 data_handle_t t_handle, double** workspace);

#endif /* DGEQRT_TASK_H_ */
#ifndef DGELQT_TASK_H_
#define DGELQT_TASK_H_

#include "../common/runtime_system.h"

void insert_dgelqt_task (data_handle_t upper_triangle_a_handle, data_handle_t lower_triangle_a_handle,
						 data_handle_t t_handle, double** workspace);

#endif /* DGELQT_TASK_H_ */
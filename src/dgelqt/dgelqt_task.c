#include "dgelqt_task.h"
#include "dgelqt.h"

#if defined (USE_STARPU)
#include "../common/starpu_matrix_utils.h"

void dgelqt_func (void* buffers[], void* args) {
	int m = STARPU_COL_MATRIX_GET_NY(buffers[0]);
	int n = STARPU_COL_MATRIX_GET_NX(buffers[0]);

	int ib = STARPU_COL_MATRIX_GET_NY(buffers[2]);

	int lda = STARPU_COL_MATRIX_GET_LD(buffers[0]);
	int ldt = STARPU_COL_MATRIX_GET_LD(buffers[2]);

	double* A = (double*)STARPU_MATRIX_GET_PTR(buffers[0]);
	double* T = (double*)STARPU_MATRIX_GET_PTR(buffers[2]);

	double** workspace;
	starpu_codelet_unpack_args (args, &workspace);

	double* work = workspace[starpu_worker_get_id ()];

	dgelqt (m, n, ib, A, lda, T, ldt, work);
}

struct starpu_codelet dgelqt_codelet = {
	.cpu_funcs = { dgelqt_func },
	.cpu_funcs_name = { "dgelqt" },
	.nbuffers = 3,
	.modes = { STARPU_RW, STARPU_RW, STARPU_W },
	.name = "DGELQT"
};

void insert_dgelqt_task (data_handle_t upper_triangle_a_handle, data_handle_t lower_triangle_a_handle,
						 data_handle_t t_handle, double** workspace) {
	starpu_task_insert (
		&dgelqt_codelet,
		STARPU_RW, upper_triangle_a_handle,
		STARPU_RW, lower_triangle_a_handle,
		STARPU_W, t_handle,
		STARPU_VALUE, &workspace, sizeof (double**),
		0
	);
}
#elif defined (USE_OPENMP)
#include <omp.h>

void insert_dgelqt_task (data_handle_t upper_triangle_a_handle, data_handle_t lower_triangle_a_handle,
						 data_handle_t t_handle, double** workspace) {
	// Do not mark lower_triangle_a_handle as unused
	(void)lower_triangle_a_handle;
	
	#pragma omp task \
			depend(inout: *upper_triangle_a_handle.ptr) \
			depend(inout: *lower_triangle_a_handle.ptr) \
			depend(out: *t_handle.ptr)
	{
		double* work = workspace[omp_get_thread_num ()];

		dgelqt (
			upper_triangle_a_handle.height,
			upper_triangle_a_handle.width,
			t_handle.height,
			upper_triangle_a_handle.ptr,
			upper_triangle_a_handle.leading_dimension,
			t_handle.ptr,
			t_handle.leading_dimension,
			work
		);
	}
}
#endif
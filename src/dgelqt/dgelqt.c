#include "dgelqt.h"
#include "../dgemlqt/dgemlqt.h"
#include "../common/math_utils.h"
#include "../dlarfg/dlarfg.h"

#include <stdlib.h>
#include <memory.h>

#define A_ENTRY(i,j) A[i + (j) * lda]
#define T_ENTRY(i,j) T[i + (j) * ldt]
#define WORK_ENTRY(i) work[i]

#if defined (USE_MKL)
#include <mkl_cblas.h>
#define USE_EXTERNAL_BLAS
#elif defined (USE_ARMPL)
#include <armpl.h>
#define USE_EXTERNAL_BLAS
#endif

void dgelqt_inner_block (int m, int n, double* A, int lda, double* T, int ldt, double* work) {
	for (int reflector_index = 0; reflector_index < min (m, n); reflector_index++) {
		//Obtain the Householder reflector
		double* diagonal_entry = A + reflector_index * lda + reflector_index;
		dlarfg (n - reflector_index, diagonal_entry, diagonal_entry + lda, lda, T + reflector_index * ldt + reflector_index);

		//Propagate the reflector into following columns
		double tau = T[reflector_index * ldt + reflector_index];

		// work := A[(reflector_index+1):m, reflector_index:n] * v
		//
		// where v^T = ( 1    A[reflector_index, (reflector_index+1):n] )
		#ifdef USE_EXTERNAL_BLAS
			cblas_dcopy (m - reflector_index - 1, &A_ENTRY(reflector_index + 1, reflector_index), 1, work, 1);

			cblas_dgemv (CblasColMajor, CblasNoTrans, m - reflector_index - 1, n - reflector_index - 1, 1.0, &A_ENTRY(reflector_index + 1, reflector_index + 1), lda, &A_ENTRY(reflector_index, reflector_index + 1), lda, 1.0, work, 1);
		#else
			for (int work_index = 0; work_index < m - reflector_index - 1; work_index++) {
				WORK_ENTRY(work_index) = A_ENTRY(reflector_index + 1 + work_index, reflector_index);

				for (int v_index = 1; v_index < n - reflector_index; v_index++) {
					WORK_ENTRY(work_index) += A_ENTRY(reflector_index + 1 + work_index, reflector_index + v_index) * A_ENTRY(reflector_index, reflector_index + v_index);
				}
			}
		#endif

		// A[(reflector_index+1):m, reflector_index:n] = A[(reflector_index+1):m, reflector_index:n] - tau * work * v^T
		//
		// where v^T = ( 1    A[reflector_index, (reflector_index+1):n] )
		#ifdef USE_EXTERNAL_BLAS
			cblas_daxpy (m - reflector_index - 1, -tau, work, 1, &A_ENTRY(reflector_index + 1, reflector_index), 1);

			// Mistake probably here:
			cblas_dger (CblasColMajor, m - reflector_index - 1, n - reflector_index - 1, -tau, work, 1, &A_ENTRY(reflector_index, reflector_index + 1), lda, &A_ENTRY(reflector_index + 1, reflector_index + 1), lda);
		#else
			for (int row = 0; row < m - reflector_index - 1; row++) {
				A_ENTRY(reflector_index + 1 + row, reflector_index) -= tau * WORK_ENTRY(row);

				for (int col = 1; col < n - reflector_index; col++) {
					A_ENTRY(reflector_index + 1 + row, reflector_index + col) -= tau * A_ENTRY(reflector_index, reflector_index + col) * WORK_ENTRY(row);
				}
			}
		#endif
	}

	// Calculate the non-diagonal reflector factors in T
	for (int col = 1; col < m; col++) {
		double tau = T[col * ldt + col];

		// T[0:col,col] := -tau * A[0:col,(col+1):n] * A[col,(col+1):n]^T
		#ifdef USE_EXTERNAL_BLAS
			cblas_dcopy (col, &A_ENTRY(0, col), 1, &T_ENTRY(0, col), 1);

			cblas_dscal (col, -tau, &T_ENTRY(0, col), 1);

			cblas_dgemv (CblasColMajor, CblasNoTrans, col, n - col - 1, -tau, &A_ENTRY(0, col + 1), lda, &A_ENTRY(col, col + 1), lda, 1.0, &T_ENTRY(0, col), 1);
		#else
			for (int row = 0; row < col; row++) {
				T_ENTRY(row, col) = -tau * A_ENTRY(row, col);

				for (int k = col + 1; k < n; k++) {
					T_ENTRY(row, col) -= tau * A_ENTRY(row, k) * A_ENTRY(col, k);
				}
			}
		#endif

		// T[0:col,col] := T[0:col,0:col] * T[0:col,col]
		#ifdef USE_EXTERNAL_BLAS
			cblas_dtrmv (CblasColMajor, CblasUpper, CblasNoTrans, CblasNonUnit, col, T, ldt, &T_ENTRY(0, col), 1);
		#else
			for (int row = 0; row < col; row++) {
				T_ENTRY(row, col) *= T_ENTRY(row, row);

				for (int k = row + 1; k < col; k++) {
					T_ENTRY(row, col) += T_ENTRY(k, col) * T_ENTRY(row, k);
				}
			}
		#endif
	}
}

int dgelqt (int m, int n, int mb, double* A, int lda, double* T, int ldt, double* work) {
	// Check parameter values
	if (m < 0) {
		return 1;
	} else if (n < 0) {
		return 2;
	} else if (mb <= 0) {
		return 3;
	} else if (lda < max (1, m)) {
		return 5;
	} else if (ldt < max (1, mb)) {
		return 7;
	}

	for (int block_start = 0; block_start < min (m, n); block_start += mb) {
		int block_size = min (mb, min(m, n) - block_start);

		dgelqt_inner_block (block_size, n - block_start, &A_ENTRY(block_start, block_start), lda, &T_ENTRY(0, block_start), ldt, work);

		if (block_start + block_size < m) {
			dgemlqt_inner_block ('R', 'T', m - block_start - block_size, n - block_start, block_size, &A_ENTRY(block_start, block_start), lda, &T_ENTRY(0, block_start), ldt, &A_ENTRY(block_start + block_size, block_start), lda, work);
		}
	}

	return 0;
}
#include "dtsmlq.h"
#include "../common/value_checking.h"
#include "../common/math_utils.h"

#include <memory.h>

#define A1_ENTRY(i,j) A1[i + (j) * lda1]
#define A2_ENTRY(i,j) A2[i + (j) * lda2]
#define V_ENTRY(i,j) V[i + (j) * ldv]
#define T_ENTRY(i,j) T[i + (j) * ldt]
#define WORK_ENTRY(i,j) work[i + (j) * (IS_LEFT(side) ? k : m1)]

#if defined (USE_MKL)
#include <mkl_cblas.h>
#define USE_EXTERNAL_BLAS
#elif defined (USE_ARMPL)
#include <armpl.h>
#define USE_EXTERNAL_BLAS
#endif

void dtsmlq_inner_block (char side, char trans, int m1, int n1, int m2, int n2, int k, double* A1, int lda1, double* A2, int lda2, double* V, int ldv, double* T, int ldt, double* work) {
	if (IS_LEFT (side)) {
		int n = n1;

		// work := A1
		#ifdef USE_EXTERNAL_BLAS
			for (int col = 0; col < n; col++) {
				cblas_dcopy (k, &A1_ENTRY(0, col), 1, &WORK_ENTRY(0, col), 1);
			}
		#else
			for (int col = 0; col < n; col++) {
				memcpy (&WORK_ENTRY(0, col), &A1_ENTRY(0, col), k * sizeof (double));
			}
		#endif
		
		// work := work + V * A2
		#ifdef USE_EXTERNAL_BLAS
			cblas_dgemm (CblasColMajor, CblasNoTrans, CblasNoTrans, k, n, m2, 1.0, V, ldv, A2, lda2, 1.0, work, k);
		#else
			for (int row = 0; row < k; row++) {
				for (int col = 0; col < n; col++) {
					for (int l = 0; l < m2; l++) {
						WORK_ENTRY(row, col) += V_ENTRY(row, l) * A2_ENTRY(l, col);
					}
				}
			}
		#endif

		if (IS_NOT_TRANS (trans)) {
			// work := T^T * work
			#ifdef USE_EXTERNAL_BLAS
				cblas_dtrmm (CblasColMajor, CblasLeft, CblasUpper, CblasTrans, CblasNonUnit, k, n, 1.0, T, ldt, work, k);
			#else
				for (int col = 0; col < n; col++) {
					for (int row = k - 1; row >= 0; row--) {
						WORK_ENTRY(row, col) *= T_ENTRY(row, row);

						for (int l = row - 1; l >= 0; l--) {
							WORK_ENTRY(row, col) += T_ENTRY(l, row) * WORK_ENTRY(l, col);
						}
					}
				}
			#endif
		} else {
			// work := T * work
			#ifdef USE_EXTERNAL_BLAS
				cblas_dtrmm (CblasColMajor, CblasLeft, CblasUpper, CblasNoTrans, CblasNonUnit, k, n, 1.0, T, ldt, work, k);
			#else
				for (int col = 0; col < n; col++) {
					for (int row = 0; row < k; row++) {
						WORK_ENTRY(row, col) *= T_ENTRY(row, row);

						for (int l = row + 1; l < k; l++) {
							WORK_ENTRY(row, col) += T_ENTRY(row, l) * WORK_ENTRY(l, col);
						}
					}
				}
			#endif
		}

		// A1 := A1 - work
		#ifdef USE_EXTERNAL_BLAS
			for (int col = 0; col < n; col++) {
				cblas_daxpy (k, -1.0, &WORK_ENTRY(0, col), 1, &A1_ENTRY(0, col), 1);
			}
		#else
			for (int col = 0; col < n; col++) {
				for (int row = 0; row < k; row++) {
					A1_ENTRY(row, col) -= WORK_ENTRY(row, col);
				}
			}
		#endif

		// A2 := A2 - V^T * work
		#ifdef USE_EXTERNAL_BLAS
			cblas_dgemm (CblasColMajor, CblasTrans, CblasNoTrans, m2, n, k, -1.0, V, ldv, work, k, 1.0, A2, lda2);
		#else
			for (int col = 0; col < n; col++) {
				for (int row = 0; row < m2; row++) {
					for (int l = 0; l < k; l++) {
						A2_ENTRY(row, col) -= V_ENTRY(l, row) * WORK_ENTRY(l, col);
					}
				}
			}
		#endif
	} else {
		int m = m1;

		// work := A1
		#ifdef USE_EXTERNAL_BLAS
			for (int col = 0; col < k; col++) {
				cblas_dcopy (m, &A1_ENTRY(0, col), 1, &WORK_ENTRY(0, col), 1);
			}
		#else
			for (int col = 0; col < k; col++) {
				memcpy (&WORK_ENTRY(0, col), &A1_ENTRY(0, col), m * sizeof (double));
			}
		#endif

		// work := work + A2 * V^T
		#ifdef USE_EXTERNAL_BLAS
			cblas_dgemm (CblasColMajor, CblasNoTrans, CblasTrans, m, k, n2, 1.0, A2, lda2, V, ldv, 1.0, work, m);
		#else
			for (int col = 0; col < k; col++) {
				for (int l = 0; l < n2; l++) {
					for (int row = 0; row < m; row++) {
						WORK_ENTRY(row, col) += A2_ENTRY(row, l) * V_ENTRY(col, l);
					}
				}
			}
		#endif

		if (IS_NOT_TRANS (trans)) {
			// work := work * T^T
			#ifdef USE_EXTERNAL_BLAS
				cblas_dtrmm (CblasColMajor, CblasRight, CblasUpper, CblasTrans, CblasNonUnit, m, k, 1.0, T, ldt, work, m);
			#else
				for (int col = 0; col < k; col++) {
					for (int row = 0; row < m; row++) {
						WORK_ENTRY(row, col) *= T_ENTRY(col, col);

						for (int l = col + 1; l < k; l++) {
							WORK_ENTRY(row, col) += WORK_ENTRY(row, l) * T_ENTRY(col, l);
						}
					}
				}
			#endif
		} else {
			// work := work * T
			#ifdef USE_EXTERNAL_BLAS
				cblas_dtrmm (CblasColMajor, CblasRight, CblasUpper, CblasNoTrans, CblasNonUnit, m, k, 1.0, T, ldt, work, m);
			#else
				for (int row = 0; row < m; row++) {
					for (int col = k - 1; col >= 0; col--) {
						WORK_ENTRY(row, col) *= T_ENTRY(col, col);

						for (int l = col - 1; l >= 0; l--) {
							WORK_ENTRY(row, col) += WORK_ENTRY(row, l) * T_ENTRY(l, col);
						}
					}
				}
			#endif
		}

		// A1 := A1 - work
		#ifdef USE_EXTERNAL_BLAS
			for (int col = 0; col < k; col++) {
				cblas_daxpy (m, -1.0, &WORK_ENTRY(0, col), 1, &A1_ENTRY(0, col), 1);
			}
		#else
			for (int col = 0; col < k; col++) {
				for (int row = 0; row < m; row++) {
					A1_ENTRY(row, col) -= WORK_ENTRY(row, col);
				}
			}
		#endif

		// A2 := A2 - work * V
		#ifdef USE_EXTERNAL_BLAS
			cblas_dgemm (CblasColMajor, CblasNoTrans, CblasNoTrans, m, n2, k, -1.0, work, m, V, ldv, 1.0, A2, lda2);
		#else
			for (int col = 0; col < n2; col++) {
				for (int l = 0; l < k; l++) {
					for (int row = 0; row < m; row++) {
						A2_ENTRY(row, col) -= WORK_ENTRY(row, l) * V_ENTRY(l, col);
					}
				}
			}
		#endif
	}
}

int dtsmlq (char side, char trans, int m1, int n1, int m2, int n2, int k, int mb, double* A1, int lda1, double* A2, int lda2, double* V, int ldv, double* T, int ldt, double* work) {
	// Check parameter values
	if (!VALID_SIDE_VALUE (side)) {
		return 1;
	} else if (!VALID_TRANS_VALUE (trans)) {
		return 2;
	} else if (m1 < 0) {
		return 3;
	} else if (n1 < 0) {
		return 4;
	} else if (m2 < 0 || (IS_RIGHT (side) && m1 != m2)) {
		return 5;
	} else if (n2 < 0 || (IS_LEFT (side) && n1 != n2)) {
		return 6;
	} else if (k < 0) {
		return 7;
	} else if ((IS_LEFT (side) && k > m1) || (IS_RIGHT (side) && k > n1)) {
		return 7;
	} else if (mb <= 0) {
		return 8;
	} else if (lda1 < max (1, m1)) {
		return 10;
	} else if (lda2 < max (1, m2)) {
		return 12;
	} else if (ldv < max (1, k)) {
		return 14;
	} else if (ldt < max (1, mb)) {
		return 16;
	}

	if (IS_LEFT (side) && IS_NOT_TRANS (trans)) {
		for (int block_start = 0; block_start < k; block_start += mb) {
			int block_size = min (mb, k - block_start);

			dtsmlq_inner_block ('L', 'N', m1 - block_start, n1, m2, n2, block_size, &A1_ENTRY(block_start, 0), lda1, A2, lda2, &V_ENTRY(block_start, 0), ldv, &T_ENTRY(0, block_start), ldt, work);
		}
	} else if (IS_LEFT (side) && IS_TRANS (trans)) {
		for (int block_start = ((k - 1) / mb) * mb; block_start >= 0; block_start -= mb) {
			int block_size = min (mb, k - block_start);

			dtsmlq_inner_block ('L', 'T', m1 - block_start, n1, m2, n2, block_size, &A1_ENTRY(block_start, 0), lda1, A2, lda2, &V_ENTRY(block_start, 0), ldv, &T_ENTRY(0, block_start), ldt, work);
		}
	} else if (IS_RIGHT (side) && IS_NOT_TRANS (trans)) {
		for (int block_start = ((k - 1) / mb) * mb; block_start >= 0; block_start -= mb) {
			int block_size = min (mb, k - block_start);

			dtsmlq_inner_block ('R', 'N', m1, n1 - block_start, m2, n2, block_size, &A1_ENTRY(0, block_start), lda1, A2, lda2, &V_ENTRY(block_start, 0), ldv, &T_ENTRY(0, block_start), ldt, work);
		}
	} else {
		for (int block_start = 0; block_start < k; block_start += mb) {
			int block_size = min (mb, k - block_start);

			dtsmlq_inner_block ('R', 'T', m1, n1 - block_start, m2, n2, block_size, &A1_ENTRY(0, block_start), lda1, A2, lda2, &V_ENTRY(block_start, 0), ldv, &T_ENTRY(0, block_start), ldt, work);
		}
	}

	return 0;
}

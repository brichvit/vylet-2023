#ifndef DLACPY_TASK_H_
#define DLACPY_TASK_H_

#include "../common/runtime_system.h"

void insert_dlacpy_task (char uplo, data_handle_t a_handle, data_handle_t b_handle);

#endif /* DLACPY_TASK_H_ */
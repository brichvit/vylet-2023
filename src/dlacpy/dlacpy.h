#ifndef DLACPY_H_
#define DLACPY_H_

void dlacpy (char uplo, int m, int n, double* A, int lda, double* B, int ldb);

#endif /* DLACPY_H_ */

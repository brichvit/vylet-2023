#include "dlacpy.h"

#if defined (USE_MKL)
#include <mkl_lapacke.h>
#elif defined (USE_ARMPL)
#include <armpl.h>
#else
#include <memory.h>
#endif

void dlacpy (char uplo, int m, int n, double* A, int lda, double* B, int ldb) {
	#if defined (USE_MKL) || defined (USE_ARMPL)
	LAPACKE_dlacpy_work (LAPACK_COL_MAJOR, uplo, m, n, A, lda, B, ldb);
	#else
	for (int col = 0; col < n; col++) {
		memcpy (A + col * lda,
				B + col * ldb,
				m * sizeof (double));
	}
	#endif
}
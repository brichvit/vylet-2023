#include "dlacpy_task.h"
#include "dlacpy.h"

#if defined (USE_STARPU)
#include "../common/starpu_matrix_utils.h"

void dlacpy_func (void* buffers[], void* args) {
	int m = STARPU_COL_MATRIX_GET_NY(buffers[0]);
	int n = STARPU_COL_MATRIX_GET_NX(buffers[0]);

	int lda = STARPU_COL_MATRIX_GET_LD(buffers[0]);
	int ldb = STARPU_COL_MATRIX_GET_LD(buffers[1]);

	double* A = (double*)STARPU_MATRIX_GET_PTR(buffers[0]);
	double* B = (double*)STARPU_MATRIX_GET_PTR(buffers[1]);

	char uplo;
	starpu_codelet_unpack_args (args, &uplo);

	dlacpy (uplo, m, n, A, lda, B, ldb);
}

struct starpu_codelet dlacpy_codelet = {
	.cpu_funcs = { dlacpy_func },
	.cpu_funcs_name = { "dlacpy" },
	.nbuffers = 2,
	.modes = { STARPU_R, STARPU_W },
	.name = "DLACPY"
};

void insert_dlacpy_task (char uplo, data_handle_t a_handle, data_handle_t b_handle) {
	starpu_task_insert (
		&dlacpy_codelet,
		STARPU_R, a_handle,
		STARPU_W, b_handle,
		STARPU_VALUE, &uplo, sizeof (char),
		0
	);
}
#elif defined (USE_OPENMP)
#include <omp.h>

void insert_dlacpy_task (char uplo, data_handle_t a_handle, data_handle_t b_handle) {
	#pragma omp task \
			depend(in: *a_handle.ptr) \
			depend(out: *b_handle.ptr)
	{
		dlacpy (
			uplo,
			a_handle.height,
			a_handle.width,
			a_handle.ptr,
			a_handle.leading_dimension,
			b_handle.ptr,
			b_handle.leading_dimension
		);
	}
}
#endif
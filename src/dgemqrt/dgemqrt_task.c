#include "dgemqrt_task.h"
#include "dgemqrt.h"

#if defined (USE_STARPU)
#include "../common/starpu_matrix_utils.h"
#include "../common/math_utils.h"

void dgemqrt_func (void* buffers[], void* args) {
	int m = STARPU_COL_MATRIX_GET_NY(buffers[1]);
	int n = STARPU_COL_MATRIX_GET_NX(buffers[1]);
	int k = min (STARPU_COL_MATRIX_GET_NX(buffers[0]), STARPU_COL_MATRIX_GET_NY (buffers[0]));

	int ib = STARPU_COL_MATRIX_GET_NY(buffers[3]);

	int ldv = STARPU_COL_MATRIX_GET_LD(buffers[0]);
	int ldc = STARPU_COL_MATRIX_GET_LD(buffers[1]);
	int ldt = STARPU_COL_MATRIX_GET_LD(buffers[3]);

	double* V = (double*)STARPU_MATRIX_GET_PTR(buffers[0]);
	double* C = (double*)STARPU_MATRIX_GET_PTR(buffers[1]);
	double* T = (double*)STARPU_MATRIX_GET_PTR(buffers[3]);

	char side, trans;
	double** workspace;
	starpu_codelet_unpack_args (args, &side, &trans, &workspace);

	double* work = workspace[starpu_worker_get_id ()];

	dgemqrt (side, trans, m, n, k, ib, V, ldv, T, ldt, C, ldc, work);
}

struct starpu_codelet dgemqrt_codelet = {
	.cpu_funcs = { dgemqrt_func },
	.cpu_funcs_name = { "dgemqrt" },
	.nbuffers = 4,
	.modes = { STARPU_R, STARPU_RW, STARPU_RW, STARPU_R },
	.name = "DGEMQRT"
};

void insert_dgemqrt_task (char side, char trans, data_handle_t lower_triangle_v_handle,
						  data_handle_t upper_triangle_c_handle, data_handle_t lower_triangle_c_handle,
						  data_handle_t t_handle, double** workspace) {
	starpu_task_insert (
		&dgemqrt_codelet,
		STARPU_R, lower_triangle_v_handle,
		STARPU_RW, upper_triangle_c_handle,
		STARPU_RW, lower_triangle_c_handle,
		STARPU_R, t_handle,
		STARPU_VALUE, &side, sizeof(char),
		STARPU_VALUE, &trans, sizeof(char),
		STARPU_VALUE, &workspace, sizeof (double**),
		0
	);
}
#elif defined (USE_OPENMP)
#include "../common/math_utils.h"

#include <omp.h>

void insert_dgemqrt_task (char side, char trans, data_handle_t lower_triangle_v_handle,
						  data_handle_t upper_triangle_c_handle, data_handle_t lower_triangle_c_handle,
						  data_handle_t t_handle, double** workspace) {
	// Do not mark lower_triangle_c_handle as unused
	(void)lower_triangle_c_handle;

	#pragma omp task \
			depend(in: *lower_triangle_v_handle.ptr) \
			depend(inout: *upper_triangle_c_handle.ptr) \
			depend(inout: *lower_triangle_c_handle.ptr) \
			depend(in: *t_handle.ptr)
	{
		double* work = workspace[omp_get_thread_num ()];

		dgemqrt (
			side,
			trans,
			upper_triangle_c_handle.height,
			upper_triangle_c_handle.width,
			min (lower_triangle_v_handle.height, lower_triangle_v_handle.width),
			t_handle.height,
			lower_triangle_v_handle.ptr,
			lower_triangle_v_handle.leading_dimension,
			t_handle.ptr,
			t_handle.leading_dimension,
			upper_triangle_c_handle.ptr,
			upper_triangle_c_handle.leading_dimension,
			work
		);
	}
}
#endif
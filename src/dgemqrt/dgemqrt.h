#ifndef DGEMQRT_H_
#define DGEMQRT_H_

// TODO: document
void dgemqrt_inner_block (char side, char trans, int m, int n, int k, double* V, int ldv, double* T, int ldt, double* C, int ldc, double* work);

/**
 * @brief Overwrites the general real M-by-N matrix C with
 *
 *                 SIDE = 'L'     SIDE = 'R'
 * TRANS = 'N':      Q C            C Q
 * TRANS = 'T':   Q**T C            C Q**T
 *
 * where Q is a real orthogonal matrix defined as the product of K
 * elementary reflectors:
 *
 *       Q = H(1) H(2) . . . H(K) = I - V T V**T
 *
 * generated using the compact WY representation as returned by DGEQRT.
 *
 * Q is of order M if SIDE = 'L' and of order N  if SIDE = 'R'.
 * 
 * This is a C reimplementation of the DGEMQRT routine from LAPACK (see https://netlib.org/lapack/).
 * 
 * @param side The side from which to apply the matrix Q or Q^T (see above). Valid values are 'L', 'l', 'R' and 'r'.
 * @param trans The matrix to apply (either Q or Q^T, see above). Valid values are 'N', 'n', 'T' and 't'.
 * @param m The number of rows of the matrix C. @p m >= 0.
 * @param n The number of columns of the matrix C. @p n >= 0.
 * @param k The number of elementary reflectors whose product defines the matrix Q. If @p side = 'L', m >= k >= 0. If @p side = 'R', n >= k >= 0.
 * @param nb The block size used for the storage of T. @p k >= @p nb >= 1. This must be the same value of @p nb used to generate @p T in dgeqrt.
 * @param V The matrix whose i-th column contains the vector which defines the elementary reflector H(i) as returned by DGEQRT in the first @p k columns of its matrix argument A.
 * @param ldv The leading dimension of the matrix V. If @p side = 'L', lda >= max(1,m). If @p side = 'R', lda >= max(1,n).
 * @param T The upper triangular factors of the block reflectors as returned by dgeqrt, stored as a nb-by-k matrix.
 * @param ldt The leading dimension of the matrix T. @p ldt >= nb.
 * @param C The m-by-n matrix C. On exit, it is overwritten by Q*C, Q^T*C, C*Q^T ot C*Q (see above).
 * @param ldc The leading dimension of the matrix C. @p ldc >= max(1,m).
 * @param work A contiguously stored k-by-n (if @p side = 'L') or m-by-k (if @p side = 'R') workspace matrix.
 * @return int The number of the first argument with an invalid value. If all arguments have a valid value, 0 is returned.
 */
int dgemqrt (char side, char trans, int m, int n, int k, int nb, double* V, int ldv, double* T, int ldt, double* C, int ldc, double* work);

#endif /* DGEMQRT_H_ */
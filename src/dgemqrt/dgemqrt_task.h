#ifndef DGEMQRT_TASK_H_
#define DGEMQRT_TASK_H_

#include "../common/runtime_system.h"

void insert_dgemqrt_task (char side, char trans, data_handle_t lower_triangle_v_handle,
						  data_handle_t upper_triangle_c_handle, data_handle_t lower_triangle_c_handle,
						  data_handle_t t_handle, double** workspace);

#endif /* DGEMQRT_TASK_H_ */
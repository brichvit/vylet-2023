#ifndef DTTLQT_TASK_H_
#define DTTLQT_TASK_H_

#include "../common/runtime_system.h"

void insert_dttlqt_task (data_handle_t lower_triangle_a1_handle, data_handle_t lower_triangle_a2_handle,
						 data_handle_t t_handle, double** workspace);

#endif /* DTTLQT_TASK_H_ */
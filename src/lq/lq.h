#ifndef LQ_H_
#define LQ_H_

#include "../common/elimination.h"
#include "../common/runtime_system.h"

int pdgelqf (int m, int n, double* A, int lda, double* T, int ldt, config_t* config, elim_context_t* context);

int pdgemlq(char side, char trans, int m, int n, int k, double* A, int lda, double* T, int ldt, double* C, int ldc, elim_context_t context);

void lq_register_T_handles (int m, int n, int mb, int ib, double* T, int ldt, data_handle_t* T_handles, elim_scheme_e elim_scheme);

void lq_deregister_T_handles (int t_handles_height, int t_handles_width, data_handle_t* T_handles, elim_scheme_e elim_scheme);

#endif /* LQ_H_ */

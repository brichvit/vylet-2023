#include "lq.h"

#include "../dgelqt/dgelqt_task.h"
#include "../dgemlqt/dgemlqt_task.h"
#include "../dtslqt/dtslqt_task.h"
#include "../dtsmlq/dtsmlq_task.h"
#include "../dttlqt/dttlqt_task.h"
#include "../dttmlq/dttmlq_task.h"

#include "../common/math_utils.h"
#include "../common/matrix_access.h"
#include "../common/matrix_utils.h"
#include "../common/value_checking.h"
#include "../common/workspace.h"

#include <stdlib.h>

void lq_register_T_handles (int m, int n, int mb, int ib, double* T, int ldt, data_handle_t* T_handles, elim_scheme_e elim_scheme) {
	int t_handles_width = (n - 1) / mb + 1;
	int t_handles_height = min ((m - 1) / mb + 1, t_handles_width);

	for (int col_start = 0; col_start < n; col_start += mb) {
		int block_col = col_start / mb;

		for (int row_start = 0; row_start < min (m, col_start + 1); row_start += mb) {
			int block_height = min (min (m, n) - row_start, mb);
			int block_row = row_start / mb;

			RUNTIME_SYSTEM_REGISTER_HANDLE (
				&T_HANDLES_ENTRY(block_row, block_col),
				&T_ENTRY(0, mb * block_row + min (m, n) * block_col),
				ib,
				block_height,
				ldt
			);

			if (get_t_size_multiplier_for_elim_scheme (elim_scheme) == 2) {
				RUNTIME_SYSTEM_REGISTER_HANDLE (
					&T2_HANDLES_ENTRY(block_row, block_col),
					&T_ENTRY(0, min (m, n) * t_handles_width + mb * block_row + min (m, n) * block_col),
					ib,
					block_height,
					ldt
				);
			}
		}
	}
}

void lq_deregister_T_handles (int t_handles_height, int t_handles_width, data_handle_t* T_handles, elim_scheme_e elim_scheme) {
	for (int block_col = 0; block_col < t_handles_width; block_col++) {
		for (int block_row = 0; block_row < min (t_handles_height, block_col + 1); block_row++) {
			RUNTIME_SYSTEM_UNREGISTER_HANDLE (T_HANDLES_ENTRY(block_row, block_col));
			if (get_t_size_multiplier_for_elim_scheme (elim_scheme) == 2) {
				RUNTIME_SYSTEM_UNREGISTER_HANDLE (T2_HANDLES_ENTRY(block_row, block_col));
			}
		}
	}
}

int pdgelqf (int m, int n, double* A, int lda, double* T, int ldt, config_t* config, elim_context_t* context) {
	// Get relevant config options
	int mb = get_nb_from_config (config);
	int ib = get_ib_from_config (config);
	elim_scheme_e elim_scheme = get_elim_scheme_from_config (config);
	int layout_translation = get_layout_translation_from_config (config);

	// Check parameter values
	if (m < 0) {
		return 1;
	} else if (n < 0) {
		return 2;
	} else if (lda < max (1, m)) {
		return 4;
	} else if (ldt < max (1, ib)) {
		return 6;
	} else if (config && !is_config_valid (*config)) {
		return 7;
	}

	// Initialize the runtime system
	int runtime_system_already_initialized = RUNTIME_SYSTEM_IS_INITIALIZED ();
	if (!runtime_system_already_initialized)
		RUNTIME_SYSTEM_INIT ();

	// If layout translation is enabled, allocate the blockwise matrix
	double* orig_A = NULL;
	if (layout_translation) {
		orig_A = A;
		A = malloc (m * n * sizeof(double));
	}

	int no_blocks_x = (n - 1) / mb + 1;
	int no_blocks_y = (m - 1) / mb + 1;

	int a_handles_height = no_blocks_y, a_handles_width = no_blocks_x;
	int t_handles_height = min (no_blocks_y, no_blocks_x), t_handles_width = no_blocks_x;

	// If the user passed a context for subsequent PDGEMLQ calls, set its attributes to the used values
	if (context != NULL) {
		if (config) {
			context->config = *config;
		} else {
			init_config (&context->config);
		}

		if (is_elim_scheme_superblock_based (context->config.elim_scheme)) {
			context->a_handles_height = a_handles_width;
			context->a_handles_width = a_handles_height;

			context->num_threads = RUNTIME_SYSTEM_GET_NO_THREADS ();
		}
	}

	// Allocate the handle matrices
	data_handle_t* A_upper_triangle_handles = malloc (a_handles_height * a_handles_width * sizeof (data_handle_t));
	data_handle_t* A_lower_triangle_handles = malloc (a_handles_height * a_handles_width * sizeof (data_handle_t));
	data_handle_t* T_handles = malloc (get_t_size_multiplier_for_elim_scheme (elim_scheme) * t_handles_height * t_handles_width * sizeof (data_handle_t));
	data_handle_t* orig_A_handles = NULL;
	
	// Register the handles
	if (layout_translation) {
		register_full_matrix_handles (m, n, mb, A, m, A_upper_triangle_handles, BlockWiseLayout);
		register_full_matrix_handles (m, n, mb, A, m, A_lower_triangle_handles, BlockWiseLayout);
		orig_A_handles = malloc (a_handles_height * a_handles_width * sizeof (data_handle_t));
		register_full_matrix_handles (m, n, mb, orig_A, lda, orig_A_handles, ColumnWiseLayout);
	} else {
		register_full_matrix_handles (m, n, mb, A, lda, A_upper_triangle_handles, ColumnWiseLayout);
		register_full_matrix_handles (m, n, mb, A, lda, A_lower_triangle_handles, ColumnWiseLayout);
	}
	lq_register_T_handles (m, n, mb, ib, T, ldt, T_handles, elim_scheme);
	
	// Obtain the elimination steps
	int no_steps = get_no_steps_for_elim_scheme (a_handles_width, a_handles_height, *context);
	elim_step_t* steps = malloc(no_steps * sizeof(elim_step_t));
	generate_steps_for_elim_scheme (steps, a_handles_width, a_handles_height, *context);

	// Allocate the workspace
	double** workspace;
	create_workspace (&workspace, mb, ib);

	// Copy the columnwise matrix to the blockwise matrix if layout translation is enabled
	if (layout_translation) {
		copy_full_matrix_nonblocking (a_handles_height, a_handles_width, orig_A_handles, A_lower_triangle_handles);
	}

	// Go through the QR elimination steps
	for (int step_index = 0; step_index < no_steps; step_index++) {
		elim_step_t step = steps[step_index];

		if (step.kernel_type == GeneralKernel) {
			insert_dgelqt_task (
				A_UPPER_TRIANGLE_HANDLES_ENTRY(step.col, step.eliminated_row),
				A_LOWER_TRIANGLE_HANDLES_ENTRY(step.col, step.eliminated_row),
				T_HANDLES_ENTRY(step.col, step.eliminated_row),
				workspace
			);

			for (int updated_row = step.col + 1; updated_row < no_blocks_y; updated_row++) {
				insert_dgemlqt_task (
					'R',
					'T',
					A_UPPER_TRIANGLE_HANDLES_ENTRY(step.col, step.eliminated_row),
					A_UPPER_TRIANGLE_HANDLES_ENTRY(updated_row, step.eliminated_row),
					A_LOWER_TRIANGLE_HANDLES_ENTRY(updated_row, step.eliminated_row),
					T_HANDLES_ENTRY(step.col, step.eliminated_row),
					workspace
				);
			}
		} else if (step.kernel_type == TsKernel) {
			insert_dtslqt_task (
				A_LOWER_TRIANGLE_HANDLES_ENTRY(step.col, step.pivot_row),
				A_UPPER_TRIANGLE_HANDLES_ENTRY(step.col, step.eliminated_row),
				A_LOWER_TRIANGLE_HANDLES_ENTRY(step.col, step.eliminated_row),
				T_HANDLES_ENTRY(step.col, step.eliminated_row),
				workspace
			);

			for (int updated_row = step.col + 1; updated_row < no_blocks_y; updated_row++) {
				insert_dtsmlq_task (
					'R',
					'T',
					A_UPPER_TRIANGLE_HANDLES_ENTRY(step.col, step.eliminated_row),
					A_LOWER_TRIANGLE_HANDLES_ENTRY(step.col, step.eliminated_row),
					A_UPPER_TRIANGLE_HANDLES_ENTRY(updated_row, step.pivot_row),
					A_LOWER_TRIANGLE_HANDLES_ENTRY(updated_row, step.pivot_row),
					A_UPPER_TRIANGLE_HANDLES_ENTRY(updated_row, step.eliminated_row),
					A_LOWER_TRIANGLE_HANDLES_ENTRY(updated_row, step.eliminated_row),
					T_HANDLES_ENTRY(step.col, step.eliminated_row),
					workspace
				);
			}
		} else if (step.kernel_type == TtKernel) {
			insert_dttlqt_task (
				A_LOWER_TRIANGLE_HANDLES_ENTRY(step.col, step.pivot_row),
				A_LOWER_TRIANGLE_HANDLES_ENTRY(step.col, step.eliminated_row),
				T2_HANDLES_ENTRY(step.col, step.eliminated_row),
				workspace
			);

			for (int updated_row = step.col + 1; updated_row < no_blocks_y; updated_row++) {
				insert_dttmlq_task (
					'R',
					'T',
					A_LOWER_TRIANGLE_HANDLES_ENTRY(step.col, step.eliminated_row),
					A_UPPER_TRIANGLE_HANDLES_ENTRY(updated_row, step.pivot_row),
					A_LOWER_TRIANGLE_HANDLES_ENTRY(updated_row, step.pivot_row),
					A_UPPER_TRIANGLE_HANDLES_ENTRY(updated_row, step.eliminated_row),
					A_LOWER_TRIANGLE_HANDLES_ENTRY(updated_row, step.eliminated_row),
					T2_HANDLES_ENTRY(step.col, step.eliminated_row),
					workspace
				);
			}
		}
	}

	// Copy the blockwise matrix back to the columnwise matrix if layout translation is enabled
	if (layout_translation) {
		copy_full_matrix_nonblocking (a_handles_height, a_handles_width, A_lower_triangle_handles, orig_A_handles);
	}

	// Wait for all tasks to finish
	RUNTIME_SYSTEM_WAIT_FOR_ALL_TASKS ();

	// Deallocate the workspace
	destroy_workspace (workspace);

	// Deregister the handles
	deregister_full_matrix_handles (a_handles_height, a_handles_width, A_upper_triangle_handles);
	deregister_full_matrix_handles (a_handles_height, a_handles_width, A_lower_triangle_handles);
	lq_deregister_T_handles (t_handles_height, t_handles_width, T_handles, elim_scheme);

	if (layout_translation) {
		deregister_full_matrix_handles (a_handles_height, a_handles_width, orig_A_handles);
	}

	// Free the handle matrices
	free (A_upper_triangle_handles);
	free (A_lower_triangle_handles);
	free (T_handles);

	if (layout_translation) {
		free (A);
		free (orig_A_handles);
	}

	// Free the allocated elimination steps list
	free (steps);

	// Shutdown the runtime system if it wasn't initialized prior to calling this function
	if (!runtime_system_already_initialized) {
		RUNTIME_SYSTEM_SHUTDOWN ();
	}

	return 0;
}

int pdgemlq(char side, char trans, int m, int n, int k, double* A, int lda, double* T, int ldt, double* C, int ldc, elim_context_t context) {
	// Get relevant config options
	int mb = context.config.nb;
	int ib = context.config.ib;
	elim_scheme_e elim_scheme = context.config.elim_scheme;
	int layout_translation = context.config.layout_translation;

	// Check parameter values
	if (!VALID_SIDE_VALUE (side)) {
		return 1;
	} else if (!VALID_TRANS_VALUE (trans)) {
		return 2;
	} else if (m < 0) {
		return 3;
	} else if (n < 0) {
		return 4;
	} else if (k < 0) {
		return 5;
	} else if ((IS_LEFT (side) && k > m) || (IS_RIGHT (side) && k > n)) {
		return 5;
	} else if (lda < max (1, k)) {
		return 7;
	} else if (ldt < max (1, ib)) {
		return 9;
	} else if (ldc < max (1, m)) {
		return 11;
	} else if (!is_config_valid (context.config)) {
		return 12;
	} else if (is_elim_scheme_superblock_based (context.config.elim_scheme) && (context.a_handles_height < 0 || context.a_handles_width < 0 || context.num_threads <= 0)) {
		return 12;
	}

	// Quick return if possible
	if (m == 0 || n == 0 || k == 0) {
		return 0;
	}

	// Initialize the runtime system
	int runtime_system_already_initialized = RUNTIME_SYSTEM_IS_INITIALIZED ();
	if (!runtime_system_already_initialized) {
		RUNTIME_SYSTEM_INIT ();
	}

	// If layout translation is enabled, allocate the blockwise matrices
	double* orig_A = NULL;
	double* orig_C = NULL;
	
	if (layout_translation) {
		orig_A = A;
		if (IS_LEFT(side)) {
			A = malloc (k * m * sizeof (double));
		} else {
			A = malloc (k * n * sizeof (double));
		}

		orig_C = C;
		C = malloc (m * n * sizeof (double));
	}

	int no_blocks_x = (n - 1) / mb + 1;
	int no_blocks_y = (m - 1) / mb + 1;

	int a_handles_height, a_handles_width;
	int c_handles_height = no_blocks_y;
	int c_handles_width = no_blocks_x;

	// Allocate the handle matrices
	data_handle_t* A_handles;
	data_handle_t* T_handles;
	data_handle_t* C_handles = malloc (c_handles_height * c_handles_width * sizeof (data_handle_t));
	data_handle_t* orig_A_handles;
	data_handle_t* orig_C_handles;

	if (IS_LEFT(side)) {
		a_handles_height = (k - 1) / mb + 1;
		a_handles_width = no_blocks_y;
	} else {
		a_handles_height = (k - 1) / mb + 1;
		a_handles_width = no_blocks_x;
	}

	int t_handles_height = (k - 1) / mb + 1;
	int t_handles_width = a_handles_width;

	A_handles = malloc (a_handles_height * a_handles_width * sizeof (data_handle_t));
	T_handles = malloc (get_t_size_multiplier_for_elim_scheme (elim_scheme) * t_handles_height * t_handles_width * sizeof (data_handle_t));

	if (layout_translation) {
		orig_A_handles = malloc (a_handles_height * a_handles_width * sizeof (data_handle_t));
		orig_C_handles = malloc (c_handles_height * c_handles_width * sizeof (data_handle_t));
	}

	// Register the handles
	if (layout_translation) {
		if (IS_LEFT(side)) {
			register_full_matrix_handles (k, m, mb, A, k, A_handles, BlockWiseLayout);
			lq_register_T_handles (k, m, mb, ib, T, ldt, T_handles, elim_scheme);
			register_full_matrix_handles (k, m, mb, orig_A, lda, orig_A_handles, ColumnWiseLayout);
		} else {
			register_full_matrix_handles (k, n, mb, A, k, A_handles, BlockWiseLayout);
			lq_register_T_handles (k, n, mb, ib, T, ldt, T_handles, elim_scheme);
			register_full_matrix_handles (k, n, mb, orig_A, lda, orig_A_handles, ColumnWiseLayout);
		}

		register_full_matrix_handles (m, n, mb, C, m, C_handles, BlockWiseLayout);
		register_full_matrix_handles (m, n, mb, orig_C, ldc, orig_C_handles, ColumnWiseLayout);
	} else {
		if (IS_LEFT(side)) {
			register_full_matrix_handles (k, m, mb, A, lda, A_handles, ColumnWiseLayout);
			lq_register_T_handles (k, m, mb, ib, T, ldt, T_handles, elim_scheme);
		} else {
			register_full_matrix_handles (k, n, mb, A, lda, A_handles, ColumnWiseLayout);
			lq_register_T_handles (k, n, mb, ib, T, ldt, T_handles, elim_scheme);
		}

		register_full_matrix_handles (m, n, mb, C, ldc, C_handles, ColumnWiseLayout);
	}

	// Obtain the elimination steps
	int no_steps = get_no_steps_for_elim_scheme (a_handles_width, a_handles_height, context);
	elim_step_t* steps = malloc(no_steps * sizeof(elim_step_t));
	generate_steps_for_elim_scheme (steps, a_handles_width, a_handles_height, context);

	// Allocate the workspace
	double** workspace;
	create_workspace (&workspace, mb, ib);

	// Copy the columnwise matrices to the blockwise matrices if layout translation is enabled
	if (layout_translation) {
		copy_full_matrix_nonblocking (a_handles_height, a_handles_width, orig_A_handles, A_handles);
		copy_full_matrix_nonblocking (c_handles_height, c_handles_width, orig_C_handles, C_handles);
	}

	// Go through the QR elimination steps
	if (IS_LEFT(side)) {
		int step_index_start, step_index_end, step_index_increment;
		if (IS_NOT_TRANS(trans)) {
			step_index_start = 0;
			step_index_end = no_steps;
			step_index_increment = 1;
		} else {
			step_index_start = no_steps - 1;
			step_index_end = -1;
			step_index_increment = -1;
		}

		for (int step_index = step_index_start; step_index != step_index_end; step_index += step_index_increment) {
			elim_step_t step = steps[step_index];

			if (step.kernel_type == GeneralKernel) {
				for (int updated_col = 0; updated_col < c_handles_width; updated_col++) {
					insert_dgemlqt_task (
						side,
						trans,
						A_HANDLES_ENTRY(step.col, step.eliminated_row),
						C_HANDLES_ENTRY(step.eliminated_row, updated_col),
						C_HANDLES_ENTRY(step.eliminated_row, updated_col),
						T_HANDLES_ENTRY(step.col, step.eliminated_row),
						workspace
					);
				}
			} else if (step.kernel_type == TsKernel) {
				for (int updated_col = 0; updated_col < c_handles_width; updated_col++) {
					insert_dtsmlq_task (
						side,
						trans,
						A_HANDLES_ENTRY(step.col, step.eliminated_row),
						A_HANDLES_ENTRY(step.col, step.eliminated_row),
						C_HANDLES_ENTRY(step.pivot_row, updated_col),
						C_HANDLES_ENTRY(step.pivot_row, updated_col),
						C_HANDLES_ENTRY(step.eliminated_row, updated_col),
						C_HANDLES_ENTRY(step.eliminated_row, updated_col),
						T_HANDLES_ENTRY(step.col, step.eliminated_row),
						workspace
					);
				}
			} else if (step.kernel_type == TtKernel) {
				for (int updated_col = 0; updated_col < c_handles_width; updated_col++) {
					insert_dttmlq_task (
						side,
						trans,
						A_HANDLES_ENTRY(step.col, step.eliminated_row),
						C_HANDLES_ENTRY(step.pivot_row, updated_col),
						C_HANDLES_ENTRY(step.pivot_row, updated_col),
						C_HANDLES_ENTRY(step.eliminated_row, updated_col),
						C_HANDLES_ENTRY(step.eliminated_row, updated_col),
						T2_HANDLES_ENTRY(step.col, step.eliminated_row),
						workspace
					);
				}
			}
		}
	} else {
		int step_index_start, step_index_end, step_index_increment;
		if (IS_TRANS(trans)) {
			step_index_start = 0;
			step_index_end = no_steps;
			step_index_increment = 1;
		} else {
			step_index_start = no_steps - 1;
			step_index_end = -1;
			step_index_increment = -1;
		}

		for (int step_index = step_index_start; step_index != step_index_end; step_index += step_index_increment) {
			elim_step_t step = steps[step_index];

			if (step.kernel_type == GeneralKernel) {
				for (int updated_row = 0; updated_row < c_handles_height; updated_row++) {
					insert_dgemlqt_task (
						side,
						trans,
						A_HANDLES_ENTRY(step.col, step.eliminated_row),
						C_HANDLES_ENTRY(updated_row, step.eliminated_row),
						C_HANDLES_ENTRY(updated_row, step.eliminated_row),
						T_HANDLES_ENTRY(step.col, step.eliminated_row),
						workspace
					);
				}
			} else if (step.kernel_type == TsKernel) {
				for (int updated_row = 0; updated_row < c_handles_height; updated_row++) {
					insert_dtsmlq_task (
						side,
						trans,
						A_HANDLES_ENTRY(step.col, step.eliminated_row),
						A_HANDLES_ENTRY(step.col, step.eliminated_row),
						C_HANDLES_ENTRY(updated_row, step.pivot_row),
						C_HANDLES_ENTRY(updated_row, step.pivot_row),
						C_HANDLES_ENTRY(updated_row, step.eliminated_row),
						C_HANDLES_ENTRY(updated_row, step.eliminated_row),
						T_HANDLES_ENTRY(step.col, step.eliminated_row),
						workspace
					);
				}
			} else if (step.kernel_type == TtKernel) {
				for (int updated_row = 0; updated_row < c_handles_height; updated_row++) {
					insert_dttmlq_task (
						side,
						trans,
						A_HANDLES_ENTRY(step.col, step.eliminated_row),
						C_HANDLES_ENTRY(updated_row, step.pivot_row),
						C_HANDLES_ENTRY(updated_row, step.pivot_row),
						C_HANDLES_ENTRY(updated_row, step.eliminated_row),
						C_HANDLES_ENTRY(updated_row, step.eliminated_row),
						T2_HANDLES_ENTRY(step.col, step.eliminated_row),
						workspace
					);
				}
			}
		}
	}

	// Copy the blockwise matrix C back to the columnwise matrix C if layout translation is enabled
	if (layout_translation) {
		copy_full_matrix_nonblocking (c_handles_height, c_handles_width, C_handles, orig_C_handles);
	}

	// Wait for all tasks to finish
	RUNTIME_SYSTEM_WAIT_FOR_ALL_TASKS ();

	// Deallocate the workspace
	destroy_workspace (workspace);

	// Deregister the handles
	deregister_full_matrix_handles (a_handles_height, a_handles_width, A_handles);
	lq_deregister_T_handles (t_handles_height, t_handles_width, T_handles, elim_scheme);
	deregister_full_matrix_handles (c_handles_height, c_handles_width, C_handles);

	if (layout_translation) {
		deregister_full_matrix_handles (a_handles_height, a_handles_width, orig_A_handles);
		deregister_full_matrix_handles (c_handles_height, c_handles_width, orig_C_handles);
	}

	// Free the handle matrices
	free (A_handles);
	free (T_handles);
	free (C_handles);

	if (layout_translation) {
		free (A);
		free (C);
		free (orig_A_handles);
		free (orig_C_handles);
	}

	// Free the allocated elimination steps list
	free (steps);

	// Shutdown the runtime system if it wasn't initialized prior to calling this function
	if (!runtime_system_already_initialized) {
		RUNTIME_SYSTEM_SHUTDOWN ();
	}

	return 0;
}

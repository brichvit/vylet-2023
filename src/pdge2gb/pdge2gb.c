#include "pdge2gb.h"
#include "../qr/qr.h"
#include "../lq/lq.h"

#include "../dgeqrt/dgeqrt_task.h"
#include "../dgemqrt/dgemqrt_task.h"
#include "../dtsqrt/dtsqrt_task.h"
#include "../dtsmqr/dtsmqr_task.h"
#include "../dttqrt/dttqrt_task.h"
#include "../dttmqr/dttmqr_task.h"
#include "../dgelqt/dgelqt_task.h"
#include "../dgemlqt/dgemlqt_task.h"
#include "../dtslqt/dtslqt_task.h"
#include "../dtsmlq/dtsmlq_task.h"
#include "../dttlqt/dttlqt_task.h"
#include "../dttmlq/dttmlq_task.h"

#include "../common/math_utils.h"
#include "../common/matrix_access.h"
#include "../common/matrix_utils.h"
#include "../common/workspace.h"

#include <stdlib.h>

void qr_eliminate_column_with_offset (elim_step_t* qr_steps, int no_qr_steps, int* qr_step_index,
									  data_handle_t* A_upper_triangle_handles, data_handle_t* A_lower_triangle_handles,
									  int a_handles_height, int a_handles_width,
									  data_handle_t* TU_handles, int tu_handles_height, int tu_handles_width,
									  double** workspace, int row_offset) {
	if (*qr_step_index < no_qr_steps) {
		int qr_eliminated_column = qr_steps[*qr_step_index].col;

		while (*qr_step_index < no_qr_steps && qr_steps[*qr_step_index].col == qr_eliminated_column) {
			elim_step_t step = qr_steps[*qr_step_index];

			if (step.kernel_type == GeneralKernel) {
				insert_dgeqrt_task (
					A_UPPER_TRIANGLE_HANDLES_ENTRY(step.eliminated_row + row_offset, step.col),
					A_LOWER_TRIANGLE_HANDLES_ENTRY(step.eliminated_row + row_offset, step.col),
					TU_HANDLES_ENTRY(step.eliminated_row, step.col),
					workspace
				);

				for (int updated_col = step.col + 1; updated_col < a_handles_width; updated_col++) {
					insert_dgemqrt_task (
						'L',
						'T',
						A_LOWER_TRIANGLE_HANDLES_ENTRY(step.eliminated_row + row_offset, step.col),
						A_UPPER_TRIANGLE_HANDLES_ENTRY(step.eliminated_row + row_offset, updated_col),
						A_LOWER_TRIANGLE_HANDLES_ENTRY(step.eliminated_row + row_offset, updated_col),
						TU_HANDLES_ENTRY(step.eliminated_row, step.col),
						workspace
					);
				}
			} else if (step.kernel_type == TsKernel) {
				insert_dtsqrt_task (
					A_UPPER_TRIANGLE_HANDLES_ENTRY(step.pivot_row + row_offset, step.col),
					A_UPPER_TRIANGLE_HANDLES_ENTRY(step.eliminated_row + row_offset, step.col),
					A_LOWER_TRIANGLE_HANDLES_ENTRY(step.eliminated_row + row_offset, step.col),
					TU_HANDLES_ENTRY(step.eliminated_row, step.col),
					workspace
				);

				for (int updated_col = step.col + 1; updated_col < a_handles_width; updated_col++) {
					insert_dtsmqr_task (
						'L',
						'T',
						A_UPPER_TRIANGLE_HANDLES_ENTRY(step.eliminated_row + row_offset, step.col),
						A_LOWER_TRIANGLE_HANDLES_ENTRY(step.eliminated_row + row_offset, step.col),
						A_UPPER_TRIANGLE_HANDLES_ENTRY(step.pivot_row + row_offset, updated_col),
						A_LOWER_TRIANGLE_HANDLES_ENTRY(step.pivot_row + row_offset, updated_col),
						A_UPPER_TRIANGLE_HANDLES_ENTRY(step.eliminated_row + row_offset, updated_col),
						A_LOWER_TRIANGLE_HANDLES_ENTRY(step.eliminated_row + row_offset, updated_col),
						TU_HANDLES_ENTRY(step.eliminated_row, step.col),
						workspace
					);
				}
			} else if (step.kernel_type == TtKernel) {
				insert_dttqrt_task (
					A_UPPER_TRIANGLE_HANDLES_ENTRY(step.pivot_row + row_offset, step.col),
					A_UPPER_TRIANGLE_HANDLES_ENTRY(step.eliminated_row + row_offset, step.col),
					TU2_HANDLES_ENTRY(step.eliminated_row, step.col),
					workspace
				);

				for (int updated_col = step.col + 1; updated_col < a_handles_width; updated_col++) {
					insert_dttmqr_task (
						'L',
						'T',
						A_UPPER_TRIANGLE_HANDLES_ENTRY(step.eliminated_row + row_offset, step.col),
						A_UPPER_TRIANGLE_HANDLES_ENTRY(step.pivot_row + row_offset, updated_col),
						A_LOWER_TRIANGLE_HANDLES_ENTRY(step.pivot_row + row_offset, updated_col),
						A_UPPER_TRIANGLE_HANDLES_ENTRY(step.eliminated_row + row_offset, updated_col),
						A_LOWER_TRIANGLE_HANDLES_ENTRY(step.eliminated_row + row_offset, updated_col),
						TU2_HANDLES_ENTRY(step.eliminated_row, step.col),
						workspace
					);
				}
			}
			
			(*qr_step_index)++;
		}
	}
}

void lq_eliminate_row_with_offset (elim_step_t* lq_steps, int no_lq_steps, int* lq_step_index,
								   data_handle_t* A_upper_triangle_handles, data_handle_t* A_lower_triangle_handles,
								   int a_handles_height, int a_handles_width,
								   data_handle_t* TV_handles, int tv_handles_height, int tv_handles_width,
								   double** workspace, int column_offset) {
	// Do not mark a_handles_width as unused
	(void)a_handles_width;

	if (*lq_step_index < no_lq_steps) {
		int lq_eliminated_row = lq_steps[*lq_step_index].col;

		while (*lq_step_index < no_lq_steps && lq_steps[*lq_step_index].col == lq_eliminated_row) {
			elim_step_t step = lq_steps[*lq_step_index];

			if (step.kernel_type == GeneralKernel) {
				insert_dgelqt_task (
					A_UPPER_TRIANGLE_HANDLES_ENTRY(step.col, step.eliminated_row + column_offset),
					A_LOWER_TRIANGLE_HANDLES_ENTRY(step.col, step.eliminated_row + column_offset),
					TV_HANDLES_ENTRY(step.col, step.eliminated_row),
					workspace
				);

				for (int updated_row = step.col + 1; updated_row < a_handles_height; updated_row++) {
					insert_dgemlqt_task (
						'R',
						'T',
						A_UPPER_TRIANGLE_HANDLES_ENTRY(step.col, step.eliminated_row + column_offset),
						A_UPPER_TRIANGLE_HANDLES_ENTRY(updated_row, step.eliminated_row + column_offset),
						A_LOWER_TRIANGLE_HANDLES_ENTRY(updated_row, step.eliminated_row + column_offset),
						TV_HANDLES_ENTRY(step.col, step.eliminated_row),
						workspace
					);
				}
			} else if (step.kernel_type == TsKernel) {
				insert_dtslqt_task (
					A_LOWER_TRIANGLE_HANDLES_ENTRY(step.col, step.pivot_row + column_offset),
					A_UPPER_TRIANGLE_HANDLES_ENTRY(step.col, step.eliminated_row + column_offset),
					A_LOWER_TRIANGLE_HANDLES_ENTRY(step.col, step.eliminated_row + column_offset),
					TV_HANDLES_ENTRY(step.col, step.eliminated_row),
					workspace
				);

				for (int updated_row = step.col + 1; updated_row < a_handles_height; updated_row++) {
					insert_dtsmlq_task (
						'R',
						'T',
						A_UPPER_TRIANGLE_HANDLES_ENTRY(step.col, step.eliminated_row + column_offset),
						A_LOWER_TRIANGLE_HANDLES_ENTRY(step.col, step.eliminated_row + column_offset),
						A_UPPER_TRIANGLE_HANDLES_ENTRY(updated_row, step.pivot_row + column_offset),
						A_LOWER_TRIANGLE_HANDLES_ENTRY(updated_row, step.pivot_row + column_offset),
						A_UPPER_TRIANGLE_HANDLES_ENTRY(updated_row, step.eliminated_row + column_offset),
						A_LOWER_TRIANGLE_HANDLES_ENTRY(updated_row, step.eliminated_row + column_offset),
						TV_HANDLES_ENTRY(step.col, step.eliminated_row),
						workspace
					);
				}
			} else if (step.kernel_type == TtKernel) {
				insert_dttlqt_task (
					A_LOWER_TRIANGLE_HANDLES_ENTRY(step.col, step.pivot_row + column_offset),
					A_LOWER_TRIANGLE_HANDLES_ENTRY(step.col, step.eliminated_row + column_offset),
					TV2_HANDLES_ENTRY(step.col, step.eliminated_row),
					workspace
				);

				for (int updated_row = step.col + 1; updated_row < a_handles_height; updated_row++) {
					insert_dttmlq_task (
						'R',
						'T',
						A_LOWER_TRIANGLE_HANDLES_ENTRY(step.col, step.eliminated_row + column_offset),
						A_UPPER_TRIANGLE_HANDLES_ENTRY(updated_row, step.pivot_row + column_offset),
						A_LOWER_TRIANGLE_HANDLES_ENTRY(updated_row, step.pivot_row + column_offset),
						A_UPPER_TRIANGLE_HANDLES_ENTRY(updated_row, step.eliminated_row + column_offset),
						A_LOWER_TRIANGLE_HANDLES_ENTRY(updated_row, step.eliminated_row + column_offset),
						TV2_HANDLES_ENTRY(step.col, step.eliminated_row),
						workspace
					);
				}
			}

			(*lq_step_index)++;
		}
	}
}

int compare_elim_steps_by_column (const elim_step_t* a, const elim_step_t* b) {
	return a->col - b->col;
}

int pdge2gb (int m, int n, double* A, int lda, double* TU, int ldtu, double* TV, int ldtv, config_t* config, elim_context_t* qr_context, elim_context_t* lq_context) {
	// Get relevant config options
	int nb = get_nb_from_config (config);
	int ib = get_ib_from_config (config);
	elim_scheme_e elim_scheme = get_elim_scheme_from_config (config);
	int layout_translation = get_layout_translation_from_config (config);

	// Check parameter values
	if (m < 0) {
		return 1;
	} else if (n < 0) {
		return 2;
	} else if (lda < max (1, m)) {
		return 4;
	} else if (ldtu < max (1, ib)) {
		return 6;
	} else if (ldtv < max (1, ib)) {
		return 8;
	} else if (config && !is_config_valid (*config)) {
		return 9;
	}

	// Initialize the runtime system
	int runtime_system_already_initialized = RUNTIME_SYSTEM_IS_INITIALIZED ();
	if (!runtime_system_already_initialized)
		RUNTIME_SYSTEM_INIT ();

	// If layout translation is enabled, allocate the blockwise matrix
	double* orig_A = NULL;
	if (layout_translation) {
		orig_A = A;
		A = malloc (m * n * sizeof(double));
	}

	int no_blocks_x = (n - 1) / nb + 1;
	int no_blocks_y = (m - 1) / nb + 1;

	int a_handles_height = no_blocks_y, a_handles_width = no_blocks_x;
	int tu_handles_height, tu_handles_width;
	int tv_handles_height, tv_handles_width;

	if (m >= n) {
		tu_handles_height = no_blocks_y;
		tu_handles_width = min (no_blocks_y, no_blocks_x);
		tv_handles_height = min (no_blocks_y, no_blocks_x - 1);
		tv_handles_width = no_blocks_x - 1;
	} else {
		tu_handles_height = no_blocks_y - 1;
		tu_handles_width = min (no_blocks_y - 1, no_blocks_x);
		tv_handles_height = min (no_blocks_y, no_blocks_x);
		tv_handles_width = no_blocks_x;
	}

	// If the user passed a QR context for subsequent PDGEMQR calls, set its attributes to the used values
	if (qr_context != NULL) {
		if (config) {
			qr_context->config = *config;
		} else {
			init_config (&qr_context->config);
		}

		if (is_elim_scheme_superblock_based (qr_context->config.elim_scheme)) {
			if (m >= n) {
				qr_context->a_handles_height = a_handles_height;
				qr_context->a_handles_width = a_handles_width;
			} else {
				qr_context->a_handles_height = a_handles_height - 1;
				qr_context->a_handles_width = a_handles_width;
			}

			qr_context->num_threads = RUNTIME_SYSTEM_GET_NO_THREADS ();
		}
	}

	// If the user passed an LQ context for subsequent PDGEMLQ calls, set its attributes to the used values
	if (lq_context != NULL) {
		if (config) {
			lq_context->config = *config;
		} else {
			init_config (&lq_context->config);
		}

		if (is_elim_scheme_superblock_based (lq_context->config.elim_scheme)) {
			if (m >= n) {
				lq_context->a_handles_height = a_handles_width - 1;
				lq_context->a_handles_width = a_handles_height;
			} else {
				lq_context->a_handles_height = a_handles_width;
				lq_context->a_handles_width = a_handles_height;
			}

			lq_context->num_threads = RUNTIME_SYSTEM_GET_NO_THREADS ();
		}
	}

	// Allocate the handle matrices
	data_handle_t* A_upper_triangle_handles = malloc (a_handles_height * a_handles_width * sizeof (data_handle_t));
	data_handle_t* A_lower_triangle_handles = malloc (a_handles_height * a_handles_width * sizeof (data_handle_t));
	data_handle_t* TU_handles = malloc (get_t_size_multiplier_for_elim_scheme (elim_scheme) * tu_handles_height * tu_handles_width * sizeof (data_handle_t));
	data_handle_t* TV_handles = malloc (get_t_size_multiplier_for_elim_scheme (elim_scheme) * tv_handles_height * tv_handles_width * sizeof (data_handle_t));
	data_handle_t* orig_A_handles = NULL;

	// Register the handles
	if (layout_translation) {
		register_full_matrix_handles (m, n, nb, A, m, A_upper_triangle_handles, BlockWiseLayout);
		register_full_matrix_handles (m, n, nb, A, m, A_lower_triangle_handles, BlockWiseLayout);
		orig_A_handles = malloc (a_handles_height * a_handles_width * sizeof (data_handle_t));
		register_full_matrix_handles (m, n, nb, orig_A, lda, orig_A_handles, ColumnWiseLayout);
	} else {
		register_full_matrix_handles (m, n, nb, A, lda, A_upper_triangle_handles, ColumnWiseLayout);
		register_full_matrix_handles (m, n, nb, A, lda, A_lower_triangle_handles, ColumnWiseLayout);
	}

	if (m >= n) {
		qr_register_T_handles (m, n, nb, ib, TU, ldtu, TU_handles, elim_scheme);
		lq_register_T_handles (m, n - nb, nb, ib, TV, ldtv, TV_handles, elim_scheme);
	} else {
		qr_register_T_handles (m - nb, n, nb, ib, TU, ldtu, TU_handles, elim_scheme);
		lq_register_T_handles (m, n, nb, ib, TV, ldtv, TV_handles, elim_scheme);
	}

	// Obtain the elimination steps
	int no_qr_steps, no_lq_steps;
	elim_step_t* qr_steps, * lq_steps;

	if (m >= n) {
		no_qr_steps = get_no_steps_for_elim_scheme (a_handles_height, a_handles_width, *qr_context);
		qr_steps = malloc (no_qr_steps * sizeof (elim_step_t));
		generate_steps_for_elim_scheme (qr_steps, a_handles_height, a_handles_width, *qr_context);

		no_lq_steps = get_no_steps_for_elim_scheme (a_handles_width - 1, a_handles_height, *lq_context);
		lq_steps = malloc (no_lq_steps * sizeof (elim_step_t));
		generate_steps_for_elim_scheme (lq_steps, a_handles_width - 1, a_handles_height, *lq_context);
	} else {
		no_qr_steps = get_no_steps_for_elim_scheme (a_handles_height - 1, a_handles_width, *qr_context);
		qr_steps = malloc (no_qr_steps * sizeof (elim_step_t));
		generate_steps_for_elim_scheme (qr_steps, a_handles_height - 1, a_handles_width, *qr_context);

		no_lq_steps = get_no_steps_for_elim_scheme (a_handles_width, a_handles_height, *lq_context);
		lq_steps = malloc (no_lq_steps * sizeof (elim_step_t));
		generate_steps_for_elim_scheme (lq_steps, a_handles_width, a_handles_height, *lq_context);
	}

	// The TtGreedy and SuperblockGreedy elimination schemes are the only schemes whose steps might not be sorted by columns
	// in an ascending order. This does not present a problem in the QR/LQ routines, but the band bidiagonalization routine PDGE2GB
	// traverses the the steps by columns. As such, we need to sort the steps by columns in an ascending order.
	if (elim_scheme == TtGreedy || elim_scheme == SuperblockGreedy) {
		qsort (qr_steps, no_qr_steps, sizeof (elim_step_t), (int (*) (const void*, const void*))compare_elim_steps_by_column);
		qsort (lq_steps, no_lq_steps, sizeof (elim_step_t), (int (*) (const void*, const void*))compare_elim_steps_by_column);
	}

	int qr_step_index = 0, lq_step_index = 0;

	// Allocate the workspace
	double** workspace;
	create_workspace (&workspace, nb, ib);

	// Copy the columnwise matrix to the blockwise matrix if layout translation is enabled
	if (layout_translation) {
		copy_full_matrix_nonblocking (a_handles_height, a_handles_width, orig_A_handles, A_upper_triangle_handles);
	}

	// Go through the elimination steps, switch between QR and LQ elimination
	if (m >= n) {
		while (qr_step_index < no_qr_steps || lq_step_index < no_lq_steps) {
			// QR steps to eliminate the current column
			qr_eliminate_column_with_offset (qr_steps, no_qr_steps, &qr_step_index, A_upper_triangle_handles, A_lower_triangle_handles,
											 a_handles_height, a_handles_width, TU_handles, tu_handles_height, tu_handles_width, workspace, 0);

			// LQ steps to eliminate the current row (shifted by one)
			lq_eliminate_row_with_offset (lq_steps, no_lq_steps, &lq_step_index, A_upper_triangle_handles, A_lower_triangle_handles,
										  a_handles_height, a_handles_width, TV_handles, tv_handles_height, tv_handles_width, workspace, 1);
		}
	} else {
		while (qr_step_index < no_qr_steps || lq_step_index < no_lq_steps) {
			// LQ steps to eliminate the current row
			lq_eliminate_row_with_offset (lq_steps, no_lq_steps, &lq_step_index, A_upper_triangle_handles, A_lower_triangle_handles,
										  a_handles_height, a_handles_width, TV_handles, tv_handles_height, tv_handles_width, workspace, 0);

			// QR steps to eliminate the current column (shifted by one)
			qr_eliminate_column_with_offset (qr_steps, no_qr_steps, &qr_step_index, A_upper_triangle_handles, A_lower_triangle_handles,
											 a_handles_height, a_handles_width, TU_handles, tu_handles_height, tu_handles_width, workspace, 1);
		}
	}

	// Copy the blockwise matrix back to the columnwise matrix if layout translation is enabled
	if (layout_translation) {
		copy_full_matrix_nonblocking (a_handles_height, a_handles_width, A_upper_triangle_handles, orig_A_handles);
	}

	// Wait for all tasks to finish
	RUNTIME_SYSTEM_WAIT_FOR_ALL_TASKS ();

	// Deallocate the workspace
	destroy_workspace (workspace);

	// Deregister the handles
	deregister_full_matrix_handles (a_handles_height, a_handles_width, A_upper_triangle_handles);
	deregister_full_matrix_handles (a_handles_height, a_handles_width, A_lower_triangle_handles);
	qr_deregister_T_handles (tu_handles_height, tu_handles_width, TU_handles, elim_scheme);
	lq_deregister_T_handles (tv_handles_height, tv_handles_width, TV_handles, elim_scheme);

	if (layout_translation) {
		deregister_full_matrix_handles (a_handles_height, a_handles_width, orig_A_handles);
	}

	// Free the handle matrices
	free (A_upper_triangle_handles);
	free (A_lower_triangle_handles);
	free (TU_handles);
	free (TV_handles);

	if (layout_translation) {
		free (A);
		free (orig_A_handles);
	}

	// Free the allocated elimination steps lists
	free (qr_steps);
	free (lq_steps);

	// Shutdown the runtime system if it wasn't initialized prior to calling this function
	if (!runtime_system_already_initialized) {
		RUNTIME_SYSTEM_SHUTDOWN ();
	}

	return 0;
}

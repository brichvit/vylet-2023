#ifndef PDGE2GB_H_
#define PDGE2GB_H_

#include "../common/elimination.h"
#include "../common/runtime_system.h"

int pdge2gb (int m, int n, double* A, int lda, double* TU, int ldtu, double* TV, int ldtv, config_t* config, elim_context_t* qr_context, elim_context_t* lq_context);

#endif /* PDGE2GB_H_ */
#include "dttqrt_task.h"
#include "dttqrt.h"

#if defined (USE_STARPU)
#include "../common/starpu_matrix_utils.h"

void dttqrt_func (void* buffers[], void* args) {
	int m = STARPU_COL_MATRIX_GET_NY(buffers[1]);
	int n = STARPU_COL_MATRIX_GET_NX(buffers[0]);

	int ib = STARPU_COL_MATRIX_GET_NY(buffers[2]);

	int lda1 = STARPU_COL_MATRIX_GET_LD(buffers[0]);
	int lda2 = STARPU_COL_MATRIX_GET_LD(buffers[1]);
	int ldt = STARPU_COL_MATRIX_GET_LD(buffers[2]);

	double* A1 = (double*)STARPU_MATRIX_GET_PTR(buffers[0]);
	double* A2 = (double*)STARPU_MATRIX_GET_PTR(buffers[1]);
	double* T = (double*)STARPU_MATRIX_GET_PTR(buffers[2]);

	double** workspace;
	starpu_codelet_unpack_args (args, &workspace);

	double* work = workspace[starpu_worker_get_id ()];
	
	dttqrt (m, n, ib, A1, lda1, A2, lda2, T, ldt, work);
}

struct starpu_codelet dttqrt_codelet = {
	.cpu_funcs = { dttqrt_func },
	.cpu_funcs_name = { "dttqrt" },
	.nbuffers = 3,
	.modes = { STARPU_RW, STARPU_RW, STARPU_W },
	.name = "DTTQRT"
};

void insert_dttqrt_task (data_handle_t upper_triangle_a1_handle, data_handle_t upper_triangle_a2_handle,
						 data_handle_t t_handle, double** workspace) {
	starpu_task_insert (
		&dttqrt_codelet,
		STARPU_RW, upper_triangle_a1_handle,
		STARPU_RW, upper_triangle_a2_handle,
		STARPU_W, t_handle,
		STARPU_VALUE, &workspace, sizeof (double**),
		0
	);
}
#elif defined (USE_OPENMP)
#include <omp.h>

void insert_dttqrt_task (data_handle_t upper_triangle_a1_handle, data_handle_t upper_triangle_a2_handle,
						 data_handle_t t_handle, double** workspace) {
	#pragma omp task \
			depend(inout: *upper_triangle_a1_handle.ptr) \
			depend(inout: *upper_triangle_a2_handle.ptr) \
			depend(out: *t_handle.ptr)
	{
		double* work = workspace[omp_get_thread_num ()];

		dttqrt (
			upper_triangle_a2_handle.height,
			upper_triangle_a1_handle.width,
			t_handle.height,
			upper_triangle_a1_handle.ptr,
			upper_triangle_a1_handle.leading_dimension,
			upper_triangle_a2_handle.ptr,
			upper_triangle_a2_handle.leading_dimension,
			t_handle.ptr,
			t_handle.leading_dimension,
			work
		);
	}
}
#endif
#include "dttqrt.h"
#include "../dttmqr/dttmqr.h"
#include "../dlarfg/dlarfg.h"
#include "../common/math_utils.h"

#include <memory.h>

#define A1_ENTRY(i,j) A1[i + (j) * lda1]
#define A2_ENTRY(i,j) A2[i + (j) * lda2]
#define T_ENTRY(i,j) T[i + (j) * ldt]
#define WORK_ENTRY(i) work[i]

#if defined (USE_MKL)
#include <mkl_lapacke.h>
#elif defined (USE_ARMPL)
#include <armpl.h>
#endif


void dttqrt_inner_block (int m, int n, int additional_A2_full_rows, double* A1, int lda1, double* A2, int lda2, double* T, int ldt, double* work) {
	for (int reflector_index = 0; reflector_index < n; reflector_index++) {
		// Obtain the Householder reflector
		double* A1_diagonal_entry = &A1_ENTRY(reflector_index, reflector_index);
		dlarfg (min (reflector_index + additional_A2_full_rows + 2, m + 1), A1_diagonal_entry, &A2_ENTRY(0, reflector_index), 1, &T_ENTRY(reflector_index, reflector_index));

		// Propagate the reflector into the following columns
		double tau = T_ENTRY(reflector_index, reflector_index);
		
		// work := A1[reflector_index,reflector_index+1:n]
		//
		// This is equivalent to:
		//
		//	cblas_dcopy (n - reflector_index - 1, &A1_ENTRY(reflector_index, reflector_index + 1), lda1, work, 1);
		for (int work_index = 0; work_index < n - reflector_index - 1; work_index++) {
			WORK_ENTRY(work_index) = A1_ENTRY(reflector_index, reflector_index + 1 + work_index);
		}

		// A1[reflector_index,reflector_index+1:n] := (1 - tau) * A1[reflector_index,reflector_index+1:n] - tau * v2^T * A2[:,reflector_index+1:n]
		//
		// where v2 = A2[:,reflector_index]
		//
		// This is equivalent to:
		//
		//	cblas_dscal (n - reflector_index - 1, 1.0 - tau, &A1_ENTRY(reflector_index, reflector_index + 1), lda1);
		//
		//	cblas_dgemv (CblasColMajor, CblasTrans, min (m, reflector_index + additional_A2_full_rows + 1), n - reflector_index - 1, -tau, &A2_ENTRY(0, reflector_index + 1), lda2, &A2_ENTRY(0, reflector_index), 1, 1.0, &A1_ENTRY(reflector_index, reflector_index + 1), lda1);
		for (int updated_col = reflector_index + 1; updated_col < n; updated_col++) {
			A1_ENTRY(reflector_index, updated_col) *= 1 - tau;
			for (int k = 0; k < min (m, reflector_index + additional_A2_full_rows + 1); k++) {
				A1_ENTRY(reflector_index, updated_col) -= tau * A2_ENTRY(k, reflector_index) * A2_ENTRY(k, updated_col);
			}
		}

		// work := work + v2^T * A2[:,reflector_index+1:n]
		//
		// where v2 = A2[:,reflector_index]
		//
		// This is equivalent to:
		//
		//	cblas_dgemv (CblasColMajor, CblasTrans, min (m, reflector_index + additional_A2_full_rows + 1), n - reflector_index - 1, 1.0, &A2_ENTRY(0, reflector_index + 1), lda2, &A2_ENTRY(0, reflector_index), 1, 1.0, work, 1);
		for (int work_index = 0; work_index < n - reflector_index - 1; work_index++) {
			for (int k = 0; k < min (m, reflector_index + additional_A2_full_rows + 1); k++) {
				WORK_ENTRY(work_index) += A2_ENTRY(k, reflector_index) * A2_ENTRY(k, work_index + reflector_index + 1);
			}
		}

		// A2[:,reflector_index+1:n] := A2[:,reflector_index+1:n] - tau * v2 * work^T
		//
		// where v2 = A2[:,reflector_index]
		//
		// This is equivalent to:
		//
		//	cblas_dger (CblasColMajor, min (m, reflector_index + additional_A2_full_rows + 1), n - reflector_index - 1, -tau, &A2_ENTRY(0, reflector_index), 1, work, 1, &A2_ENTRY(0, reflector_index + 1), lda2);
		for (int updated_col = reflector_index + 1; updated_col < n; updated_col++) {
			for (int updated_row = 0; updated_row < min (m, reflector_index + additional_A2_full_rows + 1); updated_row++) {
				A2_ENTRY(updated_row, updated_col) -= tau * A2_ENTRY(updated_row, reflector_index) * WORK_ENTRY(updated_col - reflector_index - 1);
			}	
		}
	}

	// Calculate the non-diagonal reflector factors in T
	for (int col = 1; col < n; col++) {
		double tau = T[col * ldt + col];

		// T[0:col,col] := -tau * A2[:,0:col]^T * A2[:,col]
		//
		// This is equivalent to:
		//
		//	// If additional_A2_full_rows > 0, then T[0:col,col] is initalized to contain zeros in cblas_dgemv
		//	// Otherwise, we need to zero it out manually
		//	if (additional_A2_full_rows > 0) {
		//		cblas_dgemv (CblasColMajor, CblasTrans, min (additional_A2_full_rows, m), col, -tau, A2, lda2, &A2_ENTRY(0, col), 1, 0.0, &T_ENTRY(0, col), 1);
		//	} else {
		//		cblas_dscal (col, 0, &T_ENTRY(0, col), 1);
		//	}
		//
		//	cblas_dgemv (CblasColMajor, CblasTrans, max (m - additional_A2_full_rows, 0), max (col - m + additional_A2_full_rows, 0), -tau, &A2_ENTRY(additional_A2_full_rows, m - additional_A2_full_rows), lda2, &A2_ENTRY(additional_A2_full_rows, col), 1, 1.0, &T_ENTRY(m - additional_A2_full_rows, col), 1);
		//
		//	// Unfortunately, dtrmv overwrites the multiplied vector, so using it directly on A2[:,col] is not feasible
		//	// To solve this, we copy the multiplied vector to T[col,0:col] (which is in the lower triangular part of T, and is thus unused)
		//	// and we perform the in-place computation there
		//	cblas_dcopy (clamp (m - additional_A2_full_rows, 0, col), &A2_ENTRY(additional_A2_full_rows, col), 1, &T_ENTRY(col, 0), ldt);
		//	cblas_dtrmv (CblasColMajor, CblasUpper, CblasTrans, CblasNonUnit, clamp (m - additional_A2_full_rows, 0, col), &A2_ENTRY(additional_A2_full_rows, 0), lda2, &T_ENTRY(col, 0), ldt);
		//	cblas_daxpy (clamp (m - additional_A2_full_rows, 0, col), -tau, &T_ENTRY(col, 0), ldt, &T_ENTRY(0, col), 1);
		memset (&T_ENTRY(0,col), 0, col * sizeof (double));
		for (int row = 0; row < col; row++) {
			for (int k = 0; k < min (m, row + additional_A2_full_rows + 1); k++) {
				T_ENTRY(row, col) -= tau * A2_ENTRY(k, row) * A2_ENTRY(k, col);
			}
		}

		// T[0:col,col] := T[0:col,0:col] * T[0:col,col]
		//
		// This is equivalent to:
		//
		//	cblas_dtrmv (CblasColMajor, CblasUpper, CblasNoTrans, CblasNonUnit, col, T, ldt, &T_ENTRY(0, col), 1);
		for (int row = 0; row < col; row++) {
			T_ENTRY(row, col) *= T_ENTRY(row, row);

			for (int k = row + 1; k < col; k++) {
				T_ENTRY(row, col) += T_ENTRY(k, col) * T_ENTRY(row, k);
			}
		}
	}
}

int dttqrt (int m, int n, int nb, double* A1, int lda1, double* A2, int lda2, double* T, int ldt, double* work) {
	// Check parameter values
	if (m < 0) {
		return 1;
	} else if (n < 0) {
		return 2;
	} else if (nb <= 0) {
		return 3;
	} else if (lda1 < max (1, n)) {
		return 5;
	} else if (lda2 < max (1, m)) {
		return 7;
	} else if (ldt < max (1, nb)) {
		return 9;
	}

	#if defined (USE_MKL) || defined (USE_ARMPL)
	if (nb > n) {
		nb = n;
	}

	LAPACKE_dtpqrt_work (LAPACK_COL_MAJOR, min (m, n), n, min (m, n), nb, A1, lda1, A2, lda2, T, ldt, work);
	#else
	for (int block_start = 0; block_start < n; block_start += nb) {
		int block_size = min (nb, n - block_start);

		dttqrt_inner_block (m, block_size, block_start, &A1_ENTRY(block_start, block_start), lda1, &A2_ENTRY(0, block_start), lda2, &T_ENTRY(0, block_start), ldt, work);

		if (block_start + block_size < n) {
			// Apply the calculated reflectors to all following blocks
			int A2_block_height = min (nb, m - block_start);

			for (int updated_block_start = block_start + nb; updated_block_start < n; updated_block_start += nb) {
				int updated_block_size = min (nb, n - updated_block_start);

				dttmqr_inner_block ('L', 'T', block_size, updated_block_size, block_start + A2_block_height, updated_block_size, block_size, block_start, &A1_ENTRY(block_start, updated_block_start), lda1, &A2_ENTRY(0, updated_block_start), lda2, &A2_ENTRY(0, block_start), lda2, &T_ENTRY(0, block_start), ldt, work);
			}
		}
	}
	#endif

	return 0;
}
#ifndef DTTQRT_TASK_H_
#define DTTQRT_TASK_H_

#include "../common/runtime_system.h"

void insert_dttqrt_task (data_handle_t upper_triangle_a1_handle, data_handle_t upper_triangle_a2_handle,
						 data_handle_t t_handle, double** workspace);

#endif /* DTTQRT_TASK_H_ */
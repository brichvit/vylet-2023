#include "dtslqt.h"
#include "../dtsmlq/dtsmlq.h"
#include "../dlarfg/dlarfg.h"
#include "../common/math_utils.h"

#include <memory.h>

#define A1_ENTRY(i,j) A1[i + (j) * lda1]
#define A2_ENTRY(i,j) A2[i + (j) * lda2]
#define T_ENTRY(i,j) T[i + (j) * ldt]
#define WORK_ENTRY(i) work[i]

#if defined (USE_MKL)
#include <mkl_cblas.h>
#define USE_EXTERNAL_BLAS
#elif defined (USE_ARMPL)
#include <armpl.h>
#define USE_EXTERNAL_BLAS
#endif

void dtslqt_inner_block (int m, int n, double* A1, int lda1, double* A2, int lda2, double* T, int ldt, double* work) {
	for (int reflector_index = 0; reflector_index < m; reflector_index++) {
		// Obtain the Householder reflector
		double* A1_diagonal_entry = &A1_ENTRY(reflector_index, reflector_index);
		dlarfg (n + 1, A1_diagonal_entry, &A2_ENTRY(reflector_index, 0), lda2, &T_ENTRY(reflector_index, reflector_index));

		// Propagate the reflector into the following columns
		double tau = T_ENTRY(reflector_index, reflector_index);
		
		// work := A1[(reflector_index+1):m,reflector_index]
		#ifdef USE_EXTERNAL_BLAS
			cblas_dcopy (m - reflector_index - 1, &A1_ENTRY(reflector_index + 1, reflector_index), 1, work, 1);
		#else
			for (int work_index = 0; work_index < m - reflector_index - 1; work_index++) {
				WORK_ENTRY(work_index) = A1_ENTRY(reflector_index + 1 + work_index, reflector_index);
			}
		#endif

		// A1[(reflector_index+1):m,reflector_index] := (1 - tau) * A1[(reflector_index+1):m,reflector_index] - tau * A2[(reflector_index+1):m,:] * v2
		//
		// where v2^T = A2[reflector_index,:]
		#ifdef USE_EXTERNAL_BLAS
			cblas_dscal (m - reflector_index - 1, 1.0 - tau, &A1_ENTRY(reflector_index + 1, reflector_index), 1);

			cblas_dgemv (CblasColMajor, CblasNoTrans, m - reflector_index - 1, n, -tau, &A2_ENTRY(reflector_index + 1, 0), lda2, &A2_ENTRY(reflector_index, 0), lda2, 1.0, &A1_ENTRY(reflector_index + 1, reflector_index), 1);
		#else
			for (int updated_row = reflector_index + 1; updated_row < m; updated_row++) {
				A1_ENTRY(updated_row, reflector_index) *= 1 - tau;
				for (int k = 0; k < n; k++) {
					A1_ENTRY(updated_row, reflector_index) -= tau * A2_ENTRY(updated_row, k) * A2_ENTRY(reflector_index, k);
				}
			}
		#endif

		// work := work + A2[(reflector_index+1:m),:] * v2
		//
		// where v2^T = A2[reflector_index,:]
		#ifdef USE_EXTERNAL_BLAS
			cblas_dgemv (CblasColMajor, CblasNoTrans, m - reflector_index - 1, n, 1.0, &A2_ENTRY(reflector_index + 1, 0), lda2, &A2_ENTRY(reflector_index, 0), lda2, 1.0, work, 1);
		#else
		for (int work_index = 0; work_index < m - reflector_index - 1; work_index++) {
			for (int k = 0; k < n; k++) {
				WORK_ENTRY(work_index) += A2_ENTRY(work_index + reflector_index + 1, k) * A2_ENTRY(reflector_index, k);
			}
		}
		#endif

		// A2[(reflector_index+1:m),:] := A2[(reflector_index+1:m),:] - tau * work * v2^T
		//
		// where v2^T = A2[reflector_index,:]
		#ifdef USE_EXTERNAL_BLAS
			cblas_dger (CblasColMajor, m - reflector_index - 1, n, -tau, work, 1, &A2_ENTRY(reflector_index, 0), lda2, &A2_ENTRY(reflector_index + 1, 0), lda2);
		#else
			for (int updated_col = 0; updated_col < n; updated_col++) {
				for (int updated_row = reflector_index + 1; updated_row < m; updated_row++) {
					A2_ENTRY(updated_row, updated_col) -= tau * WORK_ENTRY(updated_row - reflector_index - 1) * A2_ENTRY(reflector_index, updated_col);
				}	
			}
		#endif
	}

	// Calculate the non-diagonal reflector factors in T
	for (int col = 1; col < m; col++) {
		double tau = T[col * ldt + col];

		// T[0:col,col] := -tau * A2[0:col,:] * A2[col,:]^T
		#ifdef USE_EXTERNAL_BLAS
			cblas_dgemv (CblasColMajor, CblasNoTrans, col, n, -tau, A2, lda2, &A2_ENTRY(col, 0), lda2, 0.0, &T_ENTRY(0, col), 1);
		#else
			memset (&T_ENTRY(0,col), 0, col * sizeof (double));
			for (int row = 0; row < col; row++) {
				for (int k = 0; k < n; k++) {
					T_ENTRY(row, col) -= tau * A2_ENTRY(row, k) * A2_ENTRY(col, k);
				}
			}
		#endif

		// T[0:col,col] := T[0:col,0:col] * T[0:col,col]
		#ifdef USE_EXTERNAL_BLAS
			cblas_dtrmv (CblasColMajor, CblasUpper, CblasNoTrans, CblasNonUnit, col, T, ldt, &T_ENTRY(0, col), 1);
		#else
			for (int row = 0; row < col; row++) {
				T_ENTRY(row, col) *= T_ENTRY(row, row);

				for (int k = row + 1; k < col; k++) {
					T_ENTRY(row, col) += T_ENTRY(k, col) * T_ENTRY(row, k);
				}
			}
		#endif
	}
}

int dtslqt (int m, int n, int mb, double* A1, int lda1, double* A2, int lda2, double* T, int ldt, double* work) {
	// Check parameter values
	if (m < 0) {
		return 1;
	} else if (n < 0) {
		return 2;
	} else if (mb <= 0) {
		return 3;
	} else if (lda1 < max (1, m)) {
		return 5;
	} else if (lda2 < max (1, m)) {
		return 7;
	} else if (ldt < max (1, mb)) {
		return 9;
	}

	for (int block_start = 0; block_start < m; block_start += mb) {
		int block_size = min (mb, m - block_start);

		dtslqt_inner_block (block_size, n, &A1_ENTRY(block_start, block_start), lda1, &A2_ENTRY(block_start, 0), lda2, &T_ENTRY(0, block_start), ldt, work);

		if (block_start + block_size < m) {
			dtsmlq ('R', 'T', m - block_start - block_size, block_size, m - block_start - block_size, n, block_size, mb, &A1_ENTRY(block_start + block_size, block_start), lda1, &A2_ENTRY(block_start + block_size, 0), lda2, &A2_ENTRY(block_start, 0), lda2, &T_ENTRY(0, block_start), ldt, work);
		}
	}

	return 0;
}

#ifndef DTSLQT_TASK_H_
#define DTSLQT_TASK_H_

#include "../common/runtime_system.h"

void insert_dtslqt_task (data_handle_t lower_triangle_a1_handle,
						 data_handle_t upper_triangle_a2_handle, data_handle_t lower_triangle_a2_handle,
						 data_handle_t t_handle, double** workspace);

#endif /* DTSLQT_TASK_H_ */
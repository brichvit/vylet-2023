#ifndef QR_H_
#define QR_H_

#include "../common/elimination.h"
#include "../common/runtime_system.h"

int pdgeqrf (int m, int n, double* A, int lda, double* T, int ldt, config_t* config, elim_context_t* context);

int pdgemqr (char side, char trans, int m, int n, int k, double* A, int lda, double* T, int ldt, double* C, int ldc, elim_context_t context);

void qr_register_T_handles (int m, int n, int nb, int ib, double* T, int ldt, data_handle_t* T_handles, elim_scheme_e elim_scheme);

void qr_deregister_T_handles (int t_handles_height, int t_handles_width, data_handle_t* T_handles, elim_scheme_e elim_scheme);

#endif /* QR_H_ */

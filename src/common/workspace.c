#include "workspace.h"
#include "runtime_system.h"

#include <stdlib.h>

void create_workspace (double*** workspace, int nb, int ib) {
	int no_threads = RUNTIME_SYSTEM_GET_NO_THREADS();

	*workspace = malloc (no_threads * sizeof (double*));

	for (int i = 0; i < no_threads; i++) {
		(*workspace)[i] = malloc (nb * ib * sizeof (double));
	}
}

void destroy_workspace (double** workspace) {
	int no_threads = RUNTIME_SYSTEM_GET_NO_THREADS();

	for (int i = 0; i < no_threads; i++) {
		free (workspace[i]);
	}

	free (workspace);
}

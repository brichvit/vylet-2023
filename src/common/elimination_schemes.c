#include "elimination_schemes.h"

#include <stddef.h>

char* get_elim_scheme_name (elim_scheme_e elim_scheme) {
	switch (elim_scheme) {
		case TsFlatTree:
			return "TsFlatTree";
		case TtFlatTree:
			return "TtFlatTree";
		case TtGreedy:
			return "TtGreedy";
		case TtBinaryTree:
			return "TtBinaryTree";
		case TtFibonacci:
			return "TtFibonacci";
		case SuperblockGreedy:
			return "SuperblockGreedy";
		case SuperblockBinaryTree:
			return "SuperblockBinaryTree";
		case SuperblockFibonacci:
			return "SuperblockFibonacci";
		case AutoTree:
			return "AutoTree";
		default:
			return NULL;
	}
}

int is_elim_scheme_superblock_based (elim_scheme_e elim_scheme) {
	return elim_scheme == SuperblockGreedy || elim_scheme == SuperblockBinaryTree || elim_scheme == SuperblockFibonacci || elim_scheme == AutoTree;
}

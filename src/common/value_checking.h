#ifndef VALUE_CHECKING_H_
#define VALUE_CHECKING_H_

#include <ctype.h>
#define IS_TRANS(val) (toupper (val) == 'T')
#define IS_NOT_TRANS(val) (toupper (val) == 'N')
#define VALID_TRANS_VALUE(val) (IS_TRANS(val) || IS_NOT_TRANS(val))
#define IS_LEFT(val) (toupper (val) == 'L')
#define IS_RIGHT(val) (toupper (val) == 'R')
#define VALID_SIDE_VALUE(val) (IS_LEFT(val) || IS_RIGHT(val))

#endif /* VALUE_CHECKING_H_ */
#ifndef ELIMINATION_SCHEMES_H_
#define ELIMINATION_SCHEMES_H_

#define NO_ELIM_SCHEMES 9

typedef enum {
	TsFlatTree,
	TtFlatTree,
	TtGreedy,
	TtBinaryTree,
	TtFibonacci,
	SuperblockGreedy,
	SuperblockBinaryTree,
	SuperblockFibonacci,
	AutoTree
} elim_scheme_e;

char* get_elim_scheme_name (elim_scheme_e elim_scheme);

int is_elim_scheme_superblock_based (elim_scheme_e elim_scheme);

#endif /* ELIMINATION_SCHEMES_H_ */
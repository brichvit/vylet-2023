#include "math_utils.h"

inline int max (int a, int b);
int max (int a, int b) {
	return (a >= b) ? a : b;
}

inline int min (int a, int b);
int min (int a, int b) {
	return (a <= b) ? a : b;
}

inline int clamp (int val, int min_val, int max_val);
int clamp (int val, int min_val, int max_val) {
	return min (max (val, min_val), max_val);
}

inline double sgn (double x);
double sgn (double x) {
	if (x > 0) {
		return 1;
	} else if (x < 0) {
		return -1;
	} else {
		return 0;
	}
}

int floor_sqrt (int n) {
	if (n < 0) {
		return -1;
	} else if (n == 0) {
		return 0;
	}

	int lower = 1;
	int upper = n;

	while (upper - lower > 1) {
		int middle = (lower + upper) / 2;

		if ((long)middle * (long)middle <= (long)n) {
			lower = middle;
		} else {
			upper = middle;
		}
	}

	return lower;
}

inline int get_lower_k_for_sum_of_positive_integers (int n);
int get_lower_k_for_sum_of_positive_integers (int n) {
	return (-1 + floor_sqrt (1 + 8 * n)) / 2;
}

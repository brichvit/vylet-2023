#ifndef CONFIG_H_
#define CONFIG_H_

#include "elimination_schemes.h"

#define DEFAULT_NB_VALUE 256
#define DEFAULT_IB_VALUE 64
#define DEFAULT_SUPERBLOCK_SIZE_FACTOR_VALUE 4
#define DEFAULT_ELIM_SCHEME_VALUE SuperblockGreedy
#define DEFAULT_LAYOUT_TRANSLATION_VALUE 0

typedef struct {
	int nb, ib, superblock_size_factor;
	elim_scheme_e elim_scheme;
	int layout_translation;
} config_t;

void init_config (config_t* config);
int is_config_valid (config_t config);

int get_nb_from_config (config_t* config);
int get_ib_from_config (config_t* config);
int get_superblock_size_factor_from_config (config_t* config);
elim_scheme_e get_elim_scheme_from_config (config_t* config);
int get_layout_translation_from_config (config_t* config);

#endif /* CONFIG_H_ */
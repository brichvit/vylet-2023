#ifndef MATH_UTILS_H_
#define MATH_UTILS_H_

/**
 * @brief Returns the maximum of two numbers @p a and @p b .
 * 
 * @return int The maximum of the two arguments.
 */
int min (int a, int b);

/**
 * @brief Returns the minimum of two numbers @p a and @p b .
 * 
 * @return int The minimum of the two arguments.
 */
int max (int a, int b);

/**
 * @brief Returns:
 * - @p val if @p min_val <= @p val <= @p max_val
 * - @p min_val if @p val < @p min_val
 * - @p max_val if @p max_val < @p max_val
 * 
 * @return int The returned value as described above
 */
int clamp (int val, int min_val, int max_val);

/**
 * @brief Returns the value of the sign function for @p x, that is:
 * - 1 if @p x > 0
 * - 0 if @p x = 0
 * - -1 if @p x < 0
 * 
 * @return int The value of the sign function for the argument.
 */
double sgn (double x);

//TODO: documentation
int floor_sqrt (int n);

//TODO: documentation
int get_lower_k_for_sum_of_positive_integers (int n);

#endif /* MATH_UTILS_H_ */
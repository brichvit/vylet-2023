#ifndef STARPU_MATRIX_UTILS_H_
#define STARPU_MATRIX_UTILS_H_

#define STARPU_COL_MATRIX_DATA_REGISTER(handle,home_node,ptr,ld,nx,ny,elemsize) starpu_matrix_data_register(handle,home_node,ptr,ld,ny,nx,elemsize)
#define STARPU_COL_MATRIX_GET_NX(handle) STARPU_MATRIX_GET_NY(handle)
#define STARPU_COL_MATRIX_GET_NY(handle) STARPU_MATRIX_GET_NX(handle)
#define STARPU_COL_MATRIX_GET_LD(handle) STARPU_MATRIX_GET_LD(handle)

#endif /* STARPU_MATRIX_UTILS_H_ */
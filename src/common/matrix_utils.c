#include "matrix_utils.h"
#include "math_utils.h"
#include "matrix_access.h"

#include "../dlacpy/dlacpy_task.h"

void register_full_matrix_handles (int m, int n, int nb, double* A, int lda, data_handle_t* A_handles, matrix_layout_e layout) {
	int a_handles_height = (m - 1) / nb + 1;

	for (int col_start = 0; col_start < n; col_start += nb) {
		int block_width = min (n - col_start, nb);
		int block_col = col_start / nb;

		for (int row_start = 0; row_start < m; row_start += nb) {
			int block_height = min (m - row_start, nb);
			int block_row = row_start / nb;

			if (layout == BlockWiseLayout) {
				RUNTIME_SYSTEM_REGISTER_HANDLE (
					&A_HANDLES_ENTRY(block_row, block_col),
					&A_ENTRY(row_start * block_width, col_start),
					block_height,
					block_width,
					block_height
				);
			} else if (layout == ColumnWiseLayout) {
				RUNTIME_SYSTEM_REGISTER_HANDLE (
					&A_HANDLES_ENTRY(block_row, block_col),
					&A_ENTRY(row_start, col_start),
					block_height,
					block_width,
					lda
				);
			}
		}
	}
}

void deregister_full_matrix_handles (int a_handles_height, int a_handles_width, data_handle_t* A_handles) {
	for (int block_col = 0; block_col < a_handles_width; block_col++) {
		for (int block_row = 0; block_row < a_handles_height; block_row++) {
			RUNTIME_SYSTEM_UNREGISTER_HANDLE (A_handles[block_col * a_handles_height + block_row]);
		}
	}
}

void copy_full_matrix_nonblocking (int handles_height, int handles_width, data_handle_t* A_handles, data_handle_t* B_handles) {
	for (int block_col = 0; block_col < handles_width; block_col++) {
		for (int block_row = 0; block_row < handles_height; block_row++) {
			insert_dlacpy_task ('F', A_handles[block_col * handles_height + block_row], B_handles[block_col * handles_height + block_row]);
		}
	}
}
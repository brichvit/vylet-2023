#ifndef RUNTIME_SYSTEM_H_
#define RUNTIME_SYSTEM_H_

#if defined (USE_STARPU)
#include "starpu_matrix_utils.h"

#include <starpu.h>

typedef starpu_data_handle_t data_handle_t;

#define RUNTIME_SYSTEM_INIT() if (starpu_init (NULL) != 0) { \
	return -1; \
	}

#define RUNTIME_SYSTEM_WAIT_FOR_ALL_TASKS() starpu_task_wait_for_all ()

#define RUNTIME_SYSTEM_SHUTDOWN() starpu_shutdown ()

#define RUNTIME_SYSTEM_IS_INITIALIZED() starpu_is_initialized ()

#define RUNTIME_SYSTEM_GET_NO_THREADS() starpu_cpu_worker_get_count ()

#define RUNTIME_SYSTEM_REGISTER_HANDLE(handle_ptr, data_ptr, height, width, leading_dimension) \
	STARPU_COL_MATRIX_DATA_REGISTER( \
		handle_ptr, \
		STARPU_MAIN_RAM, \
		(uintptr_t)data_ptr, \
		leading_dimension, \
		width, \
		height, \
		sizeof (double) \
	);

#define RUNTIME_SYSTEM_UNREGISTER_HANDLE(handle) { \
	starpu_data_unregister (handle); \
}
#elif defined (USE_OPENMP)
#include <omp.h>

/**
 * @brief Structure representing data about a single matrix block in a column major full storage format.
 * 
 */
typedef struct {
	int width, height, leading_dimension;
	double* ptr;
} data_handle_t;

#define RUNTIME_SYSTEM_INIT() _Pragma ("omp parallel") \
_Pragma ("omp master") \
{

#define RUNTIME_SYSTEM_WAIT_FOR_ALL_TASKS() _Pragma ("omp taskwait")

#define RUNTIME_SYSTEM_SHUTDOWN() }

//TODO
#define RUNTIME_SYSTEM_IS_INITIALIZED() 0

#define RUNTIME_SYSTEM_GET_NO_THREADS() omp_get_num_threads ()

#define RUNTIME_SYSTEM_REGISTER_HANDLE(handle_ptr, data_ptr_value, height_value, width_value, leading_dimension_value) \
	(handle_ptr)->height = height_value; \
	(handle_ptr)->width = width_value; \
	(handle_ptr)->leading_dimension = leading_dimension_value; \
	(handle_ptr)->ptr = data_ptr_value;

#define RUNTIME_SYSTEM_UNREGISTER_HANDLE(handle)  \
	(void)handle;
#endif

#endif /* RUNTIME_SYSTEM_H_ */
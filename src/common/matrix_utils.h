#ifndef MATRIX_UTILS_H_
#define MATRIX_UTILS_H_

#include "runtime_system.h"

typedef enum {
	ColumnWiseLayout,
	BlockWiseLayout,
	TLayout
} matrix_layout_e;

void register_full_matrix_handles (int m, int n, int nb, double* A, int lda, data_handle_t* A_handles, matrix_layout_e layout);

void deregister_full_matrix_handles (int a_handles_height, int a_handles_width, data_handle_t* A_handles);

void copy_full_matrix_nonblocking (int handles_height, int handles_width, data_handle_t* A_handles, data_handle_t* B_handles);

#endif /* MATRIX_UTILS_H_ */
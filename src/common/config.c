#include "config.h"

void init_config (config_t* config) {
	*config = (config_t) {
		.nb = DEFAULT_NB_VALUE,
		.ib = DEFAULT_IB_VALUE,
		.superblock_size_factor = DEFAULT_SUPERBLOCK_SIZE_FACTOR_VALUE,
		.elim_scheme = DEFAULT_ELIM_SCHEME_VALUE,
		.layout_translation = DEFAULT_LAYOUT_TRANSLATION_VALUE
	};
}

int is_config_valid (config_t config) {
	if (config.nb <= 0) {
		return 0;
	} else if (config.ib <= 0 || config.ib > config.nb) {
		return 0;
	} else if (config.superblock_size_factor <= 0) {
		return 0;
	} else if (config.elim_scheme < 0 || config.elim_scheme >= NO_ELIM_SCHEMES) {
		return 0;
	} else if (config.layout_translation < 0 || config.layout_translation > 1) {
		return 0;
	}

	return 1;
}

int get_nb_from_config (config_t* config) {
	return config ? config->nb : DEFAULT_NB_VALUE;
}

int get_ib_from_config (config_t* config) {
	return config ? config->ib : DEFAULT_IB_VALUE;
}

int get_superblock_size_factor_from_config (config_t* config) {
	return config ? config->superblock_size_factor : DEFAULT_SUPERBLOCK_SIZE_FACTOR_VALUE;
}

elim_scheme_e get_elim_scheme_from_config (config_t* config) {
	return config ? config->elim_scheme : DEFAULT_ELIM_SCHEME_VALUE;
}

int get_layout_translation_from_config (config_t* config) {
	return config ? config->layout_translation : DEFAULT_LAYOUT_TRANSLATION_VALUE;
}
#ifndef ELIMINATION_H_
#define ELIMINATION_H_

#include "config.h"

typedef enum {
	GeneralKernel,
	TsKernel,
	TtKernel
} elim_kernel_type_e;

typedef struct {
	elim_kernel_type_e kernel_type;
	int pivot_row, eliminated_row;
	int col;
} elim_step_t;

typedef struct {
	config_t config;
	int a_handles_height, a_handles_width;
	int num_threads;
} elim_context_t;

int get_superblock_size (elim_context_t context);

int get_t_size_multiplier_for_elim_scheme (elim_scheme_e elim_scheme);

int get_no_steps_for_elim_scheme (int a_handles_height, int a_handles_width, elim_context_t context);

void generate_steps_for_elim_scheme (elim_step_t* steps, int a_handles_height, int a_handles_width, elim_context_t context);

#endif /* ELIMINATION_H_ */
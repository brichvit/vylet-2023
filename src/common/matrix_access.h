#ifndef MATRIX_ACCESS_H_
#define MATRIX_ACCESS_H_

#define A_ENTRY(i,j) A[i + (j) * lda]
#define T_ENTRY(i,j) T[i + (j) * ldt]
#define C_ENTRY(i,j) C[i + (j) * ldc]

#define A_HANDLES_ENTRY(i,j) A_handles[i + (j) * a_handles_height]
#define A_UPPER_TRIANGLE_HANDLES_ENTRY(i,j) A_upper_triangle_handles[i + (j) * a_handles_height]
#define A_LOWER_TRIANGLE_HANDLES_ENTRY(i,j) A_lower_triangle_handles[i + (j) * a_handles_height]
#define T_HANDLES_ENTRY(i,j) T_handles[i + (j) * t_handles_height]
#define T2_HANDLES_ENTRY(i,j) T_handles[i + (j) * t_handles_height + t_handles_width * t_handles_height]
#define C_HANDLES_ENTRY(i,j) C_handles[i + (j) * c_handles_height]

#define TU_HANDLES_ENTRY(i,j) TU_handles[i + (j) * tu_handles_height]
#define TU2_HANDLES_ENTRY(i,j) TU_handles[i + (j) * tu_handles_height + tu_handles_width * tu_handles_height]
#define TV_HANDLES_ENTRY(i,j) TV_handles[i + (j) * tv_handles_height]
#define TV2_HANDLES_ENTRY(i,j) TV_handles[i + (j) * tv_handles_height + tv_handles_width * tv_handles_height]

#endif /* MATRIX_ACCESS_H_ */
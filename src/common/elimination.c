#include "elimination.h"
#include "math_utils.h"

#include <stdlib.h>

int get_t_size_multiplier_for_elim_scheme (elim_scheme_e elim_scheme) {
	if (elim_scheme == TsFlatTree) {
		return 1;
	} else {
		return 2;
	}
}

int get_superblock_size (elim_context_t context) {
	long superblock_size = (long)context.a_handles_width * (long)context.a_handles_width / 2 + (long)context.a_handles_width / 2;
	superblock_size *= context.a_handles_height;
	superblock_size /= (long)context.config.superblock_size_factor * (long)context.num_threads;
	superblock_size = max (superblock_size, 1);

	return superblock_size;
}

int get_auto_tree_superblock_size (elim_context_t context, int eliminated_col) {
	long superblock_size = (long)context.a_handles_height - (long)eliminated_col - 1;
	superblock_size *= (long)min (context.a_handles_height, context.a_handles_width) - (long)eliminated_col - 1;
	superblock_size /= (long)context.config.superblock_size_factor * (long)context.num_threads;
	superblock_size = max (superblock_size, 1);

	return superblock_size;
}

int get_superblock_index (int col_below_diagonal, int superblock_size) {
	return (col_below_diagonal + (superblock_size - 1)) / superblock_size;
}

int get_no_steps_for_elim_scheme (int a_handles_height, int a_handles_width, elim_context_t context) {
	int a_handles_min_dim = min (a_handles_width, a_handles_height);

	switch (context.config.elim_scheme) {
		case TsFlatTree:
			return a_handles_min_dim * a_handles_height - a_handles_min_dim * (a_handles_min_dim - 1) / 2;
		case TtFlatTree:
		case TtGreedy:
		case TtBinaryTree:
		case TtFibonacci:
			return a_handles_min_dim * (2 * a_handles_height - a_handles_min_dim);
		case SuperblockGreedy: 
		case SuperblockBinaryTree: 
		case SuperblockFibonacci: {
			int superblock_size = get_superblock_size (context);
			int no_steps = a_handles_min_dim * a_handles_height - a_handles_min_dim * (a_handles_min_dim - 1) / 2;

			for (int eliminated_col = 0; eliminated_col < a_handles_min_dim; eliminated_col++) {
				int superblocks_to_eliminate = (a_handles_height - eliminated_col - 1) / superblock_size + 1;

				while (superblocks_to_eliminate > 1) {
					no_steps += superblocks_to_eliminate / 2;

					superblocks_to_eliminate = (superblocks_to_eliminate + 1) / 2;
				}
			}

			return no_steps;
		}
		case AutoTree: {
			int no_steps = a_handles_min_dim * a_handles_height - a_handles_min_dim * (a_handles_min_dim - 1) / 2;

			for (int eliminated_col = 0; eliminated_col < a_handles_min_dim; eliminated_col++) {
				int superblock_size = get_auto_tree_superblock_size (context, eliminated_col);

				int superblocks_to_eliminate = (a_handles_height - eliminated_col - 1) / superblock_size + 1;

				while (superblocks_to_eliminate > 1) {
					no_steps += superblocks_to_eliminate / 2;

					superblocks_to_eliminate = (superblocks_to_eliminate + 1) / 2;
				}
			}

			return no_steps;
		}
	}

	return -1;
}
	
void generate_ts_flat_tree_steps (elim_step_t* steps, int a_handles_height, int a_handles_width) {
	int a_handles_min_dim = min (a_handles_width, a_handles_height);
	int current_step_index = 0;

	for (int eliminated_col = 0; eliminated_col < a_handles_min_dim; eliminated_col++) {
		elim_step_t general_step = {
			.kernel_type = GeneralKernel,
			.col = eliminated_col,
			.eliminated_row = eliminated_col
		};

		steps[current_step_index++] = general_step;

		for (int eliminated_row = eliminated_col + 1; eliminated_row < a_handles_height; eliminated_row++) {
			elim_step_t ts_step = {
				.kernel_type = TsKernel,
				.col = eliminated_col,
				.pivot_row = eliminated_col,
				.eliminated_row = eliminated_row
			};

			steps[current_step_index++] = ts_step;
		}
	}
}

void generate_tt_flat_tree_steps (elim_step_t* steps, int a_handles_height, int a_handles_width) {
	int a_handles_min_dim = min (a_handles_width, a_handles_height);
	int current_step_index = 0;

	for (int eliminated_col = 0; eliminated_col < a_handles_min_dim; eliminated_col++) {
		elim_step_t upper_general_step = {
			.kernel_type = GeneralKernel,
			.col = eliminated_col,
			.eliminated_row = eliminated_col
		};

		steps[current_step_index++] = upper_general_step;

		for (int eliminated_row = eliminated_col + 1; eliminated_row < a_handles_height; eliminated_row++) {
			elim_step_t lower_general_step = {
				.kernel_type = GeneralKernel,
				.col = eliminated_col,
				.eliminated_row = eliminated_row
			};

			steps[current_step_index++] = lower_general_step;

			elim_step_t tt_step = {
				.kernel_type = TtKernel,
				.col = eliminated_col,
				.pivot_row = eliminated_col,
				.eliminated_row = eliminated_row
			};

			steps[current_step_index++] = tt_step;
		}
	}
}

void generate_tt_greedy_steps (elim_step_t* steps, int a_handles_height, int a_handles_width) {
	int a_handles_min_dim = min (a_handles_width, a_handles_height);
	if (a_handles_min_dim == 0) {
		return;
	}

	int current_step_index = 0;

	int* eliminated_tiles_in_col = malloc (a_handles_min_dim * sizeof (int));
	int* triangularized_tiles_in_col = malloc (a_handles_min_dim * sizeof (int));

	for (int j = 0; j < a_handles_min_dim; j++) {
		eliminated_tiles_in_col[j] = triangularized_tiles_in_col[j] = 0;
	}

	while ((triangularized_tiles_in_col[a_handles_min_dim - 1] < a_handles_height - a_handles_min_dim + 1) ||
		   (eliminated_tiles_in_col[a_handles_min_dim - 1] < a_handles_height - a_handles_min_dim)) {
		for (int col = a_handles_min_dim - 1; col >= 0; col--) {
			int triangularized_tiles_new;

			if (col == 0) {
				triangularized_tiles_new = triangularized_tiles_in_col[col] + (a_handles_height - triangularized_tiles_in_col[col]);
				if (a_handles_height - triangularized_tiles_in_col[col] > 0) {
					for (int k = a_handles_height - 1; k >= 0; k--) {
						elim_step_t general_step = {
							.kernel_type = GeneralKernel,
							.col = col,
							.eliminated_row = k
						};

						steps[current_step_index++] = general_step;
					}
				}
			} else {
				triangularized_tiles_new = eliminated_tiles_in_col[col - 1];
				for (int k = triangularized_tiles_in_col[col]; k < triangularized_tiles_new; k++) {
					int eliminated_row = a_handles_height - k - 1;

					elim_step_t general_step = {
						.kernel_type = GeneralKernel,
						.col = col,
						.eliminated_row = eliminated_row
					};

					steps[current_step_index++] = general_step;
				}
			}

			int eliminated_tiles_new = eliminated_tiles_in_col[col] + (triangularized_tiles_in_col[col] - eliminated_tiles_in_col[col]) / 2;

			for (int k = eliminated_tiles_in_col[col]; k < eliminated_tiles_new; k++) {
				int eliminated_row = a_handles_height - k - 1;
				int pivot_row = eliminated_row - (triangularized_tiles_in_col[col] - eliminated_tiles_in_col[col]) / 2;

				elim_step_t tt_step = {
					.kernel_type = TtKernel,
					.col = col,
					.pivot_row = pivot_row,
					.eliminated_row = eliminated_row
				};

				steps[current_step_index++] = tt_step;
			}

			triangularized_tiles_in_col[col] = triangularized_tiles_new;
			eliminated_tiles_in_col[col] = eliminated_tiles_new;
		}
	}
}

void generate_tt_binary_tree_steps (elim_step_t* steps, int a_handles_height, int a_handles_width) {
	int a_handles_min_dim = min (a_handles_width, a_handles_height);
	int current_step_index = 0;

	for (int eliminated_col = 0; eliminated_col < a_handles_min_dim; eliminated_col++) {
		for (int eliminated_row = eliminated_col; eliminated_row < a_handles_height; eliminated_row++) {
			elim_step_t general_step = {
				.kernel_type = GeneralKernel,
				.col = eliminated_col,
				.eliminated_row = eliminated_row
			};

			steps[current_step_index++] = general_step;
		}

		for (int spacing = 1; spacing < a_handles_height - eliminated_col; spacing *= 2) {
			for (int pivot_row = eliminated_col; pivot_row < a_handles_height; pivot_row += 2 * spacing) {
				int eliminated_row = pivot_row + spacing;

				if (eliminated_row < a_handles_height) {
					elim_step_t tt_step = {
						.kernel_type = TtKernel,
						.col = eliminated_col,
						.pivot_row = pivot_row,
						.eliminated_row = eliminated_row
					};

					steps[current_step_index++] = tt_step;
				}
			}
		}
	}
}

void generate_tt_fibonacci_steps (elim_step_t* steps, int a_handles_height, int a_handles_width) {
	int a_handles_min_dim = min (a_handles_width, a_handles_height);
	int current_step_index = 0;

	for (int eliminated_col = 0; eliminated_col < a_handles_min_dim; eliminated_col++) {
		for (int eliminated_row = eliminated_col; eliminated_row < a_handles_height; eliminated_row++) {
			elim_step_t general_step = {
				.kernel_type = GeneralKernel,
				.col = eliminated_col,
				.eliminated_row = eliminated_row
			};

			steps[current_step_index++] = general_step;
		}

		int rows_to_eliminate = a_handles_height - eliminated_col;

		int k = get_lower_k_for_sum_of_positive_integers (rows_to_eliminate - 1);
		int sum = k * (k + 1) / 2;

		for (int eliminated_row = eliminated_col + sum + 1; eliminated_row < a_handles_height; eliminated_row++) {
			elim_step_t tt_step = {
				.kernel_type = TtKernel,
				.col = eliminated_col,
				.pivot_row = eliminated_row - (rows_to_eliminate - sum) + 1,
				.eliminated_row = eliminated_row
			};

			steps[current_step_index++] = tt_step;
		}

		for (int sum_element = k; sum_element > 0; sum_element--) {
			for (int eliminated_row = eliminated_col + sum; eliminated_row > eliminated_col + sum - sum_element; eliminated_row--) {
				elim_step_t tt_step = {
					.kernel_type = TtKernel,
					.col = eliminated_col,
					.pivot_row = eliminated_row - sum_element,
					.eliminated_row = eliminated_row
				};

				steps[current_step_index++] = tt_step;
			}

			sum -= sum_element;
		}
	}
}

void generate_superblock_greedy_steps (elim_step_t* steps, int a_handles_height, int a_handles_width, elim_context_t context) {
	int a_handles_min_dim = min (a_handles_width, a_handles_height);
	if (a_handles_min_dim == 0) {
		return;
	}

	int current_step_index = 0;

	int superblock_size = get_superblock_size (context);

	int* eliminated_tiles_in_col = malloc (a_handles_min_dim * sizeof (int));
	int* triangularized_tiles_in_col = malloc (a_handles_min_dim * sizeof (int));

	for (int j = 0; j < a_handles_min_dim; j++) {
		eliminated_tiles_in_col[j] = triangularized_tiles_in_col[j] = 0;
	}

	while ((triangularized_tiles_in_col[a_handles_min_dim - 1] < get_superblock_index (a_handles_height - a_handles_min_dim + 1, superblock_size) ||
		   (eliminated_tiles_in_col[a_handles_min_dim - 1] < get_superblock_index (a_handles_height - a_handles_min_dim + 1, superblock_size) - 1))) {
		for (int col = a_handles_min_dim - 1; col >= 0; col--) {
			int triangularized_tiles_new;

			if (col == 0) {
				triangularized_tiles_new = triangularized_tiles_in_col[col] + (get_superblock_index (a_handles_height, superblock_size) - triangularized_tiles_in_col[col]);
				if (get_superblock_index (a_handles_height, superblock_size) - triangularized_tiles_in_col[col] > 0) {
					for (int k = get_superblock_index (a_handles_height, superblock_size) - 1; k >= 0; k--) {
						// TS Flat tree
						int first_block_row = col + k * superblock_size;

						elim_step_t general_step = {
							.kernel_type = GeneralKernel,
							.col = col,
							.eliminated_row = first_block_row
						};

						steps[current_step_index++] = general_step;

						for (int block_row = first_block_row + 1; block_row < first_block_row + min (superblock_size, a_handles_height - first_block_row); block_row++) {
							elim_step_t ts_step = {
								.kernel_type = TsKernel,
								.col = col,
								.pivot_row = first_block_row,
								.eliminated_row = block_row
							};

							steps[current_step_index++] = ts_step;
						}
					}
				}
			} else {
				if ((a_handles_height - (col - 1)) % superblock_size != 1 && superblock_size > 1 && triangularized_tiles_in_col[col - 1] > eliminated_tiles_in_col[col - 1]) {
                    triangularized_tiles_new = eliminated_tiles_in_col[col - 1] + 1;
                }
                else {
                    triangularized_tiles_new = eliminated_tiles_in_col[col - 1];
                }

				for (int k = triangularized_tiles_in_col[col]; k < triangularized_tiles_new; k++) {
					// TS Flat tree
					int first_block_row = get_superblock_index (a_handles_height - col, superblock_size) - k - 1;
					first_block_row = col + first_block_row * superblock_size;

					elim_step_t general_step = {
						.kernel_type = GeneralKernel,
						.col = col,
						.eliminated_row = first_block_row
					};

					steps[current_step_index++] = general_step;

					for (int block_row = first_block_row + 1; block_row < first_block_row + min (superblock_size, a_handles_height - first_block_row); block_row++) {
						elim_step_t ts_step = {
							.kernel_type = TsKernel,
							.col = col,
							.pivot_row = first_block_row,
							.eliminated_row = block_row
						};

						steps[current_step_index++] = ts_step;
					}
				}
			}

			int eliminated_tiles_new = eliminated_tiles_in_col[col] + (triangularized_tiles_in_col[col] - eliminated_tiles_in_col[col]) / 2;

			for (int k = eliminated_tiles_in_col[col]; k < eliminated_tiles_new; k++) {
				int eliminated_row = get_superblock_index (a_handles_height - col, superblock_size) - k - 1;
				int pivot_row = eliminated_row - (triangularized_tiles_in_col[col] - eliminated_tiles_in_col[col]) / 2;

				eliminated_row = col + eliminated_row * superblock_size;
				pivot_row = col + pivot_row * superblock_size;


				elim_step_t tt_step = {
					.kernel_type = TtKernel,
					.col = col,
					.pivot_row = pivot_row,
					.eliminated_row = eliminated_row
				};

				steps[current_step_index++] = tt_step;
			}

			triangularized_tiles_in_col[col] = triangularized_tiles_new;
			eliminated_tiles_in_col[col] = eliminated_tiles_new;
		}
	}
}

void generate_superblock_binary_tree_steps (elim_step_t* steps, int a_handles_height, int a_handles_width, elim_context_t context) {
	int a_handles_min_dim = min (a_handles_width, a_handles_height);
	int current_step_index = 0;

	int superblock_size = get_superblock_size (context);

	for (int eliminated_col = 0; eliminated_col < a_handles_min_dim; eliminated_col++) {
		for (int superblock_start = eliminated_col; superblock_start < a_handles_height; superblock_start += superblock_size) {
			elim_step_t general_step = {
				.kernel_type = GeneralKernel,
				.col = eliminated_col,
				.eliminated_row = superblock_start
			};

			steps[current_step_index++] = general_step;

			for (int superblock_row = 1; superblock_row < min (superblock_size, a_handles_height - superblock_start); superblock_row++) {
				elim_step_t ts_step = {
					.kernel_type = TsKernel,
					.col = eliminated_col,
					.pivot_row = superblock_start,
					.eliminated_row = superblock_start + superblock_row
				};

				steps[current_step_index++] = ts_step;
			}
		}

		for (int spacing = superblock_size; spacing < a_handles_height - eliminated_col; spacing *= 2) {
			for (int pivot_row = eliminated_col; pivot_row < a_handles_height; pivot_row += 2 * spacing) {
				int eliminated_row = pivot_row + spacing;

				if (eliminated_row < a_handles_height) {
					elim_step_t tt_step = {
						.kernel_type = TtKernel,
						.col = eliminated_col,
						.pivot_row = pivot_row,
						.eliminated_row = eliminated_row
					};

					steps[current_step_index++] = tt_step;
				}
			}
		}
	}
}

void generate_superblock_fibonacci_steps (elim_step_t* steps, int a_handles_height, int a_handles_width, elim_context_t context) {
	int a_handles_min_dim = min (a_handles_width, a_handles_height);
	int current_step_index = 0;

	int superblock_size = get_superblock_size (context);

	for (int eliminated_col = 0; eliminated_col < a_handles_min_dim; eliminated_col++) {
		for (int superblock_start = eliminated_col; superblock_start < a_handles_height; superblock_start += superblock_size) {
			elim_step_t general_step = {
				.kernel_type = GeneralKernel,
				.col = eliminated_col,
				.eliminated_row = superblock_start
			};

			steps[current_step_index++] = general_step;

			for (int superblock_row = 1; superblock_row < min (superblock_size, a_handles_height - superblock_start); superblock_row++) {
				elim_step_t ts_step = {
					.kernel_type = TsKernel,
					.col = eliminated_col,
					.pivot_row = superblock_start,
					.eliminated_row = superblock_start + superblock_row
				};

				steps[current_step_index++] = ts_step;
			}
		}

		int superblocks_to_eliminate = (a_handles_height - eliminated_col - 1) / superblock_size + 1;

		int k = get_lower_k_for_sum_of_positive_integers (superblocks_to_eliminate - 1);
		int sum = k * (k + 1) / 2;

		for (int eliminated_row = eliminated_col + (sum + 1) * superblock_size; eliminated_row < a_handles_height; eliminated_row += superblock_size) {
			elim_step_t tt_step = {
				.kernel_type = TtKernel,
				.col = eliminated_col,
				.pivot_row = eliminated_row + (sum + 1 - superblocks_to_eliminate) * superblock_size,
				.eliminated_row = eliminated_row
			};

			steps[current_step_index++] = tt_step;
		}

		for (int sum_element = k; sum_element > 0; sum_element--) {
			for (int eliminated_row = eliminated_col + sum * superblock_size; eliminated_row > eliminated_col + (sum - sum_element) * superblock_size; eliminated_row -= superblock_size) {
				elim_step_t tt_step = {
					.kernel_type = TtKernel,
					.col = eliminated_col,
					.pivot_row = eliminated_row - sum_element * superblock_size,
					.eliminated_row = eliminated_row
				};

				steps[current_step_index++] = tt_step;
			}

			sum -= sum_element;
		}
	}
}

void generate_auto_tree_steps (elim_step_t* steps, int a_handles_height, int a_handles_width, elim_context_t context) {
	int a_handles_min_dim = min (a_handles_width, a_handles_height);
	int current_step_index = 0;

	for (int eliminated_col = 0; eliminated_col < a_handles_min_dim; eliminated_col++) {
		int superblock_size = get_auto_tree_superblock_size (context, eliminated_col);

		for (int superblock_start = eliminated_col; superblock_start < a_handles_height; superblock_start += superblock_size) {
			elim_step_t general_step = {
				.kernel_type = GeneralKernel,
				.col = eliminated_col,
				.eliminated_row = superblock_start
			};

			steps[current_step_index++] = general_step;

			for (int superblock_row = 1; superblock_row < min (superblock_size, a_handles_height - superblock_start); superblock_row++) {
				elim_step_t ts_step = {
					.kernel_type = TsKernel,
					.col = eliminated_col,
					.pivot_row = superblock_start,
					.eliminated_row = superblock_start + superblock_row
				};

				steps[current_step_index++] = ts_step;
			}
		}

		int total_no_superblocks = (a_handles_height - eliminated_col - 1) / superblock_size + 1;
		int superblocks_to_eliminate = total_no_superblocks;

		while (superblocks_to_eliminate > 1) {
			int last_noneliminated_superblock_start = eliminated_col + (superblocks_to_eliminate - 1) * superblock_size;

			for (int eliminated_superblock_start = last_noneliminated_superblock_start; eliminated_superblock_start > last_noneliminated_superblock_start - (superblocks_to_eliminate / 2) * superblock_size; eliminated_superblock_start -= superblock_size) {
				int pivot_superblock_start = eliminated_superblock_start - (superblocks_to_eliminate / 2) * superblock_size;

				elim_step_t tt_step = {
					.kernel_type = TtKernel,
					.col = eliminated_col,
					.pivot_row = pivot_superblock_start,
					.eliminated_row = eliminated_superblock_start
				};

				steps[current_step_index++] = tt_step;
			}

			superblocks_to_eliminate = (superblocks_to_eliminate + 1) / 2;
		}
	}
}

void generate_steps_for_elim_scheme (elim_step_t* steps, int a_handles_height, int a_handles_width, elim_context_t context) {
	switch (context.config.elim_scheme) {
		case TsFlatTree:
			generate_ts_flat_tree_steps (steps, a_handles_height, a_handles_width);
			return;
		case TtFlatTree:
			generate_tt_flat_tree_steps (steps, a_handles_height, a_handles_width);
			return;
		case TtGreedy:
			generate_tt_greedy_steps (steps, a_handles_height, a_handles_width);
			return;
		case TtBinaryTree:
			generate_tt_binary_tree_steps (steps, a_handles_height, a_handles_width);
			return;
		case TtFibonacci:
			generate_tt_fibonacci_steps (steps, a_handles_height, a_handles_width);
			return;
		case SuperblockGreedy:
			generate_superblock_greedy_steps (steps, a_handles_height, a_handles_width, context);
			return;
		case SuperblockBinaryTree:
			generate_superblock_binary_tree_steps (steps, a_handles_height, a_handles_width, context);
			return;
		case SuperblockFibonacci:
			generate_superblock_fibonacci_steps (steps, a_handles_height, a_handles_width, context);
			return;
		case AutoTree:
			generate_auto_tree_steps (steps, a_handles_height, a_handles_width, context);
			return;
	}
}

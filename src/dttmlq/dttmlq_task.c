#include "dttmlq_task.h"
#include "dttmlq.h"

#if defined (USE_STARPU)
#include "../common/starpu_matrix_utils.h"

void dttmlq_func (void* buffers[], void* args) {
	int m1 = STARPU_COL_MATRIX_GET_NY(buffers[1]);
	int m2 = STARPU_COL_MATRIX_GET_NY(buffers[3]);
	int n1 = STARPU_COL_MATRIX_GET_NX(buffers[1]);
	int n2 = STARPU_COL_MATRIX_GET_NX(buffers[3]);
	int k = STARPU_COL_MATRIX_GET_NY(buffers[0]);

	int ib = STARPU_COL_MATRIX_GET_NY(buffers[5]);

	int ldv = STARPU_COL_MATRIX_GET_LD(buffers[0]);
	int lda1 = STARPU_COL_MATRIX_GET_LD(buffers[1]);
	int lda2 = STARPU_COL_MATRIX_GET_LD(buffers[3]);
	int ldt = STARPU_COL_MATRIX_GET_LD(buffers[5]);

	double* V = (double*)STARPU_MATRIX_GET_PTR(buffers[0]);
	double* A1 = (double*)STARPU_MATRIX_GET_PTR(buffers[1]);
	double* A2 = (double*)STARPU_MATRIX_GET_PTR(buffers[3]);
	double* T = (double*)STARPU_MATRIX_GET_PTR(buffers[5]);

	char side, trans;
	double** workspace;
	starpu_codelet_unpack_args (args, &side, &trans, &workspace);

	double* work = workspace[starpu_worker_get_id ()];

	dttmlq (side, trans, m1, n1, m2, n2, k, ib, A1, lda1, A2, lda2, V, ldv, T, ldt, work);
}

struct starpu_codelet dttmlq_codelet = {
	.cpu_funcs = { dttmlq_func },
	.cpu_funcs_name = { "dttmlq" },
	.nbuffers = 6,
	.modes = { STARPU_R, STARPU_RW, STARPU_RW, STARPU_RW, STARPU_RW, STARPU_R },
	.name = "DTTMLQ"
};

void insert_dttmlq_task (char side, char trans, data_handle_t lower_triangle_v_handle,
						 data_handle_t upper_triangle_a1_handle, data_handle_t lower_triangle_a1_handle,
						 data_handle_t upper_triangle_a2_handle, data_handle_t lower_triangle_a2_handle,
						 data_handle_t t_handle, double** workspace) {
	starpu_task_insert (
		&dttmlq_codelet,
		STARPU_R, lower_triangle_v_handle,
		STARPU_RW, upper_triangle_a1_handle,
		STARPU_RW, lower_triangle_a1_handle,
		STARPU_RW, upper_triangle_a2_handle,
		STARPU_RW, lower_triangle_a2_handle,
		STARPU_R, t_handle,
		STARPU_VALUE, &side, sizeof(char),
		STARPU_VALUE, &trans, sizeof(char),
		STARPU_VALUE, &workspace, sizeof (double**),
		0
	);
}
#elif defined (USE_OPENMP)
#include <omp.h>

void insert_dttmlq_task (char side, char trans, data_handle_t lower_triangle_v_handle,
						 data_handle_t upper_triangle_a1_handle, data_handle_t lower_triangle_a1_handle,
						 data_handle_t upper_triangle_a2_handle, data_handle_t lower_triangle_a2_handle,
						 data_handle_t t_handle, double** workspace) {
	// Do not mark lower_triangle_a1_handle & lower_triangle_a2_handle as unused
	(void)lower_triangle_a1_handle;
	(void)lower_triangle_a2_handle;

	#pragma omp task \
			depend(in: *lower_triangle_v_handle.ptr) \
			depend(inout: *upper_triangle_a1_handle.ptr) \
			depend(inout: *lower_triangle_a1_handle.ptr) \
			depend(inout: *upper_triangle_a2_handle.ptr) \
			depend(inout: *lower_triangle_a2_handle.ptr) \
			depend(in: *t_handle.ptr)
	{
		double* work = workspace[omp_get_thread_num ()];

		dttmlq (
			side,
			trans,
			upper_triangle_a1_handle.height,
			upper_triangle_a1_handle.width,
			upper_triangle_a2_handle.height,
			upper_triangle_a2_handle.width,
			lower_triangle_v_handle.height,
			t_handle.height,
			upper_triangle_a1_handle.ptr,
			upper_triangle_a1_handle.leading_dimension,
			upper_triangle_a2_handle.ptr,
			upper_triangle_a2_handle.leading_dimension,
			lower_triangle_v_handle.ptr,
			lower_triangle_v_handle.leading_dimension,
			t_handle.ptr,
			t_handle.leading_dimension,
			work
		);
	}
}
#endif
#ifndef DTTMLQ_H_
#define DTTMLQ_H_

// TODO: document
void dttmlq_inner_block (char side, char trans, int m1, int n1, int m2, int n2, int k, int additional_V_full_cols, double* A1, int lda1, double* A2, int lda2, double* V, int ldv, double* T, int ldt, double* work);

int dttmlq (char side, char trans, int m1, int n1, int m2, int n2, int k, int mb, double* A1, int lda1, double* A2, int lda2, double* V, int ldv, double* T, int ldt, double* work);

#endif /* DTTMLQ_H_ */
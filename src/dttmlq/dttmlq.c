#include "dttmlq.h"
#include "../common/value_checking.h"
#include "../common/math_utils.h"

#include <memory.h>

#define A1_ENTRY(i,j) A1[i + (j) * lda1]
#define A2_ENTRY(i,j) A2[i + (j) * lda2]
#define V_ENTRY(i,j) V[i + (j) * ldv]
#define T_ENTRY(i,j) T[i + (j) * ldt]
#define WORK_ENTRY(i,j) work[i + (j) * (IS_LEFT(side) ? k : m1)]

#if defined (USE_MKL)
#include <mkl_cblas.h>
#define USE_EXTERNAL_BLAS
#elif defined (USE_ARMPL)
#include <armpl.h>
#define USE_EXTERNAL_BLAS
#endif

void dttmlq_inner_block (char side, char trans, int m1, int n1, int m2, int n2, int k, int additional_V_full_cols, double* A1, int lda1, double* A2, int lda2, double* V, int ldv, double* T, int ldt, double* work) {
	if (IS_LEFT (side)) {
		int n = n1;

		// work := A1 + V * A2
		#ifdef USE_EXTERNAL_BLAS
			for (int col = 0; col < n; col++) {
				cblas_dcopy (clamp (m2 - additional_V_full_cols, 0, k), &A2_ENTRY(additional_V_full_cols, col), 1, &WORK_ENTRY(0, col), 1);
				cblas_dscal (clamp (k - m2 + additional_V_full_cols, 0, k), 0.0, &WORK_ENTRY(clamp (m2 - additional_V_full_cols, 0, k), col), 1);
			}

			cblas_dtrmm (CblasColMajor, CblasLeft, CblasLower, CblasNoTrans, CblasNonUnit, clamp (m2 - additional_V_full_cols, 0, k), n, 1.0, &V_ENTRY(0, additional_V_full_cols), ldv, work, k);

			cblas_dgemm (CblasColMajor, CblasNoTrans, CblasNoTrans, clamp (k - m2 + additional_V_full_cols, 0, k), n, clamp (m2 - additional_V_full_cols, 0, k), 1.0, &V_ENTRY(m2 - additional_V_full_cols, additional_V_full_cols), ldv, &A2_ENTRY(additional_V_full_cols, 0), lda2, 1.0, &WORK_ENTRY(m2 - additional_V_full_cols, 0), k);

			cblas_dgemm (CblasColMajor, CblasNoTrans, CblasNoTrans, k, n, min (additional_V_full_cols, m2), 1.0, V, ldv, A2, lda2, 1.0, work, k);

			for (int col = 0; col < n; col++) {
				cblas_daxpy (k, 1.0, &A1_ENTRY(0, col), 1, &WORK_ENTRY(0, col), 1);
			}
		#else
			for (int col = 0; col < n; col++) {
				memcpy (&WORK_ENTRY(0, col), &A1_ENTRY(0, col), k * sizeof (double));
			}

			for (int row = 0; row < k; row++) {
				for (int col = 0; col < n; col++) {
					for (int l = 0; l < min (row + additional_V_full_cols + 1, m2); l++) {
						WORK_ENTRY(row, col) += V_ENTRY(row, l) * A2_ENTRY(l, col);
					}
				}
			}
		#endif

		if (IS_NOT_TRANS (trans)) {
			// work := T^T * work
			#ifdef USE_EXTERNAL_BLAS
				cblas_dtrmm (CblasColMajor, CblasLeft, CblasUpper, CblasTrans, CblasNonUnit, k, n, 1.0, T, ldt, work, k);
			#else
				for (int col = 0; col < n; col++) {
					for (int row = k - 1; row >= 0; row--) {
						WORK_ENTRY(row, col) *= T_ENTRY(row, row);

						for (int l = row - 1; l >= 0; l--) {
							WORK_ENTRY(row, col) += T_ENTRY(l, row) * WORK_ENTRY(l, col);
						}
					}
				}
			#endif
		} else {
			// work := T * work
			#ifdef USE_EXTERNAL_BLAS
				cblas_dtrmm (CblasColMajor, CblasLeft, CblasUpper, CblasNoTrans, CblasNonUnit, k, n, 1.0, T, ldt, work, k);
			#else
				for (int col = 0; col < n; col++) {
					for (int row = 0; row < k; row++) {
						WORK_ENTRY(row, col) *= T_ENTRY(row, row);

						for (int l = row + 1; l < k; l++) {
							WORK_ENTRY(row, col) += T_ENTRY(row, l) * WORK_ENTRY(l, col);
						}
					}
				}
			#endif
		}

		// A1 := A1 - work
		#ifdef USE_EXTERNAL_BLAS
			for (int col = 0; col < n; col++) {
				cblas_daxpy (k, -1.0, &WORK_ENTRY(0, col), 1, &A1_ENTRY(0, col), 1);
			}
		#else
			for (int col = 0; col < n; col++) {
				for (int row = 0; row < k; row++) {
					A1_ENTRY(row, col) -= WORK_ENTRY(row, col);
				}
			}
		#endif

		// A2 = A2 - V^T * work
		#ifdef USE_EXTERNAL_BLAS
			cblas_dgemm (CblasColMajor, CblasTrans, CblasNoTrans, min (additional_V_full_cols, m2), n, k, -1.0, V, ldv, work, k, 1.0, A2, lda2);

			cblas_dgemm (CblasColMajor, CblasTrans, CblasNoTrans, clamp (m2 - additional_V_full_cols, 0, k), n, clamp (k - m2 + additional_V_full_cols, 0, k), -1.0, &V_ENTRY(m2 - additional_V_full_cols, additional_V_full_cols), ldv, &WORK_ENTRY(m2 - additional_V_full_cols, 0), k, 1.0, &A2_ENTRY(additional_V_full_cols, 0), lda2);

			cblas_dtrmm (CblasColMajor, CblasLeft, CblasLower, CblasTrans, CblasNonUnit, clamp (m2 - additional_V_full_cols, 0, k), n, 1.0, &V_ENTRY(0, additional_V_full_cols), ldv, work, k);

			for (int col = 0; col < n; col++) {
				cblas_daxpy (clamp (m2 - additional_V_full_cols, 0, k), -1.0, &WORK_ENTRY(0, col), 1, &A2_ENTRY(additional_V_full_cols, col), 1);
			}
		#else
			for (int col = 0; col < n; col++) {
				for (int row = 0; row < m2; row++) {
					for (int l = max (row - additional_V_full_cols, 0); l < k; l++) {
						A2_ENTRY(row, col) -= V_ENTRY(l, row) * WORK_ENTRY(l, col);
					}
				}
			}
		#endif
	} else {
		int m = m1;

		// work := A1 + A2 * V^T
		#ifdef USE_EXTERNAL_BLAS
			for (int col = 0; col < clamp (n2 - additional_V_full_cols, 0, k); col++) {
				cblas_dcopy (m, &A2_ENTRY(0, additional_V_full_cols + col), 1, &WORK_ENTRY(0, col), 1);
			}

			for (int col = clamp (n2 - additional_V_full_cols, 0, k); col < k; col++) {
				cblas_dscal (m, 0.0, &WORK_ENTRY(0, col), 1);
			}

			cblas_dtrmm (CblasColMajor, CblasRight, CblasLower, CblasTrans, CblasNonUnit, m, clamp (n2 - additional_V_full_cols, 0, k), 1.0, &V_ENTRY(0, additional_V_full_cols), ldv, work, m);

			cblas_dgemm (CblasColMajor, CblasNoTrans, CblasTrans, m, clamp (k - n2 + additional_V_full_cols, 0, k), max (n2 - additional_V_full_cols, 0), 1.0, &A2_ENTRY(0, additional_V_full_cols), lda2, &V_ENTRY(n2 - additional_V_full_cols, additional_V_full_cols), ldv, 1.0, &WORK_ENTRY(0, n2 - additional_V_full_cols), m);

			cblas_dgemm (CblasColMajor, CblasNoTrans, CblasTrans, m, k, min (additional_V_full_cols, n2), 1.0, A2, lda2, V, ldv, 1.0, work, m);

			for (int col = 0; col < k; col++) {
				cblas_daxpy (m, 1.0, &A1_ENTRY(0, col), 1, &WORK_ENTRY(0, col), 1);
			}
		#else
			for (int col = 0; col < k; col++) {
				memcpy (&WORK_ENTRY(0, col), &A1_ENTRY(0, col), m * sizeof (double));
			}

			// work := work + A2 * V^T
			for (int col = 0; col < k; col++) {
				for (int l = 0; l < min (n2, col + additional_V_full_cols + 1); l++) {
					for (int row = 0; row < m; row++) {
						WORK_ENTRY(row, col) += A2_ENTRY(row, l) * V_ENTRY(col, l);
					}
				}
			}
		#endif

		if (IS_NOT_TRANS (trans)) {
			// work := work * T^T
			#ifdef USE_EXTERNAL_BLAS
				cblas_dtrmm (CblasColMajor, CblasRight, CblasUpper, CblasTrans, CblasNonUnit, m, k, 1.0, T, ldt, work, m);
			#else
				for (int col = 0; col < k; col++) {
					for (int row = 0; row < m; row++) {
						WORK_ENTRY(row, col) *= T_ENTRY(col, col);

						for (int l = col + 1; l < k; l++) {
							WORK_ENTRY(row, col) += WORK_ENTRY(row, l) * T_ENTRY(col, l);
						}
					}
				}
			#endif
		} else {
			// work := work * T
			#ifdef USE_EXTERNAL_BLAS
				cblas_dtrmm (CblasColMajor, CblasRight, CblasUpper, CblasNoTrans, CblasNonUnit, m, k, 1.0, T, ldt, work, m);
			#else
				for (int row = 0; row < m; row++) {
					for (int col = k - 1; col >= 0; col--) {
						WORK_ENTRY(row, col) *= T_ENTRY(col, col);

						for (int l = col - 1; l >= 0; l--) {
							WORK_ENTRY(row, col) += WORK_ENTRY(row, l) * T_ENTRY(l, col);
						}
					}
				}
			#endif
		}

		// A1 := A1 - work
		#ifdef USE_EXTERNAL_BLAS
			for (int col = 0; col < k; col++) {
				cblas_daxpy (m, -1.0, &WORK_ENTRY(0, col), 1, &A1_ENTRY(0, col), 1);
			}
		#else
			for (int col = 0; col < k; col++) {
				for (int row = 0; row < m; row++) {
					A1_ENTRY(row, col) -= WORK_ENTRY(row, col);
				}
			}
		#endif

		// A2 := A2 - work * V
		#ifdef USE_EXTERNAL_BLAS
			cblas_dgemm (CblasColMajor, CblasNoTrans, CblasNoTrans, m, min (additional_V_full_cols, n2), k, -1.0, work, m, V, ldv, 1.0, A2, lda2);

			cblas_dgemm (CblasColMajor, CblasNoTrans, CblasNoTrans, m, clamp (n2 - additional_V_full_cols, 0, k), clamp (k - n2 + additional_V_full_cols, 0, k), -1.0, &WORK_ENTRY(0, n2 - additional_V_full_cols), m, &V_ENTRY(n2 - additional_V_full_cols, additional_V_full_cols), ldv, 1.0, &A2_ENTRY(0, additional_V_full_cols), lda2);

			cblas_dtrmm (CblasColMajor, CblasRight, CblasLower, CblasNoTrans, CblasNonUnit, m, clamp (n2 - additional_V_full_cols, 0, k), 1.0, &V_ENTRY(0, additional_V_full_cols), ldv, work, m);

			for (int col = 0; col < clamp (n2 - additional_V_full_cols, 0, k); col++) {
				cblas_daxpy (m, -1.0, &WORK_ENTRY(0, col), 1, &A2_ENTRY(0, additional_V_full_cols + col), 1);
			}
		#else
			for (int col = 0; col < n2; col++) {
				for (int l = max (col - additional_V_full_cols, 0); l < k; l++) {
					for (int row = 0; row < m; row++) {
						A2_ENTRY(row, col) -= WORK_ENTRY(row, l) * V_ENTRY(l, col);
					}
				}
			}
		#endif
	}
}

int dttmlq (char side, char trans, int m1, int n1, int m2, int n2, int k, int nb, double* A1, int lda1, double* A2, int lda2, double* V, int ldv, double* T, int ldt, double* work) {
	// Check parameter values
	if (!VALID_SIDE_VALUE (side)) {
		return 1;
	} else if (!VALID_TRANS_VALUE (trans)) {
		return 2;
	} else if (m1 < 0) {
		return 3;
	} else if (n1 < 0) {
		return 4;
	} else if (m2 < 0 || (IS_RIGHT (side) && m1 != m2)) {
		return 5;
	} else if (n2 < 0 || (IS_LEFT (side) && n1 != n2)) {
		return 6;
	} else if (k < 0) {
		return 7;
	} else if ((IS_LEFT (side) && k > m1) || (IS_RIGHT (side) && k > n1)) {
		return 7;
	} else if (nb <= 0) {
		return 8;
	} else if (lda1 < max (1, m1)) {
		return 10;
	} else if (lda2 < max (1, m2)) {
		return 12;
	} else if (ldv < max (1, k)) {
		return 14;
	} else if (ldt < max (1, nb)) {
		return 16;
	}

	if (IS_LEFT (side) && IS_NOT_TRANS (trans)) {
		for (int block_start = 0; block_start < k; block_start += nb) {
			int block_size = min (nb, k - block_start);

			dttmlq_inner_block ('L', 'N', m1 - block_start, n1, m2, n2, block_size, block_start, &A1_ENTRY(block_start, 0), lda1, A2, lda2, &V_ENTRY(block_start, 0), ldv, &T_ENTRY(0, block_start), ldt, work);
		}
	} else if (IS_LEFT (side) && IS_TRANS (trans)) {
		for (int block_start = ((k - 1) / nb) * nb; block_start >= 0; block_start -= nb) {
			int block_size = min (nb, k - block_start);

			dttmlq_inner_block ('L', 'T', m1 - block_start, n1, m2, n2, block_size, block_start, &A1_ENTRY(block_start, 0), lda1, A2, lda2, &V_ENTRY(block_start, 0), ldv, &T_ENTRY(0, block_start), ldt, work);
		}
	} else if (IS_RIGHT (side) && IS_NOT_TRANS (trans)) {
		for (int block_start = ((k - 1) / nb) * nb; block_start >= 0; block_start -= nb) {
			int block_size = min (nb, k - block_start);

			dttmlq_inner_block ('R', 'N', m1, n1 - block_start, m2, n2, block_size, block_start, &A1_ENTRY(0, block_start), lda1, A2, lda2, &V_ENTRY(block_start, 0), ldv, &T_ENTRY(0, block_start), ldt, work);
		}
	} else {
		for (int block_start = 0; block_start < k; block_start += nb) {
			int block_size = min (nb, k - block_start);

			dttmlq_inner_block ('R', 'T', m1, n1 - block_start, m2, n2, block_size, block_start, &A1_ENTRY(0, block_start), lda1, A2, lda2, &V_ENTRY(block_start, 0), ldv, &T_ENTRY(0, block_start), ldt, work);
		}
	}

	return 0;
}

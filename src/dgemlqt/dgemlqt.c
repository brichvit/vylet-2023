#include "dgemlqt.h"
#include "../common/value_checking.h"
#include "../common/math_utils.h"

#include <memory.h>

#define C_ENTRY(i,j) C[i + (j) * ldc]
#define V_ENTRY(i,j) V[i + (j) * ldv]
#define T_ENTRY(i,j) T[i + (j) * ldt]
#define WORK_ENTRY(i,j) work[i + (j) * (IS_LEFT(side) ? k : m)]

#if defined (USE_MKL)
#include <mkl_cblas.h>
#define USE_EXTERNAL_BLAS
#elif defined (USE_ARMPL)
#include <armpl.h>
#define USE_EXTERNAL_BLAS
#endif

void dgemlqt_inner_block (char side, char trans, int m, int n, int k, double* V, int ldv, double* T, int ldt, double* C, int ldc, double* work) {
	if (IS_LEFT (side)) {
		// work := (upper trapezoidal part of V) * C
		#ifdef USE_EXTERNAL_BLAS
			for (int col = 0; col < n; col++) {
				cblas_dcopy (k, &C_ENTRY(0, col), 1, &WORK_ENTRY(0, col), 1);
			}

			cblas_dtrmm (CblasColMajor, CblasLeft, CblasUpper, CblasNoTrans, CblasUnit, k, n, 1.0, V, ldv, work, k);

			cblas_dgemm (CblasColMajor, CblasNoTrans, CblasNoTrans, k, n, m - k, 1.0, &V_ENTRY(0, k), ldv, &C_ENTRY(k, 0), ldc, 1.0, work, k);
		#else
			memset (work, 0, k * n * sizeof (double));
			for (int row = 0; row < k; row++) {
				for (int col = 0; col < n; col++) {
					WORK_ENTRY(row, col) += C_ENTRY(row, col);

					for (int l = row + 1; l < m; l++) {
						WORK_ENTRY(row, col) += V_ENTRY(row, l) * C_ENTRY(l, col);
					}
				}
			}
		#endif

		if (IS_NOT_TRANS(trans)) {
			//work := T^T * work
			#ifdef USE_EXTERNAL_BLAS
				cblas_dtrmm (CblasColMajor, CblasLeft, CblasUpper, CblasTrans, CblasNonUnit, k, n, 1.0, T, ldt, work, k);
			#else
				for (int col = 0; col < n; col++) {
					for (int row = k - 1; row >= 0; row--) {
						WORK_ENTRY(row, col) *= T_ENTRY(row, row);

						for (int l = row - 1; l >= 0; l--) {
							WORK_ENTRY(row, col) += T_ENTRY(l, row) * WORK_ENTRY(l, col);
						}
					}
				}
			#endif
		} else {
			//work := T * work
			#ifdef USE_EXTERNAL_BLAS
				cblas_dtrmm (CblasColMajor, CblasLeft, CblasUpper, CblasNoTrans, CblasNonUnit, k, n, 1.0, T, ldt, work, k);
			#else
				for (int col = 0; col < n; col++) {
					for (int row = 0; row < k; row++) {
						WORK_ENTRY(row, col) *= T_ENTRY(row, row);

						for (int l = row + 1; l < k; l++) {
							WORK_ENTRY(row, col) += T_ENTRY(row, l) * WORK_ENTRY(l, col);
						}
					}
				}
			#endif
		}

		// C := C - (upper trapezoidal part of V)^T * work
		#ifdef USE_EXTERNAL_BLAS
			cblas_dgemm (CblasColMajor, CblasTrans, CblasNoTrans, m - k, n, k, -1.0, &V_ENTRY(0, k), ldv, work, k, 1.0, &C_ENTRY(k, 0), ldc);

			cblas_dtrmm (CblasColMajor, CblasLeft, CblasUpper, CblasTrans, CblasUnit, k, n, 1.0, V, ldv, work, k);

			for (int col = 0; col < n; col++) {
				cblas_daxpy (k, -1.0, &WORK_ENTRY(0, col), 1, &C_ENTRY(0, col), 1);
			}
		#else
			for (int l = 0; l < k; l++) {
				for (int col = 0; col < n; col++) {
					C_ENTRY(l, col) -= WORK_ENTRY(l, col);

					for (int row = l + 1; row < m; row++) {
						C_ENTRY(row, col) -= V_ENTRY(l, row) * WORK_ENTRY(l, col);
					}
				}
			}
		#endif
	} else {
		// work := C * (upper trapezoidal part of V)^T
		#ifdef USE_EXTERNAL_BLAS
			for (int col = 0; col < k; col++) {
				cblas_dcopy (m, &C_ENTRY(0, col), 1, &WORK_ENTRY(0, col), 1);
			}

			cblas_dtrmm (CblasColMajor, CblasRight, CblasUpper, CblasTrans, CblasUnit, m, k, 1.0, V, ldv, work, m);

			cblas_dgemm (CblasColMajor, CblasNoTrans, CblasTrans, m, k, n - k, 1.0, &C_ENTRY(0, k), ldc, &V_ENTRY(0, k), ldv, 1.0, work, m);
		#else
			memset (work, 0, m * k * sizeof (double));
			for (int col = 0; col < k; col++) {
				for (int row = 0; row < m; row++) {
					WORK_ENTRY(row, col) += C_ENTRY(row, col);
				}

				for (int l = col + 1; l < n; l++) {
					for (int row = 0; row < m; row++) {
						WORK_ENTRY(row, col) += C_ENTRY(row, l) * V_ENTRY(col, l);
					}
				}
			}
		#endif

		if (IS_NOT_TRANS(trans)) {
			// work := work * T^T
			#ifdef USE_EXTERNAL_BLAS
				cblas_dtrmm (CblasColMajor, CblasRight, CblasUpper, CblasTrans, CblasNonUnit, m, k, 1.0, T, ldt, work, m);
			#else
				for (int col = 0; col < k; col++) {
					for (int row = 0; row < m; row++) {
						WORK_ENTRY(row, col) *= T_ENTRY(col, col);

						for (int l = col + 1; l < k; l++) {
							WORK_ENTRY(row, col) += WORK_ENTRY(row, l) * T_ENTRY(col, l);
						}
					}
				}
			#endif
		} else {
			// work := work * T
			#ifdef USE_EXTERNAL_BLAS
				cblas_dtrmm (CblasColMajor, CblasRight, CblasUpper, CblasNoTrans, CblasNonUnit, m, k, 1.0, T, ldt, work, m);
			#else
				for (int row = 0; row < m; row++) {
					for (int col = k - 1; col >= 0; col--) {
						WORK_ENTRY(row, col) *= T_ENTRY(col, col);

						for (int l = col - 1; l >= 0; l--) {
							WORK_ENTRY(row, col) += WORK_ENTRY(row, l) * T_ENTRY(l, col);
						}
					}
				}
			#endif
		}

		// C := C - work * (upper trapezoidal part of V)
		#ifdef USE_EXTERNAL_BLAS
			cblas_dgemm (CblasColMajor, CblasNoTrans, CblasNoTrans, m, n - k, k, -1.0, work, m, &V_ENTRY(0, k), ldv, 1.0, &C_ENTRY(0, k), ldc);

			cblas_dtrmm (CblasColMajor, CblasRight, CblasUpper, CblasNoTrans, CblasUnit, m, k, 1.0, V, ldv, work, m);

			for (int col = 0; col < k; col++) {
				cblas_daxpy (m, -1.0, &WORK_ENTRY(0, col), 1, &C_ENTRY(0, col), 1);
			}
		#else
			for (int l = 0; l < k; l++) {
				for (int row = 0; row < m; row++) {
					C_ENTRY(row, l) -= WORK_ENTRY(row, l);
				}

				for (int col = l + 1; col < n; col++) {
					for (int row = 0; row < m; row++) {
						C_ENTRY(row, col) -= WORK_ENTRY(row, l) * V_ENTRY(l, col);
					}
				}
			}
		#endif
	}
}

int dgemlqt (char side, char trans, int m, int n, int k, int mb, double* V, int ldv, double* T, int ldt, double* C, int ldc, double* work) {
	// Check parameter values
	if (!VALID_SIDE_VALUE (side)) {
		return 1;
	} else if (!VALID_TRANS_VALUE (trans)) {
		return 2;
	} else if (m < 0) {
		return 3;
	} else if (n < 0) {
		return 4;
	} else if (k < 0) {
		return 5;
	} else if ((IS_LEFT (side) && k > m) || (IS_RIGHT (side) && k > n)) {
		return 5;
	} else if (mb <= 0) {
		return 6;
	} else if (ldv < max (1, k)) {
		return 8;
	} else if (ldt < max (1, mb)) {
		return 10;
	} else if (ldc < max (1, m)) {
		return 12;
	}

	if (IS_LEFT (side) && IS_NOT_TRANS (trans)) {
		for (int block_start = 0; block_start < k; block_start += mb) {
			int block_size = min (mb, k - block_start);

			dgemlqt_inner_block ('L', 'N', m - block_start, n, block_size, &V_ENTRY(block_start, block_start), ldv, &T_ENTRY(0, block_start), ldt, &C_ENTRY(block_start, 0), ldc, work);
		}
	} else if (IS_LEFT (side) && IS_TRANS (trans)) {
		for (int block_start = ((k - 1) / mb) * mb; block_start >= 0; block_start -= mb) {
			int block_size = min (mb, k - block_start);

			dgemlqt_inner_block ('L', 'T', m - block_start, n, block_size, &V_ENTRY(block_start, block_start), ldv, &T_ENTRY(0, block_start), ldt, &C_ENTRY(block_start, 0), ldc, work);
		}
	} else if (IS_RIGHT (side) && IS_NOT_TRANS (trans)) {
		for (int block_start = ((k - 1) / mb) * mb; block_start >= 0; block_start -= mb) {
			int block_size = min (mb, k - block_start);

			dgemlqt_inner_block ('R', 'N', m, n - block_start, block_size, &V_ENTRY(block_start, block_start), ldv, &T_ENTRY(0, block_start), ldt, &C_ENTRY(0, block_start), ldc, work);
		}
	} else {
		for (int block_start = 0; block_start < k; block_start += mb) {
			int block_size = min (mb, k - block_start);

			dgemlqt_inner_block ('R', 'T', m, n - block_start, block_size, &V_ENTRY(block_start, block_start), ldv, &T_ENTRY(0, block_start), ldt, &C_ENTRY(0, block_start), ldc, work);
		}
	}

	return 0;
}

#ifndef DGEMLQT_TASK_H_
#define DGEMLQT_TASK_H_

#include "../common/runtime_system.h"

void insert_dgemlqt_task (char side, char trans, data_handle_t upper_triangle_v_handle,
						  data_handle_t upper_triangle_c_handle, data_handle_t lower_triangle_c_handle,
						  data_handle_t t_handle, double** workspace);

#endif /* DGEMLQT_TASK_H_ */
#ifndef DGEMLQT_H_
#define DGEMLQT_H_

// TODO: document
void dgemlqt_inner_block (char side, char trans, int m, int n, int k, double* V, int ldv, double* T, int ldt, double* C, int ldc, double* work);

int dgemlqt (char side, char trans, int m, int n, int k, int nb, double* V, int ldv, double* T, int ldt, double* C, int ldc, double* work);

#endif /* DGEMLQT_H_ */
#define EPSILON __DBL_EPSILON__
#define NO_TESTS 6

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <setjmp.h>

#include "../lapack/lapack.h"
#include "../lapack/cblas_f77.h"

#include "../../src/common/math_utils.h"
#include "../../src/dgelqt/dgelqt.h"
#include "../../src/dgemlqt/dgemlqt.h"

void perform_general_lq_kernels_test (int m, int n, int nb, double result[NO_TESTS]) {
	int min_mn = min (m, n), max_mn = max (m, n);
	int max_1m = max (1, m), max_1n = max (1, n);
	int max_1mn = max (1, max_mn);

	int ldt = max (1, nb);

	double zero = 0, one = 1, minus_one = -1;

	int seed[] = {1988, 1989, 1990, 1991};

	// Generate random m-by-n matrix A and its copy A_factored
	double* A = malloc (m * n * sizeof (double));
	double* A_factored = malloc (m * n * sizeof (double));
	for (int col = 0; col < n; col++) {
		int distribution = 2;
		LAPACK_dlarnv (&distribution, seed, &m, A + m * col);
	}
	LAPACK_dlacpy ("Full", &m, &n, A, &max_1m, A_factored, &max_1m);

	// Factor A_factored using DGELQT, save the upper triangular factor in T
	double* T = malloc (nb * m * sizeof (double));
	double* work = malloc (max (2, max_mn) * max (2, max_mn) * nb * sizeof (double));
	dgelqt (m, n, nb, A_factored, max_1m, T, ldt, work);

	// Generate the n-by-n matrix Q using DGEMLQT
	double* Q = malloc (n * n * sizeof (double));
	LAPACK_dlaset ("Full", &n, &n, &zero, &one, Q, &max_1n);
	dgemlqt ('R', 'N', n, n, min_mn, nb, A_factored, max_1m, T, ldt, Q, max_1n, work);

	// Generate the max(m,n)-by-n matrix L by copying the lower triangular part of A_factored
	double* L = malloc (max_mn * n * sizeof (double)); // max_mn is used instead of n, because L is used as an n-by-n work matrix in test 2
	LAPACK_dlaset ("Upper", &m, &n, &zero, &zero, L, &max_1mn);
	LAPACK_dlacpy ("Lower", &m, &n, A_factored, &max_1m, L, &max_1mn);

	// Test 1: compute |L - A*Q'| / |A|
	F77_dgemm ("Non-transposed", "Transposed", &m, &n, &n, &minus_one, A, &max_1m, Q, &max_1n, &one, L, &max_1mn);

	double* rwork = malloc (max_mn * sizeof (double));
	double A_norm = LAPACK_dlange ("1-norm", &m, &n, A, &max_1m, rwork);
	double resid_norm = LAPACK_dlange ("1-norm", &m, &n, L, &max_1mn, rwork);

	if (A_norm > 0) {
		result[0] = resid_norm / (EPSILON * max_1m * A_norm);
	} else {
		result[0] = 0;
	}

	// Free A
	free (A);

	// Test 2: compute |I - Q'*Q|
	LAPACK_dlaset ("Upper", &n, &n, &zero, &one, L, &max_1mn);
	F77_dsyrk ("Upper", "Transposed", &n, &n, &minus_one, Q, &max_1n, &one, L, &max_1mn);
	resid_norm = LAPACK_dlansy ("1-norm", "Upper", &n, L, &max_1mn, rwork);
	result[1] = resid_norm / (EPSILON * max_1n);

	// Free L
	free (L);

	// Generate random n-by-m matrix D and its copy D_householder
	double* D = malloc (n * m * sizeof (double));
	double* D_householder = malloc (n * m * sizeof (double));
	for (int col = 0; col < m; col++) {
		int distribution = 2;
		LAPACK_dlarnv (&distribution, seed, &n, D + n * col);
	}
	LAPACK_dlacpy ("Full", &n, &m, D, &max_1n, D_householder, &max_1n);
	
	// Calculate the norm of D
	double D_norm = LAPACK_dlange ("1-norm", &n, &m, D, &max_1n, rwork);

	// Apply Q to D_householder using DGEMLQT
	dgemlqt ('L', 'N', n, m, min_mn, nb, A_factored, max_1m, T, ldt, D_householder, max_1n, work);

	// Test 3: calculate |Q*D_householder - Q*D| / |D|
	// - Q*D_householder is obtained using sequential application of Householder reflectors
	// - Q*D is obtained using standard matrix multiplication
	F77_dgemm ("Non-transposed", "Non-transposed", &n, &m, &n, &minus_one, Q, &max_1n, D, &max_1n, &one, D_householder, &max_1n);
	resid_norm = LAPACK_dlange ("1-norm", &n, &m, D_householder, &max_1n, rwork);

	if (D_norm > 0) {
		result[2] = resid_norm / (EPSILON * max_1m * D_norm);
	} else {
		result[2] = 0;
	}

	// Copy D into D_householder again
	LAPACK_dlacpy ("Full", &n, &m, D, &max_1n, D_householder, &max_1n);

	// Apply Q^T to D_householder using DGEMLQT
	dgemlqt ('L', 'T', n, m, min_mn, nb, A_factored, max_1m, T, ldt, D_householder, max_1n, work);

	// Test 4: calculate |Q^T*D_householder - Q^T*D| / |D|
	// - Q^T*D_householder is obtained using sequential application of Householder reflectors
	// - Q^T*D is obtained using standard matrix multiplication
	F77_dgemm ("Transposed", "Non-transposed", &n, &m, &n, &minus_one, Q, &max_1n, D, &max_1n, &one, D_householder, &max_1n);
	resid_norm = LAPACK_dlange ("1-norm", &n, &m, D_householder, &max_1n, rwork);

	if (D_norm > 0) {
		result[3] = resid_norm / (EPSILON * max_1m * D_norm);
	} else {
		result[3] = 0;
	}

	// Free D and D_householder
	free (D);
	free (D_householder);

	// Generate random m-by-n matrix C and its copy C_householder
	double* C = malloc (m * n * sizeof (double));
	double* C_householder = malloc (m * n * sizeof (double));
	for (int col = 0; col < n; col++) {
		int distribution = 2;
		LAPACK_dlarnv (&distribution, seed, &m, C + m * col);
	}
	LAPACK_dlacpy ("Full", &m, &n, C, &max_1m, C_householder, &max_1m);

	// Calculate the norm of C
	double C_norm = LAPACK_dlange ("1-norm", &m, &n, C, &max_1m, rwork);

	// Apply Q to C_householder using DGEMLQT
	dgemlqt ('R', 'N', m, n, min_mn, nb, A_factored, max_1m, T, ldt, C_householder, max_1m, work);

	// Test 5: compute |C_householder*Q - C*Q| / |C|
	// - C_householder*Q is obtained using sequential application of Householder reflectors
	// - C*Q is obtained using standard matrix multiplication
	F77_dgemm ("Non-transposed", "Non-transposed", &m, &n, &n, &minus_one, C, &max_1m, Q, &max_1n, &one, C_householder, &max_1m);
	resid_norm = LAPACK_dlange ("1-norm", &m, &n, C_householder, &max_1m, rwork);

	if (C_norm > 0) {
		result[4] = resid_norm / (EPSILON * max_1m * C_norm);
	} else {
		result[4] = 0;
	}

	// Copy C into C_householder again
	LAPACK_dlacpy ("Full", &m, &n, C, &max_1m, C_householder, &max_1m);

	// Apply Q^T to C_householder using DGEMLQT
	dgemlqt ('R', 'T', m, n, min_mn, nb, A_factored, max_1m, T, ldt, C_householder, max_1m, work);

	// Test 6: compute |C_householder*Q^T - C*Q^T| / |C|
	// - C_householder*Q^T is obtained using sequential application of Householder reflectors
	// - C*Q^T is obtained using standard matrix multiplication
	F77_dgemm ("Non-transposed", "Transposed", &m, &n, &n, &minus_one, C, &max_1m, Q, &max_1n, &one, C_householder, &max_1m);
	resid_norm = LAPACK_dlange ("1-norm", &m, &n, C_householder, &max_1m, rwork);

	if (C_norm > 0) {
		result[5] = resid_norm / (EPSILON * max_1m * C_norm);
	} else {
		result[5] = 0;
	}

	// Free remaining matrices
	free (A_factored);
	free (T);
	free (Q);
	free (work);

	free (C);
	free (C_householder);
	free (rwork);
}

volatile sig_atomic_t sigsegv_flag = 0;
sigjmp_buf mark;

void sigsegv_handler (int sig) {
	// Do not mark sig as unused
	(void)sig;

	sigsegv_flag = 1;
	siglongjmp (mark, 1);
}

int check_sigsegv_flag (char* routine_name, int param_index) {
	if (sigsegv_flag == 1) {
		fprintf (stderr, "Illegal memory access detected when parameter %d of %s has illegal value\n", param_index, routine_name);
		sigsegv_flag = 0;

		return 1;
	}

	return 0;
}

int test_dgelqt_illegal_param (int m, int n, int nb, double* A, int lda, double* T, int ldt, double* work, int illegal_param_index) {
	int failed = 0;

	if (sigsetjmp (mark, 1) == 0) {
		if (dgelqt (m, n, nb, A, lda, T, ldt, work) != illegal_param_index) {
			fprintf (stderr, "Illegal value of parameter number %d not detected by DGELQT\n", illegal_param_index);
			failed = 1;
		}
	}
	failed |= check_sigsegv_flag ("DGELQT", illegal_param_index);

	return failed;
}

int test_dgemlqt_illegal_param (char side, char trans, int m, int n, int k, int nb, double* V, int ldv, double* T, int ldt, double* C, int ldc, double* work, int illegal_param_index) {
	int failed = 0;

	if (sigsetjmp (mark, 1) == 0) {
		if (dgemlqt (side, trans, m, n, k, nb, V, ldv, T, ldt, C, ldc, work) != illegal_param_index) {
			fprintf (stderr, "Illegal value of parameter number %d not detected by DGEMLQT\n", illegal_param_index);
			failed = 1;
		}
	}
	failed |= check_sigsegv_flag ("DGEMLQT", illegal_param_index);

	return failed;
}

int test_dgemlqt_legal_param (char side, char trans, int m, int n, int k, int nb, double* V, int ldv, double* T, int ldt, double* C, int ldc, double* work, int checked_param_index) {
	if (dgemlqt (side, trans, m, n, k, nb, V, ldv, T, ldt, C, ldc, work) == checked_param_index) {
		fprintf (stderr, "Illegal value of parameter number %d falsely detected by DGEMLQT\n", checked_param_index);
		return 1;
	}

	return 0;
}

int test_dgelqt_error_exits () {
	// Set used matrices to NULL in order to ensure that they are not accessed
	double* A = NULL;
	double* T = NULL;
	double* work = NULL;

	// Attach custom signal handler to SIGSEGV in order to catch illegal memory accesses
	struct sigaction sigsegv_action;
	sigsegv_action.sa_handler = sigsegv_handler;
	sigfillset (&sigsegv_action.sa_mask);
	sigsegv_action.sa_flags = 0;

	sigaction (SIGSEGV, &sigsegv_action, NULL);

	int dgelqt_error_exit_tests_failed = 0;

	// Test error exit 1 (m < 0)
	dgelqt_error_exit_tests_failed |= test_dgelqt_illegal_param (-1, 1, 1, A, 1, T, 1, work, 1);

	// Test error exit 2 (n < 0)
	dgelqt_error_exit_tests_failed |= test_dgelqt_illegal_param (1, -1, 1, A, 1, T, 1, work, 2);

	// Test error exit 3 (nb <= 0)
	dgelqt_error_exit_tests_failed |= test_dgelqt_illegal_param (1, 1, 0, A, 1, T, 1, work, 3);

	// Test error exit 5 (lda < max (1, m))
	dgelqt_error_exit_tests_failed |= test_dgelqt_illegal_param (2, 1, 1, A, 1, T, 1, work, 5);

	// Test error exit 7 (ldt < max (1, nb))
	dgelqt_error_exit_tests_failed |= test_dgelqt_illegal_param (2, 2, 2, A, 2, T, 1, work, 7);

	// Detach custom SIGSEGV handler
	sigsegv_action.sa_handler = SIG_DFL;
	sigaction (SIGSEGV, &sigsegv_action, NULL);

	return dgelqt_error_exit_tests_failed;
}

int test_dgemlqt_error_exits () {
	// Set used matrices to NULL in order to ensure that they are not accessed
	double* A = NULL;
	double* T = NULL;
	double* C = NULL;
	double* work = NULL;

	// Attach custom signal handler to SIGSEGV in order to catch illegal memory accesses
	struct sigaction sigsegv_action;
	sigsegv_action.sa_handler = sigsegv_handler;
	sigfillset (&sigsegv_action.sa_mask);
	sigsegv_action.sa_flags = 0;

	sigaction (SIGSEGV, &sigsegv_action, NULL);

	int dgemlqt_error_exit_tests_failed = 0;

	// Test error exit 1 (side is not one of 'L', 'l', 'T', 't')
	dgemlqt_error_exit_tests_failed |= test_dgemlqt_legal_param ('l', 'N', 0, 0, 1, 1, A, 1, T, 1, C, 1, work, 1);
	dgemlqt_error_exit_tests_failed |= test_dgemlqt_illegal_param ('/', 'N', 1, 1, 1, 1, A, 1, T, 1, C, 1, work, 1);

	// Test error exit 2 (trans is not one of 'N', 'n', 'T', 't')
	dgemlqt_error_exit_tests_failed |= test_dgemlqt_legal_param ('L', 'n', 0, 0, 1, 1, A, 1, T, 1, C, 1, work, 2);
	dgemlqt_error_exit_tests_failed |= test_dgemlqt_illegal_param ('L', '/', 1, 1, 1, 1, A, 1, T, 1, C, 1, work, 2);

	// Test error exit 3 (m < 0)
	dgemlqt_error_exit_tests_failed |= test_dgemlqt_illegal_param ('L', 'N', -1, 1, 1, 1, A, 1, T, 1, C, 1, work, 3);

	// Test error exit 4 (n < 0)
	dgemlqt_error_exit_tests_failed |= test_dgemlqt_illegal_param ('L', 'N', 1, -1, 1, 1, A, 1, T, 1, C, 1, work, 4);

	// Test error exit 5 (k < 0, k > m if side = 'L', k > n if side = 'R')
	dgemlqt_error_exit_tests_failed |= test_dgemlqt_illegal_param ('L', 'N', 1, 1, -1, 1, A, 1, T, 1, C, 1, work, 5);
	dgemlqt_error_exit_tests_failed |= test_dgemlqt_illegal_param ('L', 'N', 1, 2, 2, 1, A, 2, T, 2, C, 1, work, 5);
	dgemlqt_error_exit_tests_failed |= test_dgemlqt_illegal_param ('R', 'N', 2, 1, 2, 1, A, 2, T, 2, C, 2, work, 5);

	// Test error exit 6 (nb <= 0)
	dgemlqt_error_exit_tests_failed |= test_dgemlqt_illegal_param ('L', 'N', 1, 1, 1, 0, A, 1, T, 1, C, 1, work, 6);

	// Test error exit 8 (ldv < max(1, k))
	dgemlqt_error_exit_tests_failed |= test_dgemlqt_illegal_param ('L', 'N', 2, 2, 2, 1, A, 1, T, 2, C, 2, work, 8);

	// Test error exit 10 (ldt < max (1, nb))
	dgemlqt_error_exit_tests_failed |= test_dgemlqt_illegal_param ('L', 'N', 2, 2, 2, 2, A, 2, T, 1, C, 2, work, 10);

	// Test error exit 12 (ldc < max (1, m))
	dgemlqt_error_exit_tests_failed |= test_dgemlqt_illegal_param ('L', 'N', 2, 1, 1, 1, A, 2, T, 1, C, 1, work, 12);

	// Detach custom SIGSEGV handler
	sigsegv_action.sa_handler = SIG_DFL;
	sigaction (SIGSEGV, &sigsegv_action, NULL);

	return dgemlqt_error_exit_tests_failed;
}

int test_general_lq_kernels_error_exits () {
	int error_exit_tests_failed = test_dgelqt_error_exits () | test_dgemlqt_error_exits ();

	if (error_exit_tests_failed) {
		printf ("General LQ kernels failed the tests of the error exits\n");
	} else {
		printf ("General LQ kernels passed the tests of the error exits\n");
	}

	printf ("\n");

	return error_exit_tests_failed;
}

int general_lq_kernels_test (double thresh, int nm, int* mval, int nn, int* nval, int nnb, int* nbval, int test_error_exits) {
	int error_exits_result = 0;
	if (test_error_exits) {
		error_exits_result = test_general_lq_kernels_error_exits ();
	}

	int no_tests_run = 0, no_tests_failed = 0;

	for (int m_index = 0; m_index < nm; m_index++) {
		int m = mval[m_index];

		for (int n_index = 0; n_index < nn; n_index++) {
			int n = nval[n_index];

			for (int nb_index = 0; nb_index < nnb; nb_index++) {
				int nb = nbval[nb_index];

				if (nb == 0 || nb > min (m, n)) {
					continue;
				}

				double result[NO_TESTS];

				perform_general_lq_kernels_test (m, n, nb, result);

				for (int test = 0; test < NO_TESTS; test++) {
					if (result[test] >= thresh) {
						no_tests_failed++;

						printf ("General LQ kernels test number %d failed for M=%d, N=%d, NB=%d, test ratio is %lg\n", test + 1, m, n, nb, result[test]);
					}

					no_tests_run++;
				}
			}
		}
	}

	if (no_tests_failed == 0) {
		printf ("All tests for general LQ kernels passed the threshold (%d calls)\n", no_tests_run);
		return 0 | error_exits_result;
	} else {
		printf ("%d out of %d tests for general LQ kernels failed to pass the threshold\n", no_tests_failed, no_tests_run);
		return 1;
	}
}

int main () {
	double thresh = 30;
	int nm = 10;
	int mval[] = {0, 1, 2, 3, 5, 10, 20, 30, 50, 100};
	int nn = 10;
	int nval[] = {0, 1, 2, 3, 5, 10, 20, 30, 50, 100};
	int nnb = 6;
	int nbval[] = {1, 2, 3, 7, 12, 33};

	int test_error_exits = 1;

	return general_lq_kernels_test (thresh, nm, mval, nn, nval, nnb, nbval, test_error_exits);
}

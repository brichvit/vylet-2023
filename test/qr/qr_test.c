#define EPSILON __DBL_EPSILON__
#define NO_TESTS 6

#include "../../src/qr/qr.h"
#include "../../src/common/config.h"
#include "../../src/common/math_utils.h"

#include "../lapack/lapack.h"
#include "../lapack/cblas_f77.h"

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <setjmp.h>

#if defined (USE_STARPU)
#include <starpu.h>
#endif

#if defined (USE_MKL)
#include <mkl_service.h>
#elif defined (USE_ARMPL)
#include <armpl.h>
#endif

void perform_qr_routines_test (int m, int n, config_t config, double result[NO_TESTS]) {
	// Get relevant config options
	int nb = config.nb;
	int ib = config.ib;
	elim_scheme_e elim_scheme = config.elim_scheme;

	int min_mn = min (m, n), max_mn = max (m, n);
	int max_1m = max (1, m), max_1n = max (1, n);

	int ldt = max (1, ib);

	double zero = 0, one = 1, minus_one = -1;

	int seed[] = {1988, 1989, 1990, 1991};

	// Generate random m-by-n matrix A and its copy A_factored
	double* A = malloc (m * n * sizeof (double));
	double* A_factored = malloc (m * n * sizeof (double));
	for (int col = 0; col < n; col++) {
		int distribution = 2;
		LAPACK_dlarnv (&distribution, seed, &m, A + m * col);
	}
	LAPACK_dlacpy ("Full", &m, &n, A, &max_1m, A_factored, &max_1m);

	int no_blocks_y = (m - 1) / nb + 1;

	// Factor A_factored using PDGEQRF, save the upper triangular factor in T
	double* T = malloc (get_t_size_multiplier_for_elim_scheme (elim_scheme) * ib * no_blocks_y * min_mn * sizeof (double));
	elim_context_t context;
	pdgeqrf (m, n, A_factored, max_1m, T, ldt, &config, &context);

	// Generate the m-by-m matrix Q using PDGEMQR
	double* Q = malloc (m * m * sizeof (double));
	LAPACK_dlaset ("Full", &m, &m, &zero, &one, Q, &max_1m);
	pdgemqr ('R', 'N', m, m, min_mn, A_factored, max_1m, T, ldt, Q, max_1m, context);

	// Generate the m-by-max(m,n) matrix R by copying the upper triangular part of A_factored
	double* R = malloc (m * max_mn * sizeof (double)); // max_mn is used instead of n, because R is used as an m-by-m work matrix in test 2
	LAPACK_dlaset ("Lower", &m, &n, &zero, &zero, R, &max_1m);
	LAPACK_dlacpy ("Upper", &m, &n, A_factored, &max_1m, R, &max_1m);

	// Test 1: compute |R - Q'*A| / |A|
	F77_dgemm ("Transposed", "Non-transposed", &m, &n, &m, &minus_one, Q, &max_1m, A, &max_1m, &one, R, &max_1m);

	double* rwork = malloc (max_mn * sizeof (double));
	double A_norm = LAPACK_dlange ("1-norm", &m, &n, A, &max_1m, rwork);
	double resid_norm = LAPACK_dlange ("1-norm", &m, &n, R, &max_1m, rwork);

	if (A_norm > 0) {
		result[0] = resid_norm / (EPSILON * max_1m * A_norm);
	} else {
		result[0] = 0;
	}

	// Free A
	free (A);

	// Test 2: compute |I - Q'*Q|
	LAPACK_dlaset ("Upper", &m, &m, &zero, &one, R, &max_1m);
	F77_dsyrk ("Upper", "Transposed", &m, &m, &minus_one, Q, &max_1m, &one, R, &max_1m);
	resid_norm = LAPACK_dlansy ("1-norm", "Upper", &m, R, &max_1m, rwork);
	result[1] = resid_norm / (EPSILON * max_1m);

	// Free R
	free (R);

	// Generate random m-by-n matrix C and its copy C_householder
	double* C = malloc (m * n * sizeof (double));
	double* C_householder = malloc (m * n * sizeof (double));
	for (int col = 0; col < n; col++) {
		int distribution = 2;
		LAPACK_dlarnv (&distribution, seed, &m, C + m * col);
	}
	LAPACK_dlacpy ("Full", &m, &n, C, &max_1m, C_householder, &max_1m);

	// Calculate the norm of C
	double C_norm = LAPACK_dlange ("1-norm", &m, &n, C, &max_1m, rwork);

	// Apply Q to C_householder using PDGEMQR
	pdgemqr ('L', 'N', m, n, min_mn, A_factored, max_1m, T, ldt, C_householder, max_1m, context);

	// Test 3: calculate |Q*C_householder - Q*C| / |C|
	// - Q*C_householder is obtained using sequential application of Householder reflectors
	// - Q*C is obtained using standard matrix multiplication
	F77_dgemm ("Non-transposed", "Non-transposed", &m, &n, &m, &minus_one, Q, &max_1m, C, &max_1m, &one, C_householder, &max_1m);
	resid_norm = LAPACK_dlange ("1-norm", &m, &n, C_householder, &max_1m, rwork);

	if (C_norm > 0) {
		result[2] = resid_norm / (EPSILON * max_1m * C_norm);
	} else {
		result[2] = 0;
	}

	// Copy C into C_householder again
	LAPACK_dlacpy ("Full", &m, &n, C, &max_1m, C_householder, &max_1m);

	// Apply Q^T to C_householder using PDGEMQR
	pdgemqr ('L', 'T', m, n, min_mn, A_factored, max_1m, T, ldt, C_householder, max_1m, context);

	// Test 4: calculate |Q^T*C_householder - Q^T*C| / |C|
	// - Q^T*C_householder is obtained using sequential application of Householder reflectors
	// - Q^T*C is obtained using standard matrix multiplication
	F77_dgemm ("Transposed", "Non-transposed", &m, &n, &m, &minus_one, Q, &max_1m, C, &max_1m, &one, C_householder, &max_1m);
	resid_norm = LAPACK_dlange ("1-norm", &m, &n, C_householder, &max_1m, rwork);

	if (C_norm > 0) {
		result[3] = resid_norm / (EPSILON * max_1m * C_norm);
	} else {
		result[3] = 0;
	}

	// Free C and C_householder
	free (C);
	free (C_householder);

	// Generate random n-by-m matrix D and its copy D_householder
	double* D = malloc (n * m * sizeof (double));
	double* D_householder = malloc (n * m * sizeof (double));
	for (int col = 0; col < m; col++) {
		int distribution = 2;
		LAPACK_dlarnv (&distribution, seed, &n, D + n * col);
	}
	LAPACK_dlacpy ("Full", &n, &m, D, &max_1n, D_householder, &max_1n);

	// Calculate the norm of D
	double D_norm = LAPACK_dlange ("1-norm", &n, &m, D, &max_1n, rwork);

	// Apply Q to D_householder using PDGEMQR
	pdgemqr ('R', 'N', n, m, min_mn, A_factored, max_1m, T, ldt, D_householder, max_1n, context);

	// Test 5: compute |D_householder*Q - D*Q| / |D|
	// - D_householder*Q is obtained using sequential application of Householder reflectors
	// - D*Q is obtained using standard matrix multiplication
	F77_dgemm ("Non-transposed", "Non-transposed", &n, &m, &m, &minus_one, D, &max_1n, Q, &max_1m, &one, D_householder, &max_1n);
	resid_norm = LAPACK_dlange ("1-norm", &n, &m, D_householder, &max_1n, rwork);

	if (D_norm > 0) {
		result[4] = resid_norm / (EPSILON * max_1m * D_norm);
	} else {
		result[4] = 0;
	}

	// Copy D into D_householder again
	LAPACK_dlacpy ("Full", &n, &m, D, &max_1n, D_householder, &max_1n);

	// Apply Q^T to D_householder using PDGEMQR
	pdgemqr ('R', 'T', n, m, min_mn, A_factored, max_1m, T, ldt, D_householder, max_1n, context);

	// Test 6: compute |D_householder*Q^T - D*Q^T| / |D|
	// - D_householder*Q^T is obtained using sequential application of Householder reflectors
	// - D*Q^T is obtained using standard matrix multiplication
	F77_dgemm ("Non-transposed", "Transposed", &n, &m, &m, &minus_one, D, &max_1n, Q, &max_1m, &one, D_householder, &max_1n);
	resid_norm = LAPACK_dlange ("1-norm", &n, &m, D_householder, &max_1n, rwork);

	if (D_norm > 0) {
		result[5] = resid_norm / (EPSILON * max_1m * D_norm);
	} else {
		result[5] = 0;
	}

	// Free remaining matrices
	free (A_factored);
	free (T);
	free (Q);

	free (D);
	free (D_householder);
	free (rwork);
}

volatile sig_atomic_t sigsegv_flag = 0;
sigjmp_buf mark;

void sigsegv_handler (int sig) {
	// Do not mark sig as unused
	(void)sig;

	sigsegv_flag = 1;
	siglongjmp (mark, 1);
}

int check_sigsegv_flag (char* routine_name, int param_index) {
	if (sigsegv_flag == 1) {
		fprintf (stderr, "Illegal memory access detected when parameter %d of %s has illegal value\n", param_index, routine_name);
		sigsegv_flag = 0;

		return 1;
	}

	return 0;
}

int test_pdgeqrf_illegal_param (int m, int n, double* A, int lda, double* T, int ldt, config_t config, int illegal_param_index) {
	int failed = 0;

	if (sigsetjmp (mark, 1) == 0) {
		if (pdgeqrf (m, n, A, lda, T, ldt, &config, NULL) != illegal_param_index) {
			fprintf (stderr, "Illegal value of parameter number %d not detected by PDGEQRF\n", illegal_param_index);
			failed = 1;
		}
	}
	failed |= check_sigsegv_flag ("PDGEQRF", illegal_param_index);

	return failed;
}

int test_pdgemqr_illegal_param (char side, char trans, int m, int n, int k, double* A, int lda, double* T, int ldt, double* C, int ldc, elim_context_t context, int illegal_param_index) {
	int failed = 0;

	if (sigsetjmp (mark, 1) == 0) {
		if (pdgemqr (side, trans, m, n, k, A, lda, T, ldt, C, ldc, context) != illegal_param_index) {
			fprintf (stderr, "Illegal value of parameter number %d not detected by PDGEMQR\n", illegal_param_index);
			failed = 1;
		}
	}
	failed |= check_sigsegv_flag ("PDGEMQR", illegal_param_index);

	return failed;
}

int test_pdgemqr_legal_param (char side, char trans, int m, int n, int k, double* A, int lda, double* T, int ldt, double* C, int ldc, elim_context_t context, int checked_param_index) {
	if (pdgemqr (side, trans, m, n, k, A, lda, T, ldt, C, ldc, context) == checked_param_index) {
		fprintf (stderr, "Illegal value of parameter number %d falsely detected by PDGEMQR\n", checked_param_index);
		return 1;
	}

	return 0;
}

int test_pdgeqrf_error_exits () {
	// Set used matrices & the QR context to NULL in order to ensure that they are not accessed
	double* A = NULL;
	double* T = NULL;

	// Attach custom signal handler to SIGSEGV in order to catch illegal memory accesses
	struct sigaction sigsegv_action;
	sigsegv_action.sa_handler = sigsegv_handler;
	sigfillset (&sigsegv_action.sa_mask);
	sigsegv_action.sa_flags = 0;

	sigaction (SIGSEGV, &sigsegv_action, NULL);

	int pdgeqrf_error_exit_tests_failed = 0;

	// Create and initialize config
	config_t config;
	config = (config_t) {
		.nb = 1,
		.ib = 1,
		.superblock_size_factor = 1,
		.elim_scheme = TsFlatTree,
		.layout_translation = 0
	};

	// Test error exit 1 (m < 0)
	pdgeqrf_error_exit_tests_failed |= test_pdgeqrf_illegal_param (-1, 1, A, 1, T, 1, config, 1);

	// Test error exit 2 (n < 0)
	pdgeqrf_error_exit_tests_failed |= test_pdgeqrf_illegal_param (1, -1, A, 1, T, 1, config, 2);

	// Test error exit 4 (lda < max (1, m))
	pdgeqrf_error_exit_tests_failed |= test_pdgeqrf_illegal_param (2, 1, A, 1, T, 1, config, 4);

	// Test error exit 6 (ldt < max (1, ib))
	config.nb = config.ib = 2;
	pdgeqrf_error_exit_tests_failed |= test_pdgeqrf_illegal_param (2, 2, A, 2, T, 1, config, 6);
	config.nb = config.ib = 1;

	// Test error exit 7 (nb <= 0)
	config.nb = 0;
	pdgeqrf_error_exit_tests_failed |= test_pdgeqrf_illegal_param (1, 1, A, 1, T, 1, config, 7);
	config.nb = 1;

	// Test error exit 7 (ib <= 0, ib > nb)
	config.ib = 0;
	pdgeqrf_error_exit_tests_failed |= test_pdgeqrf_illegal_param (1, 1, A, 1, T, 1, config, 7);
	config.ib = 2;
	pdgeqrf_error_exit_tests_failed |= test_pdgeqrf_illegal_param (1, 1, A, 1, T, 2, config, 7);
	config.ib = 1;

	// Test error exit 7 (superblock_size_factor <= 0)
	config.superblock_size_factor = 0;
	pdgeqrf_error_exit_tests_failed |= test_pdgeqrf_illegal_param (1, 1, A, 1, T, 1, config, 7);
	config.superblock_size_factor = 1;

	// Test error exit 7 (elim_scheme < 0, elim_scheme >= NO_ELIM_SCHEMES)
	config.elim_scheme = -1;
	pdgeqrf_error_exit_tests_failed |= test_pdgeqrf_illegal_param (1, 1, A, 1, T, 1, config, 7);
	config.elim_scheme = NO_ELIM_SCHEMES;
	pdgeqrf_error_exit_tests_failed |= test_pdgeqrf_illegal_param (1, 1, A, 1, T, 1, config, 7);
	config.elim_scheme = TsFlatTree;

	// Test error exit 7 (layout_translation < 0, layout_translation > 1)
	config.layout_translation = -1;
	pdgeqrf_error_exit_tests_failed |= test_pdgeqrf_illegal_param (1, 1, A, 1, T, 1, config, 7);
	config.layout_translation = 2;
	pdgeqrf_error_exit_tests_failed |= test_pdgeqrf_illegal_param (1, 1, A, 1, T, 1, config, 7);
	config.layout_translation = 0;

	// Detach custom SIGSEGV handler
	sigsegv_action.sa_handler = SIG_DFL;
	sigaction (SIGSEGV, &sigsegv_action, NULL);

	return pdgeqrf_error_exit_tests_failed;
}

int test_pdgemqr_error_exits () {
	// Set used matrices to NULL in order to ensure that they are not accessed
	double* A = NULL;
	double* T = NULL;
	double* C = NULL;

	// Attach custom signal handler to SIGSEGV in order to catch illegal memory accesses
	struct sigaction sigsegv_action;
	sigsegv_action.sa_handler = sigsegv_handler;
	sigfillset (&sigsegv_action.sa_mask);
	sigsegv_action.sa_flags = 0;

	sigaction (SIGSEGV, &sigsegv_action, NULL);

	int pdgemqr_error_exit_tests_failed = 0;

	// Create and initialize context
	elim_context_t context = {
		.config = {
			.nb = 1,
			.ib = 1,
			.superblock_size_factor = 1,
			.elim_scheme = TsFlatTree,
			.layout_translation = 0
		},
		.a_handles_height = 1,
		.a_handles_width = 1,
		.num_threads = 1
	};

	// Test error exit 1 (side is not one of 'L', 'l', 'T', 't')
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_legal_param ('l', 'N', 0, 0, 1, A, 1, T, 1, C, 1, context, 1);
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('/', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 1);

	// Test error exit 2 (trans is not one of 'N', 'n', 'T', 't')
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_legal_param ('L', 'n', 0, 0, 1,  A, 1, T, 1, C, 1, context, 2);
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('L', '/', 1, 1, 1, A, 1, T, 1, C, 1, context, 2);

	// Test error exit 3 (m < 0)
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('L', 'N', -1, 1, 1, A, 1, T, 1, C, 1, context, 3);

	// Test error exit 4 (n < 0)
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('L', 'N', 1, -1, 1, A, 1, T, 1, C, 1, context, 4);

	// Test error exit 5 (k < 0, k > m if side = 'L', k > n if side = 'R')
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('L', 'N', 1, 1, -1, A, 1, T, 1, C, 1, context, 5);
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('L', 'N', 1, 2, 2, A, 2, T, 2, C, 1, context, 5);
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('R', 'N', 2, 1, 2, A, 2, T, 2, C, 2, context, 5);

	// Test error exit 7 (lda < max(1, m) if side = 'L', lda < max (1, n) if side = 'R')
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('L', 'N', 2, 1, 1, A, 1, T, 2, C, 2, context, 7);
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('R', 'N', 1, 2, 1, A, 1, T, 2, C, 1, context, 7);

	// Test error exit 9 (ldt < max (1, ib))
	context.config.nb = context.config.ib = 2;
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('L', 'N', 2, 2, 2, A, 2, T, 1, C, 2, context, 9);
	context.config.nb = context.config.ib = 1;

	// Test error exit 11 (ldc < max (1, m))
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('L', 'N', 2, 1, 1, A, 2, T, 1, C, 1, context, 11);

	// Test error exit 12 (nb <= 0)
	context.config.nb = 0;
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.config.nb = 1;

	// Test error exit 12 (ib <= 0, ib > nb)
	context.config.ib = 0;
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.config.ib = 2;
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 2, C, 1, context, 12);
	context.config.ib = 1;

	// Test error exit 12 (superblock_size_factor <= 0)
	context.config.superblock_size_factor = 0;
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.config.superblock_size_factor = 1;

	// Test error exit 12 (elim_scheme < 0, elim_scheme >= NO_ELIM_SCHEMES)
	context.config.elim_scheme = -1;
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.config.elim_scheme = NO_ELIM_SCHEMES;
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.config.elim_scheme = TsFlatTree;

	// Test error exit 12 (layout_translation < 0, layout_translation > 1)
	context.config.layout_translation = -1;
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.config.layout_translation = 2;
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.config.layout_translation = 0;

	// Prepare for error exit 12 tests which depend on the elim_scheme being superblock-based
	context.config.elim_scheme = SuperblockGreedy;

	// Test error exit 12 (is_elim_scheme_superblock_based (elim_scheme) and a_handles_width < 0)
	context.a_handles_height = -1;
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.a_handles_height = 1;
	
	// Test error exit 12 (is_elim_scheme_superblock_based (elim_scheme) and a_handles_width < 0)
	context.a_handles_width = -1;
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.a_handles_width = 1;

	// Test error exit 12 (is_elim_scheme_superblock_based (elim_scheme) and num_threads <= 0)
	context.num_threads = 0;
	pdgemqr_error_exit_tests_failed |= test_pdgemqr_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.num_threads = 1;

	// Detach custom SIGSEGV handler
	sigsegv_action.sa_handler = SIG_DFL;
	sigaction (SIGSEGV, &sigsegv_action, NULL);

	return pdgemqr_error_exit_tests_failed;
}

int test_qr_routine_error_exits () {
	int error_exit_tests_failed = test_pdgeqrf_error_exits () | test_pdgemqr_error_exits ();

	if (error_exit_tests_failed) {
		fprintf (stderr, "QR routines failed the tests of the error exits\n");
	} else {
		printf ("QR routines passed the tests of the error exits\n");
	}

	printf ("\n");

	return error_exit_tests_failed;
}

int qr_routines_test (double thresh, int no_m_values, int* m_values, int no_n_values, int* n_values, int no_nb_values, int* no_values, int no_elim_schemes, elim_scheme_e* elim_schemes, int test_layout_translation, int test_error_exits) {
	#if defined (USE_MKL)
	mkl_set_num_threads (1);
	#elif defined (USE_ARMPL)
	armpl_set_num_threads (1);
	#endif

	#if defined (USE_STARPU)
	if (starpu_init (NULL) != 0) {
		fprintf (stderr, "Error: StarPU could not be initialized\n");
		exit (1);
	}
	#endif

	int error_exits_result = 0;
	if (test_error_exits) {
		error_exits_result = test_qr_routine_error_exits ();
	}

	int no_tests_run = 0, no_tests_failed = 0;

	for (int m_index = 0; m_index < no_m_values; m_index++) {
		int m = m_values[m_index];

		for (int n_index = 0; n_index < no_n_values; n_index++) {
			int n = n_values[n_index];

			for (int nb_index = 0; nb_index < no_nb_values; nb_index++) {
				int nb = no_values[nb_index];

				if (nb == 0 || nb > min (m, n)) {
					continue;
				}

				for (int elim_scheme_index = 0; elim_scheme_index < no_elim_schemes; elim_scheme_index++) {
					int elim_scheme = elim_schemes[elim_scheme_index];

					for (int layout_translation = 0; layout_translation <= !!test_layout_translation; layout_translation++) {
						double result[NO_TESTS];

						config_t config = {
							.nb = nb,
							.ib = max (1, nb / 4), //TODO
							.superblock_size_factor = 4, //TODO
							.elim_scheme = elim_scheme,
							.layout_translation = layout_translation
						};

						perform_qr_routines_test (m, n, config, result);

						for (int test = 0; test < NO_TESTS; test++) {
							if (result[test] >= thresh) {
								no_tests_failed++;

								fprintf (stderr,
										"QR routines test number %d failed for M=%d, N=%d, NB=%d, IB=%d, gamma=%d, elimination scheme %s, layout translation %s, test ratio is %lg\n",
										test + 1,
										m,
										n,
										config.nb,
										config.ib,
										config.superblock_size_factor,
										get_elim_scheme_name (config.elim_scheme),
										config.layout_translation ? "on" : "off",
										result[test]
								);
							}

							no_tests_run++;
						}
					}
				}
			}
		}
	}

	#if defined (USE_STARPU)
	starpu_shutdown ();
	#endif

	if (no_tests_failed == 0) {
		printf ("All tests for QR routines passed the threshold (%d calls)\n", no_tests_run);
		return 0 | error_exits_result;
	} else {
		fprintf (stderr, "%d out of %d tests for QR routines failed to pass the threshold\n", no_tests_failed, no_tests_run);
		return 1;
	}
}

int main () {
	double thresh = 30;
	int no_m_values = 8;
	int m_values[] = {0, 1, 2, 5, 10, 20, 50, 100};
	int no_n_values = 8;
	int n_values[] = {0, 1, 2, 5, 10, 20, 50, 100};
	int no_nb_values = 6;
	int nb_values[] = {1, 2, 3, 7, 12, 33};
	int no_elim_schemes = NO_ELIM_SCHEMES;
	elim_scheme_e elim_schemes[] = {TsFlatTree, TtFlatTree, TtGreedy, TtBinaryTree, TtFibonacci, SuperblockGreedy, SuperblockBinaryTree, SuperblockFibonacci, AutoTree};

	int test_layout_translation = 1;
	int test_error_exits = 1;

	return qr_routines_test (
		thresh,
		no_m_values,
		m_values,
		no_n_values,
		n_values,
		no_nb_values,
		nb_values,
		no_elim_schemes,
		elim_schemes,
		test_layout_translation,
		test_error_exits
	);
}

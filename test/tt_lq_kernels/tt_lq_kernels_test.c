#define EPSILON __DBL_EPSILON__
#define NO_TESTS 6

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <setjmp.h>

#include "../lapack/lapack.h"
#include "../lapack/cblas_f77.h"

#include "../../src/common/math_utils.h"
#include "../../src/dttlqt/dttlqt.h"
#include "../../src/dttmlq/dttmlq.h"

void perform_tt_lq_kernels_test (int m, int n, int nb, double result[NO_TESTS]) {
	int max_mn = max (m, n);
	int max_1m = max (1, m);

	int mpn = m + n, max_1mpn = max (1, mpn);

	int ldt = max (1, nb);

	double zero = 0, one = 1, minus_one = -1;

	int seed[] = {1988, 1989, 1990, 1991};

	// Generate random m-by-m matrix A1_factored
	double* A1_factored = malloc (m * m * sizeof (double));
	for (int col = 0; col < m; col++) {
		int distribution = 2;
		LAPACK_dlarnv (&distribution, seed, &m, A1_factored + m * col);
	}

	// Generate random m-by-n matrix A2 A2_factored
	double* A2_factored = malloc (m * n * sizeof (double));
	for (int col = 0; col < n; col++) {
		int distribution = 2;
		LAPACK_dlarnv (&distribution, seed, &m, A2_factored + m * col);
	}

	// Create m-by-(m+n) matrix A by coupling together A1_factored and A2_factored (where A1_factored goes to the left of A2_factored)
	// and setting all entries in both A1_factored and A2_factored above the main diagonal to zero
	double* A = malloc (m * mpn * sizeof (double));
	LAPACK_dlaset ("Upper", &m, &m, &zero, &zero, A, &max_1m);
	LAPACK_dlacpy ("Lower", &m, &m, A1_factored, &max_1m, A, &max_1m);
	LAPACK_dlaset ("Upper", &m, &n, &zero, &zero, A + m * m, &max_1m);
	LAPACK_dlacpy ("Lower", &m, &n, A2_factored, &max_1m, A + m * m, &max_1m);

	// Factor A1_factored and A2_factored using DTTLQT, save the upper triangular factor in T
	double* T = malloc (nb * m * sizeof (double));
	double* work = malloc (max (2, max_mn) * max (2, max_mn) * nb * sizeof (double));
	dttlqt (m, n, nb, A1_factored, max_1m, A2_factored, max_1m, T, ldt, work);

	// Generate the (m+n)-by-(m+n) matrix Q using DTTMLQ
	double* Q = malloc (mpn * mpn * sizeof (double));
	LAPACK_dlaset ("Full", &mpn, &mpn, &zero, &one, Q, &max_1mpn);
	dttmlq ('R', 'N', mpn, m, mpn, n, m, nb, Q, max_1mpn, Q + m * mpn, max_1mpn, A2_factored, max_1m, T, ldt, work);

	// Generate the (m+n)-by-(m+n) matrix L by copying the upper triangular part of A1_factored
	double* L = malloc (mpn * mpn * sizeof (double)); // m+n is used instead of m, because L is used as an (m+n)-by-(m+n) work matrix in test 2
	LAPACK_dlaset ("Upper", &m, &mpn, &zero, &zero, L, &max_1mpn);
	LAPACK_dlacpy ("Lower", &m, &m, A1_factored, &max_1m, L, &max_1mpn);

	// Test 1: compute |L - A*Q'| / |A|
	F77_dgemm ("Non-transposed", "Transposed", &m, &mpn, &mpn, &minus_one, A, &max_1m, Q, &max_1mpn, &one, L, &max_1mpn);

	double* rwork = malloc (mpn * sizeof (double));
	double A_norm = LAPACK_dlange ("1-norm", &m, &mpn, A, &max_1m, rwork);
	double resid_norm = LAPACK_dlange ("1-norm", &m, &mpn, L, &max_1mpn, rwork);

	if (A_norm > 0) {
		result[0] = resid_norm / (EPSILON * max_1mpn * A_norm);
	} else {
		result[0] = 0;
	}

	// Free A
	free (A);

	// Test 2: compute |I - Q'*Q|
	LAPACK_dlaset ("Upper", &mpn, &mpn, &zero, &one, L, &max_1mpn);
	F77_dsyrk ("Upper", "Transposed", &mpn, &mpn, &minus_one, Q, &max_1mpn, &one, L, &max_1mpn);
	resid_norm = LAPACK_dlansy ("1-norm", "Upper", &mpn, L, &max_1mpn, rwork);
	result[1] = resid_norm / (EPSILON * max_1mpn);

	// Free L
	free (L);

	// Generate random (m+n)-by-m matrix D and its copy D_householder
	double* D = malloc (mpn * m * sizeof (double));
	double* D_householder = malloc (mpn * m * sizeof (double));
	for (int col = 0; col < m; col++) {
		int distribution = 2;
		LAPACK_dlarnv (&distribution, seed, &mpn, D + mpn * col);
	}
	LAPACK_dlacpy ("Full", &mpn, &m, D, &max_1mpn, D_householder, &max_1mpn);
	
	// Calculate the norm of D
	double D_norm = LAPACK_dlange ("1-norm", &mpn, &m, D, &max_1mpn, rwork);

	// Apply Q to D_householder using DTTMLQ
	dttmlq ('L', 'N', m, m, n, m, m, nb, D_householder, max_1mpn, D_householder + m, max_1mpn, A2_factored, max_1m, T, ldt, work);

	// Test 3: calculate |Q*D_householder - Q*D| / |D|
	// - Q*D_householder is obtained using sequential application of Householder reflectors
	// - Q*D is obtained using standard matrix multiplication
	F77_dgemm ("Non-transposed", "Non-transposed", &mpn, &m, &mpn, &minus_one, Q, &max_1mpn, D, &max_1mpn, &one, D_householder, &max_1mpn);
	resid_norm = LAPACK_dlange ("1-norm", &mpn, &m, D_householder, &max_1mpn, rwork);

	if (D_norm > 0) {
		result[2] = resid_norm / (EPSILON * max_1mpn * D_norm);
	} else {
		result[2] = 0;
	}

	// Copy D into D_householder again
	LAPACK_dlacpy ("Full", &mpn, &m, D, &max_1mpn, D_householder, &max_1mpn);

	// Apply Q^T to D_householder using DGEMLQT
	dttmlq ('L', 'T', m, m, n, m, m, nb, D_householder, max_1mpn, D_householder + m, max_1mpn, A2_factored, max_1m, T, ldt, work);

	// Test 4: calculate |Q^T*D_householder - Q^T*D| / |D|
	// - Q^T*D_householder is obtained using sequential application of Householder reflectors
	// - Q^T*D is obtained using standard matrix multiplication
	F77_dgemm ("Transposed", "Non-transposed", &mpn, &m, &mpn, &minus_one, Q, &max_1mpn, D, &max_1mpn, &one, D_householder, &max_1mpn);
	resid_norm = LAPACK_dlange ("1-norm", &mpn, &m, D_householder, &max_1mpn, rwork);

	if (D_norm > 0) {
		result[3] = resid_norm / (EPSILON * max_1mpn * D_norm);
	} else {
		result[3] = 0;
	}

	// Free D and D_householder
	free (D);
	free (D_householder);

	// Generate random m-by-(m+n) matrix C and its copy C_householder
	double* C = malloc (m * mpn * sizeof (double));
	double* C_householder = malloc (m * mpn * sizeof (double));
	for (int col = 0; col < mpn; col++) {
		int distribution = 2;
		LAPACK_dlarnv (&distribution, seed, &m, C + m * col);
	}
	LAPACK_dlacpy ("Full", &m, &mpn, C, &max_1m, C_householder, &max_1m);

	// Calculate the norm of C
	double C_norm = LAPACK_dlange ("1-norm", &m, &mpn, C, &max_1m, rwork);

	// Apply Q to C_householder using DGEMLQT
	dttmlq ('R', 'N', m, m, m, n, m, nb, C_householder, max_1m, C_householder + m * m, max_1m, A2_factored, max_1m, T, ldt, work);

	// Test 5: compute |C_householder*Q - C*Q| / |C|
	// - C_householder*Q is obtained using sequential application of Householder reflectors
	// - C*Q is obtained using standard matrix multiplication
	F77_dgemm ("Non-transposed", "Non-transposed", &m, &mpn, &mpn, &minus_one, C, &max_1m, Q, &max_1mpn, &one, C_householder, &max_1m);
	resid_norm = LAPACK_dlange ("1-norm", &m, &mpn, C_householder, &max_1m, rwork);

	if (C_norm > 0) {
		result[4] = resid_norm / (EPSILON * max_1mpn * C_norm);
	} else {
		result[4] = 0;
	}

	// Copy C into C_householder again
	LAPACK_dlacpy ("Full", &m, &mpn, C, &max_1m, C_householder, &max_1m);

	// Apply Q^T to C_householder using DGEMLQT
	dttmlq ('R', 'T', m, m, m, n, m, nb, C_householder, max_1m, C_householder + m * m, max_1m, A2_factored, max_1m, T, ldt, work);

	// Test 6: compute |C_householder*Q^T - C*Q^T| / |C|
	// - C_householder*Q^T is obtained using sequential application of Householder reflectors
	// - C*Q^T is obtained using standard matrix multiplication
	F77_dgemm ("Non-transposed", "Transposed", &m, &mpn, &mpn, &minus_one, C, &max_1m, Q, &max_1mpn, &one, C_householder, &max_1m);
	resid_norm = LAPACK_dlange ("1-norm", &m, &mpn, C_householder, &max_1m, rwork);

	if (C_norm > 0) {
		result[5] = resid_norm / (EPSILON * max_1mpn * C_norm);
	} else {
		result[5] = 0;
	}

	// Free remaining matrices
	free (A1_factored);
	free (A2_factored);
	free (T);
	free (Q);
	free (work);

	free (C);
	free (C_householder);
	free (rwork);
}

volatile sig_atomic_t sigsegv_flag = 0;
sigjmp_buf mark;

void sigsegv_handler (int sig) {
	// Do not mark sig as unused
	(void)sig;

	sigsegv_flag = 1;
	siglongjmp (mark, 1);
}

int check_sigsegv_flag (char* routine_name, int param_index) {
	if (sigsegv_flag == 1) {
		fprintf (stderr, "Illegal memory access detected when parameter %d of %s has illegal value\n", param_index, routine_name);
		sigsegv_flag = 0;

		return 1;
	}

	return 0;
}

int test_dttlqt_illegal_param (int m, int n, int nb, double* A1, int lda1, double* A2, int lda2, double* T, int ldt, double* work, int illegal_param_index) {
	int failed = 0;

	if (sigsetjmp (mark, 1) == 0) {
		if (dttlqt (m, n, nb, A1, lda1, A2, lda2, T, ldt, work) != illegal_param_index) {
			fprintf (stderr, "Illegal value of parameter number %d not detected by DTTLQT\n", illegal_param_index);
			failed = 1;
		}
	}
	failed |= check_sigsegv_flag ("DTTLQT", illegal_param_index);

	return failed;
}

int test_dttmlq_illegal_param (char side, char trans, int m1, int n1, int m2, int n2, int k, int nb, double* A1, int lda1, double* A2, int lda2, double* V, int ldv, double* T, int ldt, double* work, int illegal_param_index) {
	int failed = 0;

	if (sigsetjmp (mark, 1) == 0) {
		if (dttmlq (side, trans, m1, n1, m2, n2, k, nb, A1, lda1, A2, lda2, V, ldv, T, ldt, work) != illegal_param_index) {
			fprintf (stderr, "Illegal value of parameter number %d not detected by DTTMLQ\n", illegal_param_index);
			failed = 1;
		}
	}
	failed |= check_sigsegv_flag ("DTTMLQ", illegal_param_index);

	return failed;
}

int test_dttmlq_legal_param (char side, char trans, int m1, int n1, int m2, int n2, int k, int nb, double* A1, int lda1, double* A2, int lda2, double* V, int ldv, double* T, int ldt, double* work, int checked_param_index) {
	if (dttmlq (side, trans, m1, n1, m2, n2, k, nb, A1, lda1, A2, lda2, V, ldv, T, ldt, work) == checked_param_index) {
		fprintf (stderr, "Illegal value of parameter number %d falsely detected by DTTMLQ\n", checked_param_index);
		return 1;
	}

	return 0;
}

int test_dttlqt_error_exits () {
	// Set used matrices to NULL in order to ensure that they are not accessed
	double* A1 = NULL;
	double* A2 = NULL;
	double* T = NULL;
	double* work = NULL;

	// Attach custom signal handler to SIGSEGV in order to catch illegal memory accesses
	struct sigaction sigsegv_action;
	sigsegv_action.sa_handler = sigsegv_handler;
	sigfillset (&sigsegv_action.sa_mask);
	sigsegv_action.sa_flags = 0;

	sigaction (SIGSEGV, &sigsegv_action, NULL);

	int dttlqt_error_exit_tests_failed = 0;

	// Test error exit 1 (m < 0)
	dttlqt_error_exit_tests_failed |= test_dttlqt_illegal_param (-1, 0, 0, A1, 1, A2, 1, T, 1, work, 1);

	// Test error exit 2 (n < 0)
	dttlqt_error_exit_tests_failed |= test_dttlqt_illegal_param (0, -1, 0, A1, 1, A2, 1, T, 1, work, 2);

	// Test error exit 3 (nb <= 0)
	dttlqt_error_exit_tests_failed |= test_dttlqt_illegal_param (1, 1, 0, A1, 1, A2, 1, T, 1, work, 3);

	// Test error exit 5 (lda1 < max (1, 2))
	dttlqt_error_exit_tests_failed |= test_dttlqt_illegal_param (2, 1, 1, A1, 1, A2, 2, T, 1, work, 5);

	// Test error exit 7 (lda2 < max (1, m))
	dttlqt_error_exit_tests_failed |= test_dttlqt_illegal_param (2, 1, 1, A1, 2, A2, 1, T, 1, work, 7);

	// Test error exit 9 (ldt < max (1, nb))
	dttlqt_error_exit_tests_failed |= test_dttlqt_illegal_param (2, 2, 2, A1, 2, A2, 2, T, 1, work, 9);

	// Detach custom SIGSEGV handler
	sigsegv_action.sa_handler = SIG_DFL;
	sigaction (SIGSEGV, &sigsegv_action, NULL);

	return dttlqt_error_exit_tests_failed;
}

int test_dttmlq_error_exits () {
	// Set used matrices to NULL in order to ensure that they are not accessed
	double* V = NULL;
	double* A1 = NULL;
	double* A2 = NULL;
	double* T = NULL;
	double* work = NULL;

	// Attach custom signal handler to SIGSEGV in order to catch illegal memory accesses
	struct sigaction sigsegv_action;
	sigsegv_action.sa_handler = sigsegv_handler;
	sigfillset (&sigsegv_action.sa_mask);
	sigsegv_action.sa_flags = 0;

	sigaction (SIGSEGV, &sigsegv_action, NULL);

	int dttmlq_error_exit_tests_failed = 0;

	// Test error exit 1 (side is not one of 'L', 'l', 'T', 't')
	dttmlq_error_exit_tests_failed |= test_dttmlq_legal_param ('l', 'N', 0, 0, 0, 0, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 1);
	dttmlq_error_exit_tests_failed |= test_dttmlq_illegal_param ('/', 'N', 1, 1, 1, 1, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 1);

	// Test error exit 2 (trans is not one of 'N', 'n', 'T', 't')
	dttmlq_error_exit_tests_failed |= test_dttmlq_legal_param ('L', 'n', 0, 0, 0, 0, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 2);
	dttmlq_error_exit_tests_failed |= test_dttmlq_illegal_param ('L', '/', 1, 1, 1, 1, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 2);

	// Test error exit 3 (m1 < 0)
	dttmlq_error_exit_tests_failed |= test_dttmlq_illegal_param ('L', 'N', -1, 1, 1, 1, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 3);

	// Test error exit 4 (n1 < 0)
	dttmlq_error_exit_tests_failed |= test_dttmlq_illegal_param ('L', 'N', 1, -1, 1, 1, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 4);

	// Test error exit 5 (m2 < 0, m1 != m2 if side = 'R')
	dttmlq_error_exit_tests_failed |= test_dttmlq_illegal_param ('L', 'N', 1, 1, -1, 1, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 5);
	dttmlq_error_exit_tests_failed |= test_dttmlq_illegal_param ('R', 'N', 1, 1, 2, 1, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 5);

	// Test error exit 6 (n2 < 0, n1 != n2 if side = 'L')
	dttmlq_error_exit_tests_failed |= test_dttmlq_illegal_param ('L', 'N', 1, 1, 1, -1, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 6);
	dttmlq_error_exit_tests_failed |= test_dttmlq_illegal_param ('L', 'N', 1, 1, 1, 2, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 6);

	// Test error exit 7 (k < 0, k > m1 if side = 'L', k > n1 if side = 'R')
	dttmlq_error_exit_tests_failed |= test_dttmlq_illegal_param ('L', 'N', 1, 1, 1, 1, -1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 7);
	dttmlq_error_exit_tests_failed |= test_dttmlq_illegal_param ('L', 'N', 1, 2, 1, 2, 2, 1, A1, 1, A2, 1, V, 1, T, 1, work, 7);
	dttmlq_error_exit_tests_failed |= test_dttmlq_illegal_param ('R', 'N', 2, 1, 2, 1, 2, 1, A1, 2, A2, 2, V, 1, T, 1, work, 7);

	// Test error exit 8 (nb <= 0)
	dttmlq_error_exit_tests_failed |= test_dttmlq_illegal_param ('L', 'N', 1, 1, 1, 1, 1, 0, A1, 1, A2, 1, V, 1, T, 1, work, 8);

	// Test error exit 10 (lda1 < max (1, m1))
	dttmlq_error_exit_tests_failed |= test_dttmlq_illegal_param ('L', 'N', 2, 1, 1, 1, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 10);

	// Test error exit 12 (lda1 < max (1, m1))
	dttmlq_error_exit_tests_failed |= test_dttmlq_illegal_param ('L', 'N', 1, 1, 2, 1, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 12);

	// Test error exit 14 (ldv < max (1, k))
	dttmlq_error_exit_tests_failed |= test_dttmlq_illegal_param ('L', 'N', 2, 1, 2, 1, 2, 1, A1, 2, A2, 2, V, 1, T, 1, work, 14);

	// Test error exit 16 (ldt < max (1, nb))
	dttmlq_error_exit_tests_failed |= test_dttmlq_illegal_param ('L', 'N', 2, 2, 2, 2, 2, 2, A1, 2, A2, 2, V, 2, T, 1, work, 16);

	// Detach custom SIGSEGV handler
	sigsegv_action.sa_handler = SIG_DFL;
	sigaction (SIGSEGV, &sigsegv_action, NULL);

	return dttmlq_error_exit_tests_failed;
}

int test_tt_lq_kernels_error_exits () {
	int error_exit_tests_failed = test_dttlqt_error_exits () | test_dttmlq_error_exits ();

	if (error_exit_tests_failed) {
		printf ("TT LQ kernels failed the tests of the error exits\n");
	} else {
		printf ("TT LQ kernels passed the tests of the error exits\n");
	}

	printf ("\n");

	return error_exit_tests_failed;
}

int tt_lq_kernels_test (double thresh, int nm, int* mval, int nn, int* nval, int nnb, int* nbval, int test_error_exits) {
	int error_exits_result = 0;
	if (test_error_exits) {
		error_exits_result = test_tt_lq_kernels_error_exits ();
	}

	int no_tests_run = 0, no_tests_failed = 0;

	for (int m_index = 0; m_index < nm; m_index++) {
		int m = mval[m_index];

		for (int n_index = 0; n_index < nn; n_index++) {
			int n = nval[n_index];

			for (int nb_index = 0; nb_index < nnb; nb_index++) {
				int nb = nbval[nb_index];

				if (nb == 0 || nb > min (m, n)) {
					continue;
				}

				double result[NO_TESTS];

				perform_tt_lq_kernels_test (m, n, nb, result);

				for (int test = 0; test < NO_TESTS; test++) {
					if (result[test] >= thresh) {
						no_tests_failed++;

						printf ("TT LQ kernels test number %d failed for M=%d, N=%d, test ratio is %lg\n", test + 1, m, n, result[test]);
					}

					no_tests_run++;
				}
			}
		}
	}

	if (no_tests_failed == 0) {
		printf ("All tests for TT LQ kernels passed the threshold (%d calls)\n", no_tests_run);
		return 0 | error_exits_result;
	} else {
		printf ("%d out of %d tests for TT LQ kernels failed to pass the threshold\n", no_tests_failed, no_tests_run);
		return 1;
	}
}

int main () {
	double thresh = 30;
	int nm = 10;
	int mval[] = {0, 1, 2, 3, 5, 10, 20, 30, 50, 100};
	int nn = 10;
	int nval[] = {0, 1, 2, 3, 5, 10, 20, 30, 50, 100};
	int nnb = 6;
	int nbval[] = {1, 2, 3, 7, 12, 33};

	int test_error_exits = 1;

	return tt_lq_kernels_test (thresh, nm, mval, nn, nval, nnb, nbval, test_error_exits);
}

#define EPSILON __DBL_EPSILON__
#define NO_TESTS 6

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <setjmp.h>

#include "../lapack/lapack.h"
#include "../lapack/cblas_f77.h"

#include "../../src/common/math_utils.h"
#include "../../src/dttqrt/dttqrt.h"
#include "../../src/dttmqr/dttmqr.h"

void perform_tt_qr_kernels_test (int m, int n, int nb, double result[NO_TESTS]) {
	int max_mn = max (m, n);
	int max_1m = max (1, m), max_1n = max (1, n);

	int mpn = m + n, max_1mpn = max (1, mpn);

	int ldt = max (1, nb);

	double zero = 0, one = 1, minus_one = -1;

	int seed[] = {1988, 1989, 1990, 1991};

	// Generate random n-by-n matrix A1_factored
	double* A1_factored = malloc (n * n * sizeof (double));
	for (int col = 0; col < n; col++) {
		int distribution = 2;
		LAPACK_dlarnv (&distribution, seed, &n, A1_factored + n * col);
	}

	// Generate random m-by-n matrix A2 A2_factored
	double* A2_factored = malloc (m * n * sizeof (double));
	for (int col = 0; col < n; col++) {
		int distribution = 2;
		LAPACK_dlarnv (&distribution, seed, &m, A2_factored + m * col);
	}

	// Create (m+n)-by-n matrix A by coupling together A1_factored and A2_factored (where A1_factored goes above A2_factored)
	// and setting all entries in both tiles below the main diagonal to zero
	double* A = malloc (mpn * n * sizeof (double));
	LAPACK_dlaset ("Lower", &n, &n, &zero, &zero, A, &max_1mpn);
	LAPACK_dlacpy ("Upper", &n, &n, A1_factored, &max_1n, A, &max_1mpn);
	LAPACK_dlaset ("Lower", &m, &n, &zero, &zero, A + n, &max_1mpn);
	LAPACK_dlacpy ("Upper", &m, &n, A2_factored, &max_1m, A + n, &max_1mpn);

	// Factor A1_factored and A2_factored using DTTQRT, save the upper triangular factor in T
	double* T = malloc (nb * n * sizeof (double));
	double* work = malloc (max (2, max_mn) * max (2, max_mn) * nb * sizeof (double));
	dttqrt (m, n, nb, A1_factored, n, A2_factored, m, T, ldt, work);

	// Generate the (m+n)-by-(m+n) matrix Q using DTTMQR
	double* Q = malloc (mpn * mpn * sizeof (double));
	LAPACK_dlaset ("Full", &mpn, &mpn, &zero, &one, Q, &max_1mpn);
	dttmqr ('R', 'N', mpn, n, mpn, m, n, nb, Q, max_1mpn, Q + n * mpn, max_1mpn, A2_factored, max_1m, T, ldt, work);

	// Generate the (m+n)-by-(m+n) matrix R by copying the upper triangular part of A1_factored
	double* R = malloc (mpn * mpn * sizeof (double)); // m+n is used instead of n, because R is used as an (m+n)-by-(m+n) work matrix in test 2
	LAPACK_dlaset ("Lower", &mpn, &n, &zero, &zero, R, &max_1mpn);
	LAPACK_dlacpy ("Upper", &n, &n, A1_factored, &max_1n, R, &max_1mpn);

	// Test 1: compute |R - Q'*A| / |A|
	F77_dgemm ("Transposed", "Non-transposed", &mpn, &n, &mpn, &minus_one, Q, &max_1mpn, A, &max_1mpn, &one, R, &max_1mpn);

	double* rwork = malloc (mpn * sizeof (double));
	double A_norm = LAPACK_dlange ("1-norm", &mpn, &n, A, &max_1mpn, rwork);
	double resid_norm = LAPACK_dlange ("1-norm", &mpn, &n, R, &max_1mpn, rwork);

	if (A_norm > 0) {
		result[0] = resid_norm / (EPSILON * max_1mpn * A_norm);
	} else {
		result[0] = 0;
	}

	// Free A
	free (A);

	// Test 2: compute |I - Q'*Q|
	LAPACK_dlaset ("Upper", &mpn, &mpn, &zero, &one, R, &max_1mpn);
	F77_dsyrk ("Upper", "Transposed", &mpn, &mpn, &minus_one, Q, &max_1mpn, &one, R, &max_1mpn);
	resid_norm = LAPACK_dlansy ("1-norm", "Upper", &mpn, R, &max_1mpn, rwork);
	result[1] = resid_norm / (EPSILON * max_1mpn);

	// Free R
	free (R);

	// Generate random (m+n)-by-n matrix C and its copy C_householder
	double* C = malloc (mpn * n * sizeof (double));
	double* C_householder = malloc (mpn * n * sizeof (double));
	for (int col = 0; col < n; col++) {
		int distribution = 2;
		LAPACK_dlarnv (&distribution, seed, &mpn, C + mpn * col);
	}
	LAPACK_dlacpy ("Full", &mpn, &n, C, &max_1mpn, C_householder, &max_1mpn);
	
	// Calculate the norm of C
	double C_norm = LAPACK_dlange ("1-norm", &mpn, &n, C, &max_1mpn, rwork);

	// Apply Q to C_householder using DTTMQR
	dttmqr ('L', 'N', n, n, m, n, n, nb, C_householder, max_1mpn, C_householder + n, max_1mpn, A2_factored, max_1m, T, ldt, work);

	// Test 3: calculate |Q*C_householder - Q*C| / |C|
	// - Q*C_householder is obtained using sequential application of Householder reflectors
	// - Q*C is obtained using standard matrix multiplication
	F77_dgemm ("Non-transposed", "Non-transposed", &mpn, &n, &mpn, &minus_one, Q, &max_1mpn, C, &max_1mpn, &one, C_householder, &max_1mpn);
	resid_norm = LAPACK_dlange ("1-norm", &mpn, &n, C_householder, &max_1mpn, rwork);

	if (C_norm > 0) {
		result[2] = resid_norm / (EPSILON * max_1mpn * C_norm);
	} else {
		result[2] = 0;
	}

	// Copy C into C_householder again
	LAPACK_dlacpy ("Full", &mpn, &n, C, &max_1mpn, C_householder, &max_1mpn);

	// Apply Q^T to C_householder using DTTMQR
	dttmqr ('L', 'T', n, n, m, n, n, nb, C_householder, max_1mpn, C_householder + n, max_1mpn, A2_factored, max_1m, T, ldt, work);

	// Test 4: calculate |Q^T*C_householder - Q^T*C| / |C|
	// - Q^T*C_householder is obtained using sequential application of Householder reflectors
	// - Q^T*C is obtained using standard matrix multiplication
	F77_dgemm ("Transposed", "Non-transposed", &mpn, &n, &mpn, &minus_one, Q, &max_1mpn, C, &max_1mpn, &one, C_householder, &max_1mpn);
	resid_norm = LAPACK_dlange ("1-norm", &mpn, &n, C_householder, &max_1mpn, rwork);

	if (C_norm > 0) {
		result[3] = resid_norm / (EPSILON * max_1mpn * C_norm);
	} else {
		result[3] = 0;
	}

	// Free C and C_householder
	free (C);
	free (C_householder);

	// Generate random n-by-(m+n) matrix D and its copy D_householder
	double* D = malloc (n * mpn * sizeof (double));
	double* D_householder = malloc (n * mpn * sizeof (double));
	for (int col = 0; col < mpn; col++) {
		int distribution = 2;
		LAPACK_dlarnv (&distribution, seed, &n, D + n * col);
	}
	LAPACK_dlacpy ("Full", &n, &mpn, D, &max_1n, D_householder, &max_1n);

	// Calculate the norm of D
	double D_norm = LAPACK_dlange ("1-norm", &n, &mpn, D, &max_1n, rwork);

	// Apply Q to D_householder using DTTMQR
	dttmqr ('R', 'N', n, n, n, m, n, nb, D_householder, max_1n, D_householder + n * n, max_1n, A2_factored, max_1m, T, ldt, work);

	// Test 5: compute |D_householder*Q - D*Q| / |D|
	// - D_householder*Q is obtained using sequential application of Householder reflectors
	// - D*Q is obtained using standard matrix multiplication
	F77_dgemm ("Non-transposed", "Non-transposed", &n, &mpn, &mpn, &minus_one, D, &max_1n, Q, &max_1mpn, &one, D_householder, &max_1n);
	resid_norm = LAPACK_dlange ("1-norm", &n, &mpn, D_householder, &max_1n, rwork);

	if (D_norm > 0) {
		result[4] = resid_norm / (EPSILON * max_1mpn * D_norm);
	} else {
		result[4] = 0;
	}

	// Copy D into D_householder again
	LAPACK_dlacpy ("Full", &n, &mpn, D, &max_1n, D_householder, &max_1n);

	// Apply Q^T to D_householder using DTTMQR
	dttmqr ('R', 'T', n, n, n, m, n, nb, D_householder, max_1n, D_householder + n * n, max_1n, A2_factored, max_1m, T, ldt, work);

	// Test 6: compute |D_householder*Q^T - D*Q^T| / |D|
	// - D_householder*Q^T is obtained using sequential application of Householder reflectors
	// - D*Q^T is obtained using standard matrix multiplication
	F77_dgemm ("Non-transposed", "Transposed", &n, &mpn, &mpn, &minus_one, D, &max_1n, Q, &max_1mpn, &one, D_householder, &max_1n);
	resid_norm = LAPACK_dlange ("1-norm", &n, &mpn, D_householder, &max_1n, rwork);

	if (D_norm > 0) {
		result[5] = resid_norm / (EPSILON * max_1mpn * D_norm);
	} else {
		result[5] = 0;
	}

	// Free remaining matrices
	free (A1_factored);
	free (A2_factored);
	free (T);
	free (Q);
	free (work);

	free (D);
	free (D_householder);
	free (rwork);
}

volatile sig_atomic_t sigsegv_flag = 0;
sigjmp_buf mark;

void sigsegv_handler (int sig) {
	// Do not mark sig as unused
	(void)sig;

	sigsegv_flag = 1;
	siglongjmp (mark, 1);
}

int check_sigsegv_flag (char* routine_name, int param_index) {
	if (sigsegv_flag == 1) {
		fprintf (stderr, "Illegal memory access detected when parameter %d of %s has illegal value\n", param_index, routine_name);
		sigsegv_flag = 0;

		return 1;
	}

	return 0;
}

int test_dttqrt_illegal_param (int m, int n, int nb, double* A1, int lda1, double* A2, int lda2, double* T, int ldt, double* work, int illegal_param_index) {
	int failed = 0;

	if (sigsetjmp (mark, 1) == 0) {
		if (dttqrt (m, n, nb, A1, lda1, A2, lda2, T, ldt, work) != illegal_param_index) {
			fprintf (stderr, "Illegal value of parameter number %d not detected by DTTQRT\n", illegal_param_index);
			failed = 1;
		}
	}
	failed |= check_sigsegv_flag ("DTTQRT", illegal_param_index);

	return failed;
}

int test_dttmqr_illegal_param (char side, char trans, int m1, int n1, int m2, int n2, int k, int nb, double* A1, int lda1, double* A2, int lda2, double* V, int ldv, double* T, int ldt, double* work, int illegal_param_index) {
	int failed = 0;

	if (sigsetjmp (mark, 1) == 0) {
		if (dttmqr (side, trans, m1, n1, m2, n2, k, nb, A1, lda1, A2, lda2, V, ldv, T, ldt, work) != illegal_param_index) {
			fprintf (stderr, "Illegal value of parameter number %d not detected by DTTMQR\n", illegal_param_index);
			failed = 1;
		}
	}
	failed |= check_sigsegv_flag ("DTTMQR", illegal_param_index);

	return failed;
}

int test_dttmqr_legal_param (char side, char trans, int m1, int n1, int m2, int n2, int k, int nb, double* A1, int lda1, double* A2, int lda2, double* V, int ldv, double* T, int ldt, double* work, int checked_param_index) {
	if (dttmqr (side, trans, m1, n1, m2, n2, k, nb, A1, lda1, A2, lda2, V, ldv, T, ldt, work) == checked_param_index) {
		fprintf (stderr, "Illegal value of parameter number %d falsely detected by DTTMQR\n", checked_param_index);
		return 1;
	}

	return 0;
}

int test_dttqrt_error_exits () {
	// Set used matrices to NULL in order to ensure that they are not accessed
	double* A1 = NULL;
	double* A2 = NULL;
	double* T = NULL;
	double* work = NULL;

	// Attach custom signal handler to SIGSEGV in order to catch illegal memory accesses
	struct sigaction sigsegv_action;
	sigsegv_action.sa_handler = sigsegv_handler;
	sigfillset (&sigsegv_action.sa_mask);
	sigsegv_action.sa_flags = 0;

	sigaction (SIGSEGV, &sigsegv_action, NULL);

	int dttqrt_error_exit_tests_failed = 0;

	// Test error exit 1 (m < 0)
	dttqrt_error_exit_tests_failed |= test_dttqrt_illegal_param (-1, 0, 0, A1, 1, A2, 1, T, 1, work, 1);

	// Test error exit 2 (n < 0)
	dttqrt_error_exit_tests_failed |= test_dttqrt_illegal_param (0, -1, 0, A1, 1, A2, 1, T, 1, work, 2);

	// Test error exit 3 (nb <= 0)
	dttqrt_error_exit_tests_failed |= test_dttqrt_illegal_param (1, 1, 0, A1, 1, A2, 1, T, 1, work, 3);

	// Test error exit 5 (lda1 < max (1, n))
	dttqrt_error_exit_tests_failed |= test_dttqrt_illegal_param (1, 2, 1, A1, 1, A2, 1, T, 1, work, 5);

	// Test error exit 7 (lda2 < max (1, m))
	dttqrt_error_exit_tests_failed |= test_dttqrt_illegal_param (2, 1, 1, A1, 1, A2, 1, T, 1, work, 7);

	// Test error exit 9 (ldt < max (1, nb))
	dttqrt_error_exit_tests_failed |= test_dttqrt_illegal_param (2, 2, 2, A1, 2, A2, 2, T, 1, work, 9);

	// Detach custom SIGSEGV handler
	sigsegv_action.sa_handler = SIG_DFL;
	sigaction (SIGSEGV, &sigsegv_action, NULL);

	return dttqrt_error_exit_tests_failed;
}

int test_dttmqr_error_exits () {
	// Set used matrices to NULL in order to ensure that they are not accessed
	double* V = NULL;
	double* A1 = NULL;
	double* A2 = NULL;
	double* T = NULL;
	double* work = NULL;

	// Attach custom signal handler to SIGSEGV in order to catch illegal memory accesses
	struct sigaction sigsegv_action;
	sigsegv_action.sa_handler = sigsegv_handler;
	sigfillset (&sigsegv_action.sa_mask);
	sigsegv_action.sa_flags = 0;

	sigaction (SIGSEGV, &sigsegv_action, NULL);

	int dttmqr_error_exit_tests_failed = 0;

	// Test error exit 1 (side is not one of 'L', 'l', 'T', 't')
	dttmqr_error_exit_tests_failed |= test_dttmqr_legal_param ('l', 'N', 0, 0, 0, 0, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 1);
	dttmqr_error_exit_tests_failed |= test_dttmqr_illegal_param ('/', 'N', 1, 1, 1, 1, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 1);

	// Test error exit 2 (trans is not one of 'N', 'n', 'T', 't')
	dttmqr_error_exit_tests_failed |= test_dttmqr_legal_param ('L', 'n', 0, 0, 0, 0, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 2);
	dttmqr_error_exit_tests_failed |= test_dttmqr_illegal_param ('L', '/', 1, 1, 1, 1, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 2);

	// Test error exit 3 (m1 < 0)
	dttmqr_error_exit_tests_failed |= test_dttmqr_illegal_param ('L', 'N', -1, 1, 1, 1, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 3);

	// Test error exit 4 (n1 < 0)
	dttmqr_error_exit_tests_failed |= test_dttmqr_illegal_param ('L', 'N', 1, -1, 1, 1, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 4);

	// Test error exit 5 (m2 < 0, m1 != m2 if side = 'R')
	dttmqr_error_exit_tests_failed |= test_dttmqr_illegal_param ('L', 'N', 1, 1, -1, 1, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 5);
	dttmqr_error_exit_tests_failed |= test_dttmqr_illegal_param ('R', 'N', 1, 1, 2, 1, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 5);

	// Test error exit 6 (n2 < 0, n1 != n2 if side = 'L')
	dttmqr_error_exit_tests_failed |= test_dttmqr_illegal_param ('L', 'N', 1, 1, 1, -1, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 6);
	dttmqr_error_exit_tests_failed |= test_dttmqr_illegal_param ('L', 'N', 1, 1, 1, 2, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 6);

	// Test error exit 7 (k < 0, k > m1 if side = 'L', k > n1 if side = 'R')
	dttmqr_error_exit_tests_failed |= test_dttmqr_illegal_param ('L', 'N', 1, 1, 1, 1, -1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 7);
	dttmqr_error_exit_tests_failed |= test_dttmqr_illegal_param ('L', 'N', 1, 2, 1, 2, 2, 1, A1, 1, A2, 1, V, 1, T, 1, work, 7);
	dttmqr_error_exit_tests_failed |= test_dttmqr_illegal_param ('R', 'N', 2, 1, 2, 1, 2, 1, A1, 2, A2, 2, V, 1, T, 1, work, 7);

	// Test error exit 8 (nb <= 0)
	dttmqr_error_exit_tests_failed |= test_dttmqr_illegal_param ('L', 'N', 1, 1, 1, 1, 1, 0, A1, 1, A2, 1, V, 1, T, 1, work, 8);

	// Test error exit 10 (lda1 < max (1, m1))
	dttmqr_error_exit_tests_failed |= test_dttmqr_illegal_param ('L', 'N', 2, 1, 1, 1, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 10);

	// Test error exit 12 (lda1 < max (1, m1))
	dttmqr_error_exit_tests_failed |= test_dttmqr_illegal_param ('L', 'N', 1, 1, 2, 1, 1, 1, A1, 1, A2, 1, V, 1, T, 1, work, 12);

	// Test error exit 14 (ldv < max (1, m2) if side = 'L', ldv < max (1, n2) if side = 'R')
	dttmqr_error_exit_tests_failed |= test_dttmqr_illegal_param ('L', 'N', 1, 1, 2, 1, 1, 1, A1, 1, A2, 2, V, 1, T, 1, work, 14);
	dttmqr_error_exit_tests_failed |= test_dttmqr_illegal_param ('R', 'N', 1, 1, 1, 2, 1, 1, A1, 1, A2, 2, V, 1, T, 1, work, 14);

	// Test error exit 16 (ldt < max (1, nb))
	dttmqr_error_exit_tests_failed |= test_dttmqr_illegal_param ('L', 'N', 2, 2, 2, 2, 2, 2, A1, 2, A2, 2, V, 2, T, 1, work, 16);

	// Detach custom SIGSEGV handler
	sigsegv_action.sa_handler = SIG_DFL;
	sigaction (SIGSEGV, &sigsegv_action, NULL);

	return dttmqr_error_exit_tests_failed;
}

int test_tt_qr_kernels_error_exits () {
	int error_exit_tests_failed = test_dttqrt_error_exits () | test_dttmqr_error_exits ();

	if (error_exit_tests_failed) {
		printf ("TT QR kernels failed the tests of the error exits\n");
	} else {
		printf ("TT QR kernels passed the tests of the error exits\n");
	}

	printf ("\n");

	return error_exit_tests_failed;
}

int tt_qr_kernels_test (double thresh, int nm, int* mval, int nn, int* nval, int nnb, int* nbval, int test_error_exits) {
	int error_exits_result = 0;
	if (test_error_exits) {
		error_exits_result = test_tt_qr_kernels_error_exits ();
	}

	int no_tests_run = 0, no_tests_failed = 0;

	for (int m_index = 0; m_index < nm; m_index++) {
		int m = mval[m_index];

		for (int n_index = 0; n_index < nn; n_index++) {
			int n = nval[n_index];

			for (int nb_index = 0; nb_index < nnb; nb_index++) {
				int nb = nbval[nb_index];

				if (nb == 0 || nb > min (m, n)) {
					continue;
				}

				double result[NO_TESTS];

				perform_tt_qr_kernels_test (m, n, nb, result);

				for (int test = 0; test < NO_TESTS; test++) {
					if (result[test] >= thresh) {
						no_tests_failed++;

						printf ("TT QR kernels test number %d failed for M=%d, N=%d, test ratio is %lg\n", test + 1, m, n, result[test]);
					}

					no_tests_run++;
				}
			}
		}
	}

	if (no_tests_failed == 0) {
		printf ("All tests for TT QR kernels passed the threshold (%d calls)\n", no_tests_run);
		return 0 | error_exits_result;
	} else {
		printf ("%d out of %d tests for TT QR kernels failed to pass the threshold\n", no_tests_failed, no_tests_run);
		return 1;
	}
}

int main () {
	double thresh = 30;
	int nm = 10;
	int mval[] = {0, 1, 2, 3, 5, 10, 20, 30, 50, 100};
	int nn = 10;
	int nval[] = {0, 1, 2, 3, 5, 10, 20, 30, 50, 100};
	int nnb = 6;
	int nbval[] = {1, 2, 3, 7, 12, 33};

	int test_error_exits = 1;

	return tt_qr_kernels_test (thresh, nm, mval, nn, nval, nnb, nbval, test_error_exits);
}

#define EPSILON __DBL_EPSILON__
#define NO_TESTS 3

#include "../../src/pdge2gb/pdge2gb.h"
#include "../../src/qr/qr.h"
#include "../../src/lq/lq.h"
#include "../../src/common/math_utils.h"


#include "../lapack/lapack.h"
#include "../lapack/cblas_f77.h"

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <setjmp.h>

#if defined (USE_STARPU)
#include <starpu.h>
#endif

#if defined (USE_MKL)
#include <mkl_service.h>
#elif defined (USE_ARMPL)
#include <armpl.h>
#endif

void perform_pdge2gb_test (int m, int n, config_t config, double result[NO_TESTS]) {
	// Get relevant config options
	int nb = config.nb;
	int ib = config.ib;
	elim_scheme_e elim_scheme = config.elim_scheme;

	int min_mn = min (m, n), max_mn = max (m, n);
	int max_1m = max (1, m), max_1n = max (1, n);

	int ldt = ib;

	double zero = 0, one = 1, minus_one = -1;

	int seed[] = {1988, 1989, 1990, 1991};

	// Generate random m-by-n matrix A and its copy A_factored
	double* A = malloc (m * n * sizeof (double));
	double* A_factored = malloc (m * n * sizeof (double));
	for (int col = 0; col < n; col++) {
		int distribution = 2;
		LAPACK_dlarnv (&distribution, seed, &m, A + m * col);
	}
	LAPACK_dlacpy ("Full", &m, &n, A, &max_1m, A_factored, &max_1m);

	// Calculate the norm of A
	double* rwork = malloc (max_mn * sizeof (double));
	double A_norm = LAPACK_dlange ("1-norm", &m, &n, A, &max_1m, rwork);

	int no_blocks_x = (n - 1) / nb + 1;
	int no_blocks_y = (m - 1) / nb + 1;

	// Transform A_factored to band using PDGE2GB, save the upper triangular factors in TU and TV
	double* TU = malloc (get_t_size_multiplier_for_elim_scheme (elim_scheme) * ib * no_blocks_y * min_mn * sizeof (double));
	double* TV = malloc (get_t_size_multiplier_for_elim_scheme (elim_scheme) * ib * no_blocks_x * min_mn * sizeof (double));
	elim_context_t qr_context, lq_context;
	pdge2gb (m, n, A_factored, max_1m, TU, ldt, TV, ldt, &config, &qr_context, &lq_context);

	// Generate the m-by-m matrix U using PDGEMQR
	double* U = malloc (m * m * sizeof (double));
	LAPACK_dlaset ("Full", &m, &m, &zero, &one, U, &max_1m); //TODO: move to second branch
	if (m >= n) {
		pdgemqr ('R', 'N', m, m, min_mn, A_factored, max_1m, TU, ldt, U, max_1m, qr_context);
	} else {
		pdgemqr ('R', 'N', m - nb, m - nb, min (m - nb, n), A_factored + nb, max_1m, TU, ldt, U + m * nb + nb, max_1m, qr_context);
	}

	// Generate the n-by-n matrix VT using PDGEMLQ
	double* VT = malloc (n * n * sizeof (double));
	LAPACK_dlaset ("Full", &n, &n, &zero, &one, VT, &max_1n); //TODO: move to first branch
	if (m >= n) {
		pdgemlq ('R', 'N', n - nb, n - nb, min (m, n - nb), A_factored + m * nb, max_1m, TV, ldt, VT + n * nb + nb, max_1n, lq_context);
	} else {
		pdgemlq ('R', 'N', n, n, min_mn, A_factored, max_1m, TV, ldt, VT, max_1n, lq_context);
	}

	// Generate the m-by-n matrix B by copying the upper/lower triangular band part of A_factored
	double* B = malloc (max_mn * max_mn * sizeof (double)); // max_mn is used instead of n, because B is used as an m-by-n matrix in test 2 and an n-by-n work matrix in test 3
	LAPACK_dlaset ("Full", &m, &n, &zero, &zero, B, &max_1m);
	int m_plus_one = m + 1, nb_plus_one = nb + 1;
	if (m >= n) {
		int n_minus_nb = n - nb;
		LAPACK_dlacpy ("Upper", &nb, &nb, A_factored, &max_1m, B, &max_1m);
		LAPACK_dlacpy ("Full", &nb_plus_one, &n_minus_nb, A_factored + nb * m, &m_plus_one, B + nb * m, &m_plus_one);
	} else {
		int m_minus_nb = m - nb;
		LAPACK_dlacpy ("Full", &nb_plus_one, &m_minus_nb, A_factored, &m_plus_one, B, &m_plus_one);
		LAPACK_dlacpy ("Lower", &nb, &nb, A_factored + m_minus_nb * m + m_minus_nb, &m, B + m_minus_nb * m + m_minus_nb, &m);
	}

	// Test 1: compute |A - U*B*VT| / |A|
	double* temp = malloc (m * n * sizeof (double));
	F77_dgemm ("Non-transposed", "Non-transposed", &m, &n, &m, &one, U, &max_1m, B, &max_1m, &zero, temp, &max_1m);
	F77_dgemm ("Non-transposed", "Non-transposed", &m, &n, &n, &minus_one, temp, &max_1m, VT, &max_1n, &one, A, &max_1m);

	double resid_norm = LAPACK_dlange ("1-norm", &m, &n, A, &max_1m, rwork);

	if (A_norm > 0) {
		result[0] = resid_norm / (EPSILON * max_1m * A_norm);
	} else {
		result[0] = 0;
	}

	// Free A
	free (A);

	// Test 2: compute |I - U'*U|
	LAPACK_dlaset ("Upper", &m, &m, &zero, &one, B, &max_1m);
	F77_dsyrk ("Upper", "Transposed", &m, &m, &minus_one, U, &max_1m, &one, B, &max_1m);
	resid_norm = LAPACK_dlansy ("1-norm", "Upper", &m, B, &max_1m, rwork);
	result[1] = resid_norm / (EPSILON * max_1m);

	// Test 3: compute |I - VT'*VT|
	LAPACK_dlaset ("Upper", &n, &n, &zero, &one, B, &max_1n);
	F77_dsyrk ("Upper", "Non-transposed", &n, &n, &minus_one, VT, &max_1n, &one, B, &max_1n);
	resid_norm = LAPACK_dlansy ("1-norm", "Upper", &n, B, &max_1n, rwork);
	result[2] = resid_norm / (EPSILON * max_1n);

	// Free allocated matrices
	free (A_factored);
	free (TU);
	free (TV);
	free (B);
	free (U);
	free (VT);

	free (rwork);
}

volatile sig_atomic_t sigsegv_flag = 0;
sigjmp_buf mark;

void sigsegv_handler (int sig) {
	// Do not mark sig as unused
	(void)sig;

	sigsegv_flag = 1;
	siglongjmp (mark, 1);
}

int check_sigsegv_flag (char* routine_name, int param_index) {
	if (sigsegv_flag == 1) {
		fprintf (stderr, "Illegal memory access detected when parameter %d of %s has illegal value\n", param_index, routine_name);
		sigsegv_flag = 0;

		return 1;
	}

	return 0;
}

int test_pdge2gb_illegal_param (int m, int n, double* A, int lda, double* TU, int ldtu, double* TV, int ldtv, config_t config, int illegal_param_index) {
	int failed = 0;

	if (sigsetjmp (mark, 1) == 0) {
		if (pdge2gb (m, n, A, lda, TU, ldtu, TV, ldtv, &config, NULL, NULL) != illegal_param_index) {
			fprintf (stderr, "Illegal value of parameter number %d not detected by PDGE2GB\n", illegal_param_index);
			failed = 1;
		}
	}
	failed |= check_sigsegv_flag ("PDGE2GB", illegal_param_index);

	return failed;
}

int perform_pdge2gb_error_exit_tests () {
	// Set used matrices & contexts to NULL in order to ensure that they are not accessed
	double* A = NULL;
	double* TU = NULL;
	double* TV = NULL;

	// Attach custom signal handler to SIGSEGV in order to catch illegal memory accesses
	struct sigaction sigsegv_action;
	sigsegv_action.sa_handler = sigsegv_handler;
	sigfillset (&sigsegv_action.sa_mask);
	sigsegv_action.sa_flags = 0;

	sigaction (SIGSEGV, &sigsegv_action, NULL);

	int pdge2gb_error_exit_tests_failed = 0;

	// Create and initialize config
	config_t config;
	config = (config_t) {
		.nb = 1,
		.ib = 1,
		.superblock_size_factor = 1,
		.elim_scheme = TsFlatTree,
		.layout_translation = 0
	};

	// Test error exit 1 (m < 0)
	pdge2gb_error_exit_tests_failed |= test_pdge2gb_illegal_param (-1, 1, A, 1, TU, 1, TV, 1, config, 1);

	// Test error exit 2 (n < 0)
	pdge2gb_error_exit_tests_failed |= test_pdge2gb_illegal_param (1, -1, A, 1, TU, 1, TV, 1, config, 2);

	// Test error exit 4 (lda < max (1, m))
	pdge2gb_error_exit_tests_failed |= test_pdge2gb_illegal_param (2, 1, A, 1, TU, 1, TV, 1, config, 4);

	// Test error exit 6 (ldtu < max (1, ib))
	config.nb = config.ib = 2;
	pdge2gb_error_exit_tests_failed |= test_pdge2gb_illegal_param (2, 2, A, 2, TU, 1, TV, 2, config, 6);

	// Test error exit 8 (ldtv < max (1, ib))
	pdge2gb_error_exit_tests_failed |= test_pdge2gb_illegal_param (2, 2, A, 2, TU, 2, TV, 1, config, 8);
	config.nb = config.ib = 1;

	// Test error exit 9 (nb <= 0)
	config.nb = 0;
	pdge2gb_error_exit_tests_failed |= test_pdge2gb_illegal_param (1, 1, A, 1, TU, 1, TV, 1, config, 9);
	config.nb = 1;

	// Test error exit 9 (ib <= 0, ib > nb)
	config.ib = 0;
	pdge2gb_error_exit_tests_failed |= test_pdge2gb_illegal_param (1, 1, A, 1, TU, 1, TV, 1, config, 9);
	config.ib = 2;
	pdge2gb_error_exit_tests_failed |= test_pdge2gb_illegal_param (1, 1, A, 1, TU, 2, TV, 2, config, 9);
	config.ib = 1;

	// Test error exit 9 (superblock_size_factor <= 0)
	config.superblock_size_factor = 0;
	pdge2gb_error_exit_tests_failed |= test_pdge2gb_illegal_param (1, 1, A, 1, TU, 2, TV, 2, config, 9);
	config.superblock_size_factor = 1;

	// Test error exit 9 (elim_scheme < 0, elim_scheme >= NO_ELIM_SCHEMES)
	config.elim_scheme = -1;
	pdge2gb_error_exit_tests_failed |= test_pdge2gb_illegal_param (1, 1, A, 1, TU, 2, TV, 2, config, 9);
	config.elim_scheme = NO_ELIM_SCHEMES;
	pdge2gb_error_exit_tests_failed |= test_pdge2gb_illegal_param (1, 1, A, 1, TU, 2, TV, 2, config, 9);
	config.elim_scheme = TsFlatTree;

	// Test error exit 9 (layout_translation < 0, layout_translation > 1)
	config.layout_translation = -1;
	pdge2gb_error_exit_tests_failed |= test_pdge2gb_illegal_param (1, 1, A, 1, TU, 2, TV, 2, config, 9);
	config.layout_translation = 2;
	pdge2gb_error_exit_tests_failed |= test_pdge2gb_illegal_param (1, 1, A, 1, TU, 2, TV, 2, config, 9);
	config.layout_translation = 0;

	// Detach custom SIGSEGV handler
	sigsegv_action.sa_handler = SIG_DFL;
	sigaction (SIGSEGV, &sigsegv_action, NULL);

	return pdge2gb_error_exit_tests_failed;
}

int test_pdge2gb_error_exits () {
	int error_exit_tests_failed = perform_pdge2gb_error_exit_tests ();

	if (error_exit_tests_failed) {
		fprintf (stderr, "PDGE2GB failed the tests of the error exits\n");
	} else {
		printf ("PDGE2GB passed the tests of the error exits\n");
	}

	printf ("\n");

	return error_exit_tests_failed;
}

int pdge2gb_test (double thresh, int no_m_values, int* m_values, int no_n_values, int* n_values, int no_nb_values, int* nb_values, int no_elim_schemes, elim_scheme_e* elim_schemes, int test_layout_translation, int test_error_exits) {
	#if defined (USE_MKL)
	mkl_set_num_threads (1);
	#elif defined (USE_ARMPL)
	armpl_set_num_threads (1);
	#endif

	#if defined (USE_STARPU)
	if (starpu_init (NULL) != 0) {
		fprintf (stderr, "Error: StarPU could not be initialized\n");
		exit (1);
	}
	#endif

	int error_exits_result = 0;
	if (test_error_exits) {
		error_exits_result = test_pdge2gb_error_exits ();
	}

	int no_tests_run = 0, no_tests_failed = 0;

	for (int m_index = 0; m_index < no_m_values; m_index++) {
		int m = m_values[m_index];

		for (int n_index = 0; n_index < no_n_values; n_index++) {
			int n = n_values[n_index];

			for (int nb_index = 0; nb_index < no_nb_values; nb_index++) {
				int nb = nb_values[nb_index];

				if (nb == 0 || nb > min (m, n)) {
					continue;
				}

				for (int elim_scheme_index = 0; elim_scheme_index < no_elim_schemes; elim_scheme_index++) {
					int elim_scheme = elim_schemes[elim_scheme_index];

					for (int layout_translation = 0; layout_translation <= !!test_layout_translation; layout_translation++) {
						double result[NO_TESTS];

						config_t config = {
							.nb = nb,
							.ib = max (1, nb / 4), //TODO
							.superblock_size_factor = 4, //TODO
							.elim_scheme = elim_scheme,
							.layout_translation = layout_translation
						};

						perform_pdge2gb_test (m, n, config, result);

						for (int test = 0; test < NO_TESTS; test++) {
							if (result[test] >= thresh) {
								no_tests_failed++;

								fprintf (stderr,
										"PDGE2GB test number %d failed for M=%d, N=%d, NB=%d, IB=%d, gamma=%d, elimination scheme %s, layout translation %s, test ratio is %lg\n",
										test + 1,
										m,
										n,
										config.nb,
										config.ib,
										config.superblock_size_factor,
										get_elim_scheme_name (config.elim_scheme),
										config.layout_translation ? "on" : "off",
										result[test]
								);
							}

							no_tests_run++;
						}
					}
				}
			}
		}
	}

	#if defined (USE_STARPU)
	starpu_shutdown ();
	#endif

	if (no_tests_failed == 0) {
		printf ("All tests for PDGE2GB passed the threshold (%d calls)\n", no_tests_run);
		return 0 | error_exits_result;
	} else {
		fprintf (stderr, "%d out of %d tests for PDGE2GB failed to pass the threshold\n", no_tests_failed, no_tests_run);
		return 1;
	}
}

int main () {
	double thresh = 30;
	int no_m_values = 10;
	int m_values[] = {0, 1, 2, 3, 5, 10, 20, 30, 50, 100};
	int no_n_values = 10;
	int n_values[] = {0, 1, 2, 3, 5, 10, 20, 30, 50, 100};
	int no_nb_values = 6;
	int nb_values[] = {1, 2, 3, 7, 12, 33};
	int no_elim_schemes = NO_ELIM_SCHEMES;
	elim_scheme_e elim_schemes[] = {TsFlatTree, TtFlatTree, TtGreedy, TtBinaryTree, TtFibonacci, SuperblockGreedy, SuperblockBinaryTree, SuperblockFibonacci, AutoTree};

	int test_layout_translation = 1;
	int test_error_exits = 1;

	return pdge2gb_test (
		thresh,
		no_m_values,
		m_values,
		no_n_values,
		n_values,
		no_nb_values,
		nb_values,
		no_elim_schemes,
		elim_schemes,
		test_layout_translation,
		test_error_exits
	);
}

#define EPSILON __DBL_EPSILON__
#define NO_TESTS 6

#include "../../src/lq/lq.h"
#include "../../src/common/config.h"
#include "../../src/common/math_utils.h"

#include "../lapack/lapack.h"
#include "../lapack/cblas_f77.h"

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <setjmp.h>

#if defined (USE_STARPU)
#include <starpu.h>
#endif

#if defined (USE_MKL)
#include <mkl_service.h>
#elif defined (USE_ARMPL)
#include <armpl.h>
#endif

void perform_lq_routines_test (int m, int n, config_t config, double result[NO_TESTS]) {
	// Get relevant config options
	int mb = config.nb;
	int ib = config.ib;
	elim_scheme_e elim_scheme = config.elim_scheme;

	int min_mn = min (m, n), max_mn = max (m, n);
	int max_1m = max (1, m), max_1n = max (1, n);
	int max_1mn = max (1, max_mn);

	int ldt = max (1, ib);

	double zero = 0, one = 1, minus_one = -1;

	int seed[] = {1988, 1989, 1990, 1991};

	// Generate random m-by-n matrix A and its copy A_factored
	double* A = malloc (m * n * sizeof (double));
	double* A_factored = malloc (m * n * sizeof (double));
	for (int col = 0; col < n; col++) {
		int distribution = 2;
		LAPACK_dlarnv (&distribution, seed, &m, A + m * col);
	}
	LAPACK_dlacpy ("Full", &m, &n, A, &max_1m, A_factored, &max_1m);

	int no_blocks_x = (n - 1) / mb + 1;

	// Factor A_factored using PDGELQF, save the upper triangular factor in T
	double* T = malloc (get_t_size_multiplier_for_elim_scheme (elim_scheme) * ib * no_blocks_x * min_mn * sizeof (double));
	elim_context_t context;
	pdgelqf (m, n, A_factored, max_1m, T, ldt, &config, &context);

	// Generate the n-by-n matrix Q using PDGEMLQ
	double* Q = malloc (n * n * sizeof (double));
	LAPACK_dlaset ("Full", &n, &n, &zero, &one, Q, &max_1n);
	pdgemlq ('R', 'N', n, n, min_mn, A_factored, max_1m, T, ldt, Q, max_1n, context);

	// Generate the max(m,n)-by-n matrix L by copying the lower triangular part of A_factored
	double* L = malloc (max_mn * n * sizeof (double)); // max_mn is used instead of n, because L is used as an n-by-n work matrix in test 2
	LAPACK_dlaset ("Upper", &m, &n, &zero, &zero, L, &max_1mn);
	LAPACK_dlacpy ("Lower", &m, &n, A_factored, &max_1m, L, &max_1mn);

	// Test 1: compute |L - A*Q'| / |A|
	F77_dgemm ("Non-transposed", "Transposed", &m, &n, &n, &minus_one, A, &max_1m, Q, &max_1n, &one, L, &max_1mn);

	double* rwork = malloc (max_mn * sizeof (double));
	double A_norm = LAPACK_dlange ("1-norm", &m, &n, A, &max_1m, rwork);
	double resid_norm = LAPACK_dlange ("1-norm", &m, &n, L, &max_1mn, rwork);

	if (A_norm > 0) {
		result[0] = resid_norm / (EPSILON * max_1m * A_norm);
	} else {
		result[0] = 0;
	}

	// Free A
	free (A);

	// Test 2: compute |I - Q'*Q|
	LAPACK_dlaset ("Upper", &n, &n, &zero, &one, L, &max_1mn);
	F77_dsyrk ("Upper", "Transposed", &n, &n, &minus_one, Q, &max_1n, &one, L, &max_1mn);
	resid_norm = LAPACK_dlansy ("1-norm", "Upper", &n, L, &max_1mn, rwork);
	result[1] = resid_norm / (EPSILON * max_1n);

	// Free L
	free (L);

	// Generate random n-by-m matrix D and its copy D_householder
	double* D = malloc (n * m * sizeof (double));
	double* D_householder = malloc (n * m * sizeof (double));
	for (int col = 0; col < m; col++) {
		int distribution = 2;
		LAPACK_dlarnv (&distribution, seed, &n, D + n * col);
	}
	LAPACK_dlacpy ("Full", &n, &m, D, &max_1n, D_householder, &max_1n);
	
	// Calculate the norm of D
	double D_norm = LAPACK_dlange ("1-norm", &n, &m, D, &max_1n, rwork);

	// Apply Q to D_householder using PDGEMLQ
	pdgemlq ('L', 'N', n, m, min_mn, A_factored, max_1m, T, ldt, D_householder, max_1n, context);

	// Test 3: calculate |Q*D_householder - Q*D| / |D|
	// - Q*D_householder is obtained using sequential application of Householder reflectors
	// - Q*D is obtained using standard matrix multiplication
	F77_dgemm ("Non-transposed", "Non-transposed", &n, &m, &n, &minus_one, Q, &max_1n, D, &max_1n, &one, D_householder, &max_1n);
	resid_norm = LAPACK_dlange ("1-norm", &n, &m, D_householder, &max_1n, rwork);

	if (D_norm > 0) {
		result[2] = resid_norm / (EPSILON * max_1m * D_norm);
	} else {
		result[2] = 0;
	}

	// Copy D into D_householder again
	LAPACK_dlacpy ("Full", &n, &m, D, &max_1n, D_householder, &max_1n);

	// Apply Q^T to D_householder using PDGEMLQ
	pdgemlq ('L', 'T', n, m, min_mn, A_factored, max_1m, T, ldt, D_householder, max_1n, context);

	// Test 4: calculate |Q^T*D_householder - Q^T*D| / |D|
	// - Q^T*D_householder is obtained using sequential application of Householder reflectors
	// - Q^T*D is obtained using standard matrix multiplication
	F77_dgemm ("Transposed", "Non-transposed", &n, &m, &n, &minus_one, Q, &max_1n, D, &max_1n, &one, D_householder, &max_1n);
	resid_norm = LAPACK_dlange ("1-norm", &n, &m, D_householder, &max_1n, rwork);

	if (D_norm > 0) {
		result[3] = resid_norm / (EPSILON * max_1m * D_norm);
	} else {
		result[3] = 0;
	}

	// Free D and D_householder
	free (D);
	free (D_householder);

	// Generate random m-by-n matrix C and its copy C_householder
	double* C = malloc (m * n * sizeof (double));
	double* C_householder = malloc (m * n * sizeof (double));
	for (int col = 0; col < n; col++) {
		int distribution = 2;
		LAPACK_dlarnv (&distribution, seed, &m, C + m * col);
	}
	LAPACK_dlacpy ("Full", &m, &n, C, &max_1m, C_householder, &max_1m);

	// Calculate the norm of C
	double C_norm = LAPACK_dlange ("1-norm", &m, &n, C, &max_1m, rwork);

	// Apply Q to C_householder using PDGEMLQ
	pdgemlq ('R', 'N', m, n, min_mn, A_factored, max_1m, T, ldt, C_householder, max_1m, context);

	// Test 5: compute |C_householder*Q - C*Q| / |C|
	// - C_householder*Q is obtained using sequential application of Householder reflectors
	// - C*Q is obtained using standard matrix multiplication
	F77_dgemm ("Non-transposed", "Non-transposed", &m, &n, &n, &minus_one, C, &max_1m, Q, &max_1n, &one, C_householder, &max_1m);
	resid_norm = LAPACK_dlange ("1-norm", &m, &n, C_householder, &max_1m, rwork);

	if (C_norm > 0) {
		result[4] = resid_norm / (EPSILON * max_1m * C_norm);
	} else {
		result[4] = 0;
	}

	// Copy C into C_householder again
	LAPACK_dlacpy ("Full", &m, &n, C, &max_1m, C_householder, &max_1m);

	// Apply Q^T to C_householder using PDGEMLQ
	pdgemlq ('R', 'T', m, n, min_mn, A_factored, max_1m, T, ldt, C_householder, max_1m, context);

	// Test 6: compute |C_householder*Q^T - C*Q^T| / |C|
	// - C_householder*Q^T is obtained using sequential application of Householder reflectors
	// - C*Q^T is obtained using standard matrix multiplication
	F77_dgemm ("Non-transposed", "Transposed", &m, &n, &n, &minus_one, C, &max_1m, Q, &max_1n, &one, C_householder, &max_1m);
	resid_norm = LAPACK_dlange ("1-norm", &m, &n, C_householder, &max_1m, rwork);

	if (C_norm > 0) {
		result[5] = resid_norm / (EPSILON * max_1m * C_norm);
	} else {
		result[5] = 0;
	}

	// Free remaining matrices
	free (A_factored);
	free (T);
	free (Q);

	free (C);
	free (C_householder);
	free (rwork);
}

volatile sig_atomic_t sigsegv_flag = 0;
sigjmp_buf mark;

void sigsegv_handler (int sig) {
	// Do not mark sig as unused
	(void)sig;

	sigsegv_flag = 1;
	siglongjmp (mark, 1);
}

int check_sigsegv_flag (char* routine_name, int param_index) {
	if (sigsegv_flag == 1) {
		fprintf (stderr, "Illegal memory access detected when parameter %d of %s has illegal value\n", param_index, routine_name);
		sigsegv_flag = 0;

		return 1;
	}

	return 0;
}

int test_pdgelqf_illegal_param (int m, int n, double* A, int lda, double* T, int ldt, config_t config, int illegal_param_index) {
	int failed = 0;

	if (sigsetjmp (mark, 1) == 0) {
		if (pdgelqf (m, n, A, lda, T, ldt, &config, NULL) != illegal_param_index) {
			fprintf (stderr, "Illegal value of parameter number %d not detected by PDGELQF\n", illegal_param_index);
			failed = 1;
		}
	}
	failed |= check_sigsegv_flag ("PDGELQF", illegal_param_index);

	return failed;
}

int test_pdgemlq_illegal_param (char side, char trans, int m, int n, int k, double* A, int lda, double* T, int ldt, double* C, int ldc, elim_context_t context, int illegal_param_index) {
	int failed = 0;

	if (sigsetjmp (mark, 1) == 0) {
		if (pdgemlq (side, trans, m, n, k, A, lda, T, ldt, C, ldc, context) != illegal_param_index) {
			fprintf (stderr, "Illegal value of parameter number %d not detected by PDGEMLQ\n", illegal_param_index);
			failed = 1;
		}
	}
	failed |= check_sigsegv_flag ("PDGEMLQ", illegal_param_index);

	return failed;
}

int test_pdgemlq_legal_param (char side, char trans, int m, int n, int k, double* A, int lda, double* T, int ldt, double* C, int ldc, elim_context_t context, int checked_param_index) {
	if (pdgemlq (side, trans, m, n, k, A, lda, T, ldt, C, ldc, context) == checked_param_index) {
		fprintf (stderr, "Illegal value of parameter number %d falsely detected by PDGEMLQ\n", checked_param_index);
		return 1;
	}

	return 0;
}

int test_pdgelqf_error_exits () {
	// Set used matrices & the QR context to NULL in order to ensure that they are not accessed
	double* A = NULL;
	double* T = NULL;

	// Attach custom signal handler to SIGSEGV in order to catch illegal memory accesses
	struct sigaction sigsegv_action;
	sigsegv_action.sa_handler = sigsegv_handler;
	sigfillset (&sigsegv_action.sa_mask);
	sigsegv_action.sa_flags = 0;

	sigaction (SIGSEGV, &sigsegv_action, NULL);

	int pdgelqf_error_exit_tests_failed = 0;

	// Create and initialize config
	config_t config;
	config = (config_t) {
		.nb = 1,
		.ib = 1,
		.superblock_size_factor = 1,
		.elim_scheme = TsFlatTree,
		.layout_translation = 0
	};

	// Test error exit 1 (m < 0)
	pdgelqf_error_exit_tests_failed |= test_pdgelqf_illegal_param (-1, 1, A, 1, T, 1, config, 1);

	// Test error exit 2 (n < 0)
	pdgelqf_error_exit_tests_failed |= test_pdgelqf_illegal_param (1, -1, A, 1, T, 1, config, 2);

	// Test error exit 4 (lda < max (1, m))
	pdgelqf_error_exit_tests_failed |= test_pdgelqf_illegal_param (2, 1, A, 1, T, 1, config, 4);

	// Test error exit 6 (ldt < max (1, ib))
	config.nb = config.ib = 2;
	pdgelqf_error_exit_tests_failed |= test_pdgelqf_illegal_param (2, 2, A, 2, T, 1, config, 6);
	config.nb = config.ib = 1;

	// Test error exit 7 (mb <= 0)
	config.nb = 0;
	pdgelqf_error_exit_tests_failed |= test_pdgelqf_illegal_param (1, 1, A, 1, T, 1, config, 7);
	config.nb = 1;

	// Test error exit 7 (ib <= 0, ib > mb)
	config.ib = 0;
	pdgelqf_error_exit_tests_failed |= test_pdgelqf_illegal_param (1, 1, A, 1, T, 1, config, 7);
	config.ib = 2;
	pdgelqf_error_exit_tests_failed |= test_pdgelqf_illegal_param (1, 1, A, 1, T, 2, config, 7);
	config.ib = 1;

	// Test error exit 7 (superblock_size_factor <= 0)
	config.superblock_size_factor = 0;
	pdgelqf_error_exit_tests_failed |= test_pdgelqf_illegal_param (1, 1, A, 1, T, 1, config, 7);
	config.superblock_size_factor = 1;

	// Test error exit 7 (elim_scheme < 0, elim_scheme >= NO_ELIM_SCHEMES)
	config.elim_scheme = -1;
	pdgelqf_error_exit_tests_failed |= test_pdgelqf_illegal_param (1, 1, A, 1, T, 1, config, 7);
	config.elim_scheme = NO_ELIM_SCHEMES;
	pdgelqf_error_exit_tests_failed |= test_pdgelqf_illegal_param (1, 1, A, 1, T, 1, config, 7);
	config.elim_scheme = TsFlatTree;

	// Test error exit 7 (layout_translation < 0, layout_translation > 1)
	config.layout_translation = -1;
	pdgelqf_error_exit_tests_failed |= test_pdgelqf_illegal_param (1, 1, A, 1, T, 1, config, 7);
	config.layout_translation = 2;
	pdgelqf_error_exit_tests_failed |= test_pdgelqf_illegal_param (1, 1, A, 1, T, 1, config, 7);
	config.layout_translation = 0;

	// Detach custom SIGSEGV handler
	sigsegv_action.sa_handler = SIG_DFL;
	sigaction (SIGSEGV, &sigsegv_action, NULL);

	return pdgelqf_error_exit_tests_failed;
}

int test_pdgemlq_error_exits () {
	// Set used matrices to NULL in order to ensure that they are not accessed
	double* A = NULL;
	double* T = NULL;
	double* C = NULL;

	// Attach custom signal handler to SIGSEGV in order to catch illegal memory accesses
	struct sigaction sigsegv_action;
	sigsegv_action.sa_handler = sigsegv_handler;
	sigfillset (&sigsegv_action.sa_mask);
	sigsegv_action.sa_flags = 0;

	sigaction (SIGSEGV, &sigsegv_action, NULL);

	int pdgemlq_error_exit_tests_failed = 0;

	// Create and initialize context
	elim_context_t context = {
		.config = {
			.nb = 1,
			.ib = 1,
			.superblock_size_factor = 1,
			.elim_scheme = TsFlatTree,
			.layout_translation = 0
		},
		.a_handles_height = 1,
		.a_handles_width = 1,
		.num_threads = 1
	};

	// Test error exit 1 (side is not one of 'L', 'l', 'T', 't')
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_legal_param ('l', 'N', 0, 0, 1, A, 1, T, 1, C, 1, context, 1);
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('/', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 1);

	// Test error exit 2 (trans is not one of 'N', 'n', 'T', 't')
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_legal_param ('L', 'n', 0, 0, 1, A, 1, T, 1, C, 1, context, 2);
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('L', '/', 1, 1, 1, A, 1, T, 1, C, 1, context, 2);

	// Test error exit 3 (m < 0)
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('L', 'N', -1, 1, 1, A, 1, T, 1, C, 1, context, 3);

	// Test error exit 4 (n < 0)
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('L', 'N', 1, -1, 1, A, 1, T, 1, C, 1, context, 4);

	// Test error exit 5 (k < 0, k > m if side = 'L', k > n if side = 'R')
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('L', 'N', 1, 1, -1, A, 1, T, 1, C, 1, context, 5);
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('L', 'N', 1, 2, 2, A, 2, T, 2, C, 1, context, 5);
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('R', 'N', 2, 1, 2, A, 2, T, 2, C, 2, context, 5);

	// Test error exit 7 (lda < max(1, k))
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('L', 'N', 2, 1, 2, A, 1, T, 2, C, 2, context, 7);

	// Test error exit 9 (ldt < max (1, ib))
	context.config.nb = context.config.ib = 2;
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('L', 'N', 2, 2, 2, A, 2, T, 1, C, 2, context, 9);
	context.config.nb = context.config.ib = 1;

	// Test error exit 11 (ldc < max (1, m))
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('L', 'N', 2, 1, 1, A, 2, T, 1, C, 1, context, 11);

	// Test error exit 12 (mb <= 0)
	context.config.nb = 0;
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.config.nb = 1;

	// Test error exit 7 (ib <= 0, ib > mb)
	context.config.ib = 0;
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.config.ib = 2;
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 2, C, 1, context, 12);
	context.config.ib = 1;

	// Test error exit 12 (superblock_size_factor <= 0)
	context.config.superblock_size_factor = 0;
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.config.superblock_size_factor = 1;

	// Test error exit 12 (elim_scheme < 0, elim_scheme >= NO_ELIM_SCHEMES)
	context.config.elim_scheme = -1;
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.config.elim_scheme = NO_ELIM_SCHEMES;
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.config.elim_scheme = TsFlatTree;

	// Test error exit 12 (layout_translation < 0, layout_translation > 1)
	context.config.layout_translation = -1;
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.config.layout_translation = 2;
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.config.layout_translation = 0;

	// Prepare for error exit 12 tests which depend on the elim_scheme being superblock-based
	context.config.elim_scheme = SuperblockGreedy;

	// Test error exit 12 (is_elim_scheme_superblock_based (elim_scheme) and a_handles_width < 0)
	context.a_handles_height = -1;
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.a_handles_height = 1;
	
	// Test error exit 12 (is_elim_scheme_superblock_based (elim_scheme) and a_handles_width < 0)
	context.a_handles_width = -1;
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.a_handles_width = 1;

	// Test error exit 12 (is_elim_scheme_superblock_based (elim_scheme) and num_threads <= 0)
	context.num_threads = 0;
	pdgemlq_error_exit_tests_failed |= test_pdgemlq_illegal_param ('L', 'N', 1, 1, 1, A, 1, T, 1, C, 1, context, 12);
	context.num_threads = 1;

	// Detach custom SIGSEGV handler
	sigsegv_action.sa_handler = SIG_DFL;
	sigaction (SIGSEGV, &sigsegv_action, NULL);

	return pdgemlq_error_exit_tests_failed;
}

int test_lq_routine_error_exits () {
	int error_exit_tests_failed = test_pdgelqf_error_exits () | test_pdgemlq_error_exits ();

	if (error_exit_tests_failed) {
		fprintf (stderr, "LQ routines failed the tests of the error exits\n");
	} else {
		printf ("LQ routines passed the tests of the error exits\n");
	}

	printf ("\n");

	return error_exit_tests_failed;
}

int lq_routines_test (double thresh, int no_m_values, int* m_values, int no_n_values, int* n_values, int no_mb_values, int* no_values, int no_elim_schemes, elim_scheme_e* elim_schemes, int test_layout_translation, int test_error_exits) {
	#if defined (USE_MKL)
	mkl_set_num_threads (1);
	#elif defined (USE_ARMPL)
	armpl_set_num_threads (1);
	#endif

	#if defined (USE_STARPU)
	if (starpu_init (NULL) != 0) {
		fprintf (stderr, "Error: StarPU could not be initialized\n");
		exit (1);
	}
	#endif

	int error_exits_result = 0;
	if (test_error_exits) {
		error_exits_result = test_lq_routine_error_exits ();
	}

	int no_tests_run = 0, no_tests_failed = 0;

	for (int m_index = 0; m_index < no_m_values; m_index++) {
		int m = m_values[m_index];

		for (int n_index = 0; n_index < no_n_values; n_index++) {
			int n = n_values[n_index];

			for (int mb_index = 0; mb_index < no_mb_values; mb_index++) {
				int mb = no_values[mb_index];

				if (mb == 0 || mb > min (m, n)) {
					continue;
				}

				for (int elim_scheme_index = 0; elim_scheme_index < no_elim_schemes; elim_scheme_index++) {
					int elim_scheme = elim_schemes[elim_scheme_index];

					for (int layout_translation = 0; layout_translation <= !!test_layout_translation; layout_translation++) {
						double result[NO_TESTS];

						config_t config = {
							.nb = mb,
							.ib = max (1, mb / 4), //TODO
							.superblock_size_factor = 4, //TODO
							.elim_scheme = elim_scheme,
							.layout_translation = layout_translation
						};

						perform_lq_routines_test (m, n, config, result);

						for (int test = 0; test < NO_TESTS; test++) {
							if (result[test] >= thresh) {
								no_tests_failed++;

								fprintf (stderr,
										"LQ routines test number %d failed for M=%d, N=%d, NB=%d, IB=%d, gamma=%d, elimination scheme %s, layout translation %s, test ratio is %lg\n",
										test + 1,
										m,
										n,
										config.nb,
										config.ib,
										config.superblock_size_factor,
										get_elim_scheme_name (config.elim_scheme),
										config.layout_translation ? "on" : "off",
										result[test]
								);
							}

							no_tests_run++;
						}
					}
				}
			}
		}
	}

	#if defined (USE_STARPU)
	starpu_shutdown ();
	#endif

	if (no_tests_failed == 0) {
		printf ("All tests for LQ routines passed the threshold (%d calls)\n", no_tests_run);
		return 0 | error_exits_result;
	} else {
		fprintf (stderr, "%d out of %d tests for LQ routines failed to pass the threshold\n", no_tests_failed, no_tests_run);
		return 1;
	}
}

int main () {
	double thresh = 30;
	int no_m_values = 8;
	int m_values[] = {0, 1, 2, 5, 10, 20, 50, 100};
	int no_n_values = 8;
	int n_values[] = {0, 1, 2, 5, 10, 20, 50, 100};
	int no_mb_values = 6;
	int mb_values[] = {1, 2, 3, 7, 12, 33};
	int no_elim_schemes = NO_ELIM_SCHEMES;
	elim_scheme_e elim_schemes[] = {TsFlatTree, TtFlatTree, TtGreedy, TtBinaryTree, TtFibonacci, SuperblockGreedy, SuperblockBinaryTree, SuperblockFibonacci, AutoTree};

	int test_layout_translation = 1;
	int test_error_exits = 1;

	return lq_routines_test (
		thresh,
		no_m_values,
		m_values,
		no_n_values,
		n_values,
		no_mb_values,
		mb_values,
		no_elim_schemes,
		elim_schemes,
		test_layout_translation,
		test_error_exits
	);
}

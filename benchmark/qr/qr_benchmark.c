#include "../common/arg_parsing.h"
#include "../../src/qr/qr.h"
#include "../../src/common/math_utils.h"
#include "../../test/lapack/lapack.h"
#include "../../test/lapack/cblas_f77.h"

#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <time.h>
#include <argp.h>

#if defined (USE_MKL)
#include <mkl_lapacke.h>
#include <mkl_service.h>
#elif defined (USE_ARMPL)
#define LAPACK_COL_MAJOR 102
#endif

#define DEFAULT_MAT_SIZE_VALUE 1000
#define DEFAULT_ITERATIONS_VALUE 1
#define DEFAULT_NO_THREADS_VALUE -1

#define EPSILON __DBL_EPSILON__
#define TEST_THRESHOLD 30

#if defined (USE_MKL)
#define CUSTOM_IMPLEMENTATION_TEXT "Custom PDGEQRF + MKL BLAS"
#define LIBRARY_IMPLEMENTATION_TEXT "MKL DGEQRF"
#define PLASMA_IMPLEMENTATION_TEXT "PLASMA DGEQRF + MKL BLAS"
#elif defined (USE_ARMPL)
#define CUSTOM_IMPLEMENTATION_TEXT "Custom PDGEQRF + ArmPL BLAS"
#define LIBRARY_IMPLEMENTATION_TEXT "ArmPL DGEQRF"
#define PLASMA_IMPLEMENTATION_TEXT "PLASMA DGEQRF + ArmPL BLAS"
#else
#define CUSTOM_IMPLEMENTATION_TEXT "Custom PDGEQRF"
#endif

#if defined (USE_STARPU)
#include <starpu.h>
#elif defined (USE_OPENMP)
#include <omp.h>
#endif

#if defined (USE_PLASMA)
#include <plasma.h>
#endif

// Helper struct for argp.h representing command line options
struct argp_option options[] = {
	{.name = "no-rows", .key = 'm', .arg = "<Number of rows>", .flags = 0, .doc = "The number of rows of the matrix."},
	{.name = "no-columns", .key = 'n', .arg = "<Number of columns>", .flags = 0, .doc = "The number of columns of the matrix."},
	{.name = "block-size", .key = 'b', .arg = "<Block size>", .flags = 0, .doc = "The size (the number of rows and columns) of the blocks."},
	{.name = "inner-block-size", .key = 'I', .arg = "<Inner block size>", .flags = 0, .doc = "The size (the number of rows and columns) of the inner blocks."},
	{.name = "superblock-size-factor", .key= 'f', .arg = "<Superblock size factor>", .flags = 0, .doc = "The factor in the denominator of the formula used to compute the superblock size."},
	{.name = "elim-scheme", .key = 'e', .arg = "<Elimination scheme>", .flags = 0, .doc = "The elimination scheme used for the QR factorization."},
	{.name = "layout-translation", .key = 'L', .arg = NULL, .flags = 0, .doc = "Use matrices in blockwise layout during the factorization"},
  	{.name = "iterations", .key = 'i', .arg = "<Iterations>", .flags = 0, .doc = "The number of times the routine is run."},
	{.name = "threads", .key = 't', .arg = "<Number of threads>", .flags = 0, .doc = "The number of threads used by the custom implementation. The default value is determined by the runtime system."},
	{.name = "custom-only", .key = 'c', .arg = NULL, .flags = 0, .doc = "Only run the custom implementation and skip the library one."},
	{.name = "check-correctness", .key = 'C', .arg = NULL, .flags = 0, .doc = "Perform additional tests to check the correctness of the custom implementation."},
	{.name = "print-superblock-size", .key = 'p', .arg = NULL, .flags = 0, .doc = "Print the used superblock size for each matrix size if a superblock-based elimination scheme is used."},
	{0}
};

// Helper struct for argp.h storing parameter values
struct args_t {
    start_end_step_range_t m, n;
	int nb, ib, iterations, superblock_size_factor, threads;
	int layout_translation, custom_only, check_correctness, print_superblock_size;
	elim_scheme_e elim_scheme;
};

// Callback for argp.h used to parse command line arguments
error_t parse_opt(int key, char *arg, struct argp_state *state) {
	struct args_t *arguments = state->input;

    switch (key) {
        case 'm': arguments->m = parse_positive_start_end_step_range (arg, "<Number of rows>"); break;
		case 'n': arguments->n = parse_positive_start_end_step_range (arg, "<Number of columns>"); break;
		case 'b': arguments->nb = parse_positive_int_argument (arg, "<Block size>"); break;
		case 'I': arguments->ib = parse_positive_int_argument (arg, "<Inner block size>"); break;
		case 'f': arguments->superblock_size_factor = parse_positive_int_argument (arg, "<Superblock size factor>"); break;
		case 'e': arguments->elim_scheme = parse_elim_scheme_name (arg, "<Elimination scheme>"); break;
		case 'L': arguments->layout_translation = 1; break;
		case 'i': arguments->iterations = parse_positive_int_argument (arg, "<Iterations>"); break;
		case 't': arguments->threads = parse_positive_int_argument (arg, "<Number of threads>"); break;
		case 'c': arguments->custom_only = 1; break;
		case 'C': arguments->check_correctness = 1; break;
		case 'p': arguments->print_superblock_size = 1; break;
		case ARGP_KEY_ARG: argp_usage (state); break;
        default: return ARGP_ERR_UNKNOWN;
	}
    return 0;
}

// Helper struct for argp.h
struct argp argp = {
	.options = options, 
	.parser = parse_opt
};

// Calculate wall time from start and end timestamps
double get_wall_time (struct timespec start, struct timespec finish) {
	return finish.tv_sec - start.tv_sec + (finish.tv_nsec - start.tv_nsec) * 1e-9;
}

// Calculate Gflop/s from the matrix size and the wall time
double calculate_gflops (int m, int n, double wall_time) {
	double dm = m, dn = n;
	double floating_point_ops;
	if (m > n) {
		floating_point_ops = 2 * dm * dn * dn - 2.0/3.0 * dn * dn * dn + dm * dn + dn * dn + 28.0/6.0 * dn;
	} else {
		floating_point_ops = 2 * dn * dm * dm - 2.0/3.0 * dm * dm * dm + 3.0 * dn * dm - dm * dm + 28.0/6.0 * dm;
	}

	return floating_point_ops * 1e-9 / wall_time;
}

#if defined (USE_ARMPL) && defined (USE_STARPU)
void armpl_set_num_threads_helper (int* no_threads) {
	armpl_set_num_threads (*no_threads);
}
#endif

int main (int argc, char** argv) {
	// Set default parameter values
	struct args_t args;
	args.m = get_single_value_start_end_step_range (DEFAULT_MAT_SIZE_VALUE);
	args.n = get_single_value_start_end_step_range (DEFAULT_MAT_SIZE_VALUE);
	args.nb = DEFAULT_NB_VALUE;
	args.ib = DEFAULT_IB_VALUE;
	args.superblock_size_factor = DEFAULT_SUPERBLOCK_SIZE_FACTOR_VALUE;
	args.elim_scheme = DEFAULT_ELIM_SCHEME_VALUE;
	args.layout_translation = 0;
	args.iterations = DEFAULT_ITERATIONS_VALUE;
	args.threads = DEFAULT_NO_THREADS_VALUE;
	args.custom_only = 0;
	args.check_correctness = 0;
	args.print_superblock_size = 0;

	argp_parse (&argp, argc, argv, 0, 0, &args);

	int max_threads;
	#if defined (USE_STARPU)
	if (starpu_init (NULL) != 0) {
		fprintf (stderr, "Error: StarPU could not be initialized\n");
		exit (1);
	}
	max_threads = starpu_cpu_worker_get_count ();
	starpu_shutdown ();
	#elif defined (USE_OPENMP)
	max_threads = omp_get_max_threads ();
	#endif

	if (args.threads == DEFAULT_NO_THREADS_VALUE) {
		args.threads = max_threads;
	}

	printf ("QR decomposition benchmarking utility\n");
	
	if (args.m.start == args.m.end) {
		printf ("- used matrix height (m): %d\n", args.m.start);
	} else {
		printf ("- used matrix heights (m): %d - %d (step %d)\n", args.m.start, args.m.end, args.m.step);
	}

	if (args.n.start == args.n.end) {
		printf ("- used matrix width (n): %d\n", args.n.start);
	} else {
		printf ("- used matrix widths (n): %d - %d (step %d)\n", args.n.start, args.n.end, args.n.step);
	}

	printf ("- used block size: %d\n", args.nb);
	printf ("- used inner block size: %d\n", args.ib);
	printf ("- used superblock size factor: %d\n", args.superblock_size_factor);
	printf ("- used elimination scheme: %s\n", get_elim_scheme_name (args.elim_scheme));
	printf ("- layout translation %s\n", args.layout_translation ? "enabled" : "disabled");
	printf ("- used number of iterations: %d\n", args.iterations);
	printf ("- used number of threads: %d\n", args.threads);

	if (args.custom_only) {
		#if defined (USE_MKL) || defined (USE_ARMPL)
			printf ("- %s benchmarking disabled\n", LIBRARY_IMPLEMENTATION_TEXT);
		#endif

		#if defined (USE_PLASMA)
			printf ("- PLASMA DGEQRF benchmarking disabled\n");
		#endif
	}

	if (args.check_correctness) {
		printf ("- additional correctness testing enabled\n");
	}

	printf ("=====================================================\n\n");

	// Construct the config
	config_t config = {
		.nb = args.nb,
		.ib = args.ib,
		.superblock_size_factor = args.superblock_size_factor,
		.elim_scheme = args.elim_scheme,
		.layout_translation = args.layout_translation
	};

	int seed[4] = {1988, 1989, 1990, 1991};
	int distribution = 2;

	// Initialize PLASMA
	#if defined (USE_PLASMA)
	plasma_init ();
	#endif

	// Apply nb & ib parameter values to PLASMA
	#if defined (USE_PLASMA)
	plasma_set (PlasmaNb, args.nb);
	plasma_set (PlasmaIb, args.ib);
	#endif

	// Iterate through all of the matrix heights in the input range
	for (int m = args.m.start; m <= args.m.end; m += args.m.step) {
		
		// Iterate through all of the matrix widths in the input range
		for (int n = args.n.start; n <= args.n.end; n += args.n.step) {
			// Print superblock size if enabled
			if (args.print_superblock_size && is_elim_scheme_superblock_based (args.elim_scheme)) {
				RUNTIME_SYSTEM_INIT ();

				elim_context_t dummy_context = {
					.a_handles_height = (m - 1) / args.nb + 1,
					.a_handles_width = (n - 1) / args.nb + 1,
					.num_threads = RUNTIME_SYSTEM_GET_NO_THREADS (),
					.config = {
						.nb = args.nb,
						.ib = args.ib,
						.superblock_size_factor = args.superblock_size_factor,
						.elim_scheme = args.elim_scheme,
						.layout_translation = args.layout_translation
					}
				};
				printf ("Used superblock size for matrix %dx%d: %d\n", m, n, get_superblock_size (dummy_context));

				RUNTIME_SYSTEM_SHUTDOWN ();
			}

			double* mat = malloc (m * n * sizeof (double));
			
			double* Q = NULL;
			double* R = NULL;
			if (args.check_correctness) {
				Q = malloc (m * m * sizeof (double));
				R = malloc (m * max (m, n) * sizeof (double));

				if (Q == NULL || R == NULL) {
					free (mat);
					free (Q);
					free (R);

					fprintf (stderr, "Error: failed to allocate enough memory\n");
					exit (1);
				}
			}

			int min_mn = min (m, n);
			int no_blocks_y = (m - 1) / args.nb + 1;

			double* T = malloc (get_t_size_multiplier_for_elim_scheme (args.elim_scheme) * args.ib * no_blocks_y * min_mn * sizeof (double));

			if (mat == NULL || T == NULL) {
				free (mat);
				free (T);

				if (args.check_correctness) {
					free (Q);
					free (R);
				}
				fprintf (stderr, "Error: failed to allocate enough memory\n");
				exit (1);
			}

			for (int iter = 0; iter < args.iterations; iter++) {
				int current_seed[4];
				memcpy (current_seed, seed, 4 * sizeof (int));

				// Paralellize matrix generation
				#if defined (USE_MKL)
				mkl_set_num_threads (max_threads);
				#elif defined (USE_ARMPL)
				armpl_set_num_threads (max_threads);
				#endif

				for (int col = 0; col < n; col++) {
					LAPACK_dlarnv (&distribution, seed, &m, mat + col * m);
				}

				double A_norm = 0;
				double* rwork = NULL;
				if (args.check_correctness) {
					rwork = malloc (max (m, n) * sizeof (double));
					A_norm = LAPACK_dlange ("1-norm", &m, &n, mat, &m, rwork);
				}

				#if defined (USE_STARPU)
				struct starpu_conf conf;
				starpu_conf_init (&conf);
				conf.ncpus = args.threads;
				if (starpu_init (&conf) != 0) {
					free (mat);
					free (T);

					if (args.check_correctness) {
						free (Q);
						free (R);
						free (rwork);
					}
					fprintf (stderr, "Error: StarPU could not be initialized\n");
					exit (1);
				}
				#elif defined (USE_OPENMP)
				omp_set_num_threads (args.threads);
				#endif

				#if defined (USE_MKL)
				mkl_set_num_threads (1);
				#elif defined (USE_ARMPL) && defined (USE_STARPU)
				// As ARM performance libraries are parallelized using OpenMP, the number of threads count in OpenMP is kept separately
				// for each StarPU worker, so we have to tell StarPU to set the OpenMP thread count to 1 on each worker
				// Source: https://github.com/starpu-runtime/starpu/issues/8
				int one = 1;
				starpu_execute_on_each_worker ((void(*)(void *))armpl_set_num_threads_helper, &one, STARPU_CPU);
				#endif
				
				struct timespec start, finish;
				elim_context_t context;

				clock_gettime (CLOCK_MONOTONIC, &start);
				if (pdgeqrf (m, n, mat, m, T, args.ib, &config, &context) == -1) {
					free (mat);
					free (T);

					if (args.check_correctness) {
						free (Q);
						free (R);
						free (rwork);
					}
					fprintf (stderr, "Error: runtime system could not be initialized\n");
					exit (1);
				}
				clock_gettime (CLOCK_MONOTONIC, &finish);

				#if defined (USE_STARPU)
				starpu_shutdown ();
				#elif defined (USE_OPENMP)
				omp_set_num_threads (omp_get_max_threads ());
				#endif

				double wall_time = get_wall_time (start, finish);
				double gflops = calculate_gflops (m, n, wall_time);

				if (args.iterations == 1) {
					printf ("%s (%dx%d matrix): %lg s, %lg Gflop/s\n", CUSTOM_IMPLEMENTATION_TEXT, m, n, wall_time, gflops);
				} else {
					printf ("%s (%dx%d matrix, iteration %d): %lg s, %lg Gflop/s\n", CUSTOM_IMPLEMENTATION_TEXT, m, n, iter + 1, wall_time, gflops);
				}

				if (args.check_correctness) {
					double zero = 0, one = 1, minus_one = -1;
					LAPACK_dlaset ("Lower", &m, &n, &zero, &zero, R, &m);
					LAPACK_dlacpy ("Upper", &m, &n, mat, &m, R, &m);

					// Test 1: Implicit A reconstruction
					if (pdgemqr ('L', 'N', m, n, min (m, n), mat, m, T, args.ib, R, m, context) == -1) {
						free (mat);
						free (T);
						free (Q);
						free (R);
						free (rwork);

						fprintf (stderr, "Error: runtime system could not be initialized\n");
						exit (1);
					}

					memcpy (seed, current_seed, 4 * sizeof (int));
					int int_one = 1;
					for (int col = 0; col < n; col++) {
						LAPACK_dlarnv (&distribution, seed, &m, rwork);
						F77_daxpy (&m, &minus_one, rwork, &int_one, R + m * col, &int_one);
					}
					double resid_norm = LAPACK_dlange ("1-norm", &m, &n, R, &m, rwork);

					if (A_norm == 0) {
						printf ("- test 1 (implicit A reconstruction) passed with test ratio 0\n");
					} else {
						double test_1_ratio = resid_norm / (EPSILON * m * A_norm);

						if (test_1_ratio < TEST_THRESHOLD) {
							printf ("- test 1 (implicit A reconstruction) passed with test ratio %.3lg\n", test_1_ratio);
						} else {
							fprintf (stderr, "- test 1 (implicit A reconstruction) failed with test ratio %.3lg\n", test_1_ratio);
						}
					}

					// Test 2: explicit R reconstruction
					LAPACK_dlaset ("Lower", &m, &n, &zero, &zero, R, &m);
					LAPACK_dlacpy ("Upper", &m, &n, mat, &m, R, &m);

					LAPACK_dlaset ("Full", &m, &m, &zero, &one, Q, &m);
					if (pdgemqr ('R', 'N', m, m, min (m, n), mat, m, T, args.ib, Q, m, context) == -1) {
						free (mat);
						free (T);
						free (Q);
						free (R);
						free (rwork);

						fprintf (stderr, "Error: runtime system could not be initialized\n");
						exit (1);
					}

					memcpy (seed, current_seed, 4 * sizeof (int));

					for (int col = 0; col < n; col++) {
						LAPACK_dlarnv (&distribution, seed, &m, mat + col * m);
					}

					F77_dgemm ("Transposed", "Non-transposed", &m, &n, &m, &minus_one, Q, &m, mat, &m, &one, R, &m);
					resid_norm = LAPACK_dlange ("1-norm", &m, &n, R, &m, rwork);

					if (A_norm == 0) {
						printf ("- test 2 (explicit R reconstruction) passed with test ratio 0\n");
					} else {
						double test_2_ratio = resid_norm / (EPSILON * m * A_norm);

						if (test_2_ratio < TEST_THRESHOLD) {
							printf ("- test 2 (explicit R reconstruction) passed with test ratio %.3lg\n", test_2_ratio);
						} else {
							fprintf (stderr, "- test 2 (explicit R reconstruction) failed with test ratio %.3lg\n", test_2_ratio);
						}
					}

					// Test 3: orthogonality of Q
					LAPACK_dlaset ("Upper", &m, &m, &zero, &one, R, &m);
					F77_dsyrk ("Upper", "Transposed", &m, &m, &minus_one, Q, &m, &one, R, &m);
					resid_norm = LAPACK_dlansy ("1-norm", "Upper", &m, R, &m, rwork);

					double test_3_ratio = resid_norm / (EPSILON * m);
					if (test_3_ratio < TEST_THRESHOLD) {
						printf ("- test 3 (orthogonality of Q) passed with test ratio %.3lg\n", test_3_ratio);
					} else {
						fprintf (stderr, "- test 3 (orthogonality of Q) failed with test ratio %.3lg\n", test_3_ratio);
					}

					free (rwork);
				}

				// Paralellize matrix generation
				#if defined (USE_MKL)
				mkl_set_num_threads (max_threads);
				#elif defined (USE_ARMPL)
				armpl_set_num_threads (max_threads);
				#endif

				#if defined (USE_MKL)
				if (!args.custom_only) {
					memcpy (seed, current_seed, 4 * sizeof (int));

					for (int col = 0; col < n; col++) {
						LAPACK_dlarnv (&distribution, seed, &m, mat + col * m);
					}

					mkl_set_num_threads (max_threads);

					clock_gettime (CLOCK_MONOTONIC, &start);
					LAPACKE_dgeqrf (LAPACK_COL_MAJOR, m, n, mat, m, T);
					clock_gettime (CLOCK_MONOTONIC, &finish);

					wall_time = get_wall_time (start, finish);
					gflops = calculate_gflops (m, n, wall_time);

					if (args.iterations == 1) {
						printf ("%s (%dx%d matrix): %lg s, %lg Gflop/s\n", LIBRARY_IMPLEMENTATION_TEXT, m, n, wall_time, gflops);
					} else {
						printf ("%s (%dx%d matrix, iteration %d): %lg s, %lg Gflop/s\n", LIBRARY_IMPLEMENTATION_TEXT, m, n, iter + 1, wall_time, gflops);
					}
				}
				#elif defined (USE_ARMPL)
				if (!args.custom_only) {
					memcpy (seed, current_seed, 4 * sizeof (int));

					for (int col = 0; col < n; col++) {
						LAPACK_dlarnv (&distribution, seed, &m, mat + col * m);
					}

					armpl_set_num_threads (max_threads);

					clock_gettime (CLOCK_MONOTONIC, &start);
					LAPACKE_dgeqrf (LAPACK_COL_MAJOR, m, n, mat, m, T);
					clock_gettime (CLOCK_MONOTONIC, &finish);

					wall_time = get_wall_time (start, finish);
					gflops = calculate_gflops (m, n, wall_time);

					if (args.iterations == 1) {
						printf ("%s (%dx%d matrix): %lg s, %lg Gflop/s\n", LIBRARY_IMPLEMENTATION_TEXT, m, n, wall_time, gflops);
					} else {
						printf ("%s (%dx%d matrix, iteration %d): %lg s, %lg Gflop/s\n", LIBRARY_IMPLEMENTATION_TEXT, m, n, iter + 1, wall_time, gflops);
					}
				}
				#endif

				#if defined (USE_PLASMA)
				if (!args.custom_only) {
					memcpy (seed, current_seed, 4 * sizeof (int));

					for (int col = 0; col < n; col++) {
						LAPACK_dlarnv (&distribution, seed, &m, mat + col * m);
					}

					plasma_set (PlasmaHouseholderMode, PlasmaTreeHouseholder);

					#if defined (USE_MKL)
					mkl_set_num_threads (1);
					#else
					// Do not set the number of ArmPL threads to 1, as it internally calls omp_set_num_threads (1)
					#endif

					plasma_desc_t T_desc;

					clock_gettime (CLOCK_MONOTONIC, &start);
					plasma_dgeqrf (m, n, mat, m, &T_desc);
					clock_gettime (CLOCK_MONOTONIC, &finish);

					wall_time = get_wall_time (start, finish);
					gflops = calculate_gflops (m, n, wall_time);

					if (args.iterations == 1) {
						printf ("%s (%dx%d matrix): %lg s, %lg Gflop/s\n", PLASMA_IMPLEMENTATION_TEXT, m, n, wall_time, gflops);
					} else {
						printf ("%s (%dx%d matrix, iteration %d): %lg s, %lg Gflop/s\n", PLASMA_IMPLEMENTATION_TEXT, m, n, iter + 1, wall_time, gflops);
					}
				}
				#endif
			}
			free (mat);
			free (T);

			if (args.check_correctness) {
				free (Q);
				free (R);
			}
		}
	}

	// Finalize PLASMA
	#if defined (USE_PLASMA)
	plasma_finalize ();
	#endif

	return 0;
}
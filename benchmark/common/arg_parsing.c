#include "arg_parsing.h"

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>

int parse_positive_int_argument (char* arg, char* arg_name) {
	char* endptr;
	
	long long_val = strtol (arg, &endptr, 10);
	if (*endptr != '\0') {
		fprintf (stderr, "Error: %s is not an integer value\n", arg_name);
		exit (NOT_AN_INTEGER);
	}

	if (long_val <= 0 || long_val > INT_MAX) {
		fprintf (stderr, "Error: %s should be in range [1, %d]", arg_name, INT_MAX);
		exit (OUT_OF_RANGE);
	}

	return (int)long_val;
}

start_end_step_range_t get_single_value_start_end_step_range (int value) {
	start_end_step_range_t range;
	range.start = range.end = value;
	range.step = 1;

	return range;
}

int parse_positive_start_end_step_range_part (char* arg, char* arg_name, char* part_name) {
	char* endptr;
	
	long long_val = strtol (arg, &endptr, 10);
	if (*endptr != '\0') {
		fprintf (stderr, "Error: %s part of %s (%s) is not an integer value\n", part_name, arg_name, arg);
		exit (NOT_AN_INTEGER);
	}

	if (long_val <= 0 || long_val > INT_MAX) {
		fprintf (stderr, "Error: %s part of %s (%s) should be in range [0, %d]", part_name, arg_name, arg, INT_MAX);
		exit (OUT_OF_RANGE);
	}

	return (int)long_val;
}

start_end_step_range_t parse_positive_start_end_step_range (char* arg, char* arg_name) {

	// Obtain the number of parts delimited by ":" by counting subsequent calls to strtok() until it returns NULL
	char* arg_copy = (char*)malloc ((strlen (arg) + 1) * sizeof (char));
	strcpy (arg_copy, arg);

	long no_delimited_parts = 1;
	strtok (arg_copy, ":");
	while (strtok (NULL, ":")) {
		no_delimited_parts++;
	}

	if (no_delimited_parts == 1) {

		// If there is only 1 part, we parse a positive integer
		free (arg_copy);
		return get_single_value_start_end_step_range (parse_positive_int_argument (arg, arg_name));
	} else if (no_delimited_parts == 3) {

		// If there are 3 parts, we parse a start:end:step range of positive integers
		start_end_step_range_t range;

		char* start_part = strtok (arg, ":");
		range.start = parse_positive_start_end_step_range_part (start_part, arg_name, "start");

		char* end_part = strtok (NULL, ":");
		range.end = parse_positive_start_end_step_range_part (end_part, arg_name, "end");

		if (range.end < range.start) {
			free (arg_copy);

			fprintf (stderr, "Error: end part of %s (%d) may not be smaller than the start part (%d)", arg_name, range.start, range.end);
			exit (INVALID_START_END_STEP_RANGE);
		}

		char* step_part = strtok (NULL, ":");
		range.step = parse_positive_start_end_step_range_part (step_part, arg_name, "step");

		free (arg_copy);

		return range;
	} else {
		// If there is a different number of parts, we exit with an error
		free (arg_copy);
		
		fprintf (stderr, "Error: %s should be an integer value or a start:end:step range", arg_name);
		exit (INVALID_START_END_STEP_RANGE);
	}
}

elim_scheme_e parse_elim_scheme_name (char* arg, char* arg_name) {
	if (strcmp (arg, "TsFlatTree") == 0) {
		return TsFlatTree;
	} else if (strcmp (arg, "TtFlatTree") == 0) {
		return TtFlatTree;
	} else if (strcmp (arg, "TtGreedy") == 0) {
		return TtGreedy;
	} else if (strcmp (arg, "TtBinaryTree") == 0) {
		return TtBinaryTree;
	} else if (strcmp (arg, "TtFibonacci") == 0) {
		return TtFibonacci;
	} else if (strcmp (arg, "SuperblockGreedy") == 0) {
		return SuperblockGreedy;
	} else if (strcmp (arg, "SuperblockBinaryTree") == 0) {
		return SuperblockBinaryTree;
	} else if (strcmp (arg, "SuperblockFibonacci") == 0) {
		return SuperblockFibonacci;
	} else if (strcmp (arg, "AutoTree") == 0) {
		return AutoTree;
	}

	fprintf (stderr, "Error: %s should have one of the following values:\n- TsFlatTree\n- TtFlatTree\n- TtGreedy\n- TtBinaryTree\n- TtFibonacci\n- SuperblockGreedy\n- SuperblockBinaryTree\n- SuperblockFibonacci\n- AutoTree\n", arg_name);
	exit (INVALID_TRANSLATION_METHOD);
}
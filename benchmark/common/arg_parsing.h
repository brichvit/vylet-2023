#ifndef ARG_PARSING_H_
#define ARG_PARSING_H_

#define NOT_AN_INTEGER 1
#define OUT_OF_RANGE 2
#define INVALID_START_END_STEP_RANGE 3
#define INVALID_ARGUMENT_COMBINATION 4
#define STARPU_INITIALIZATION_ERROR 5
#define INVALID_TRANSLATION_METHOD 6

#include "../../src/common/elimination_schemes.h"

typedef struct {
	int start, end, step;
} start_end_step_range_t;

/**
 * @brief Parses a positive integer argument and exits with a corresponding code if the argument has a wrong format.
 * 
 * @param arg The argument value
 * @param arg_name The argument name to display in the error message
 * @return int The parsed positive integer value of the argument
 */
int parse_positive_int_argument (char* arg, char* arg_name);

/**
 * @brief Returns a start:end:step range containing a single value
 * 
 * @param value The (only) value included in the range
 * @return start_end_step_range_t The start:end:step range containg only @p value
 */
start_end_step_range_t get_single_value_start_end_step_range (int value);

/**
 * @brief Parses an argument that has one of the following formats:
 * - a positive integer
 * - a start:end:step range of positive integers
 * and exits with a corresponding code if the argument has a wrong format.
 * 
 * @param arg The argument value
 * @param arg_name The argument name to display in the error message
 * @return int The parsed value of the argument
 */
start_end_step_range_t parse_positive_start_end_step_range (char* arg, char* arg_name);

/**
 * @brief Parses an elimination scheme name argument and exits with a corresponding code if the argument has a wrong format.
 * 
 * @param arg The argument value
 * @param arg_name The argument name to display in the error message
 * @return int The parsed value of the argument
 */
elim_scheme_e parse_elim_scheme_name (char* arg, char* arg_name);

#endif /* ARG_PARSING_H_ */
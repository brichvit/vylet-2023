# Task-based implementation of QR factorization and SVD

This is a repository for the project **Task-based implementation of QR factorization and SVD**, which is a part of the [VýLeT 2023](https://fit.cvut.cz/cs/zivot-na-fit/aktualne/pravidelne-akce/11244-vyzkumne-leto-na-fit-vylet) research program on FIT CTU.

- Student: **Bc. Vít Břichňáč** ([brichvit@fit.cvut.cz](mailto:brichvit@fit.cvut.cz))
- Mentor/supervisor: **Ing. Jakub Šístek, Ph.D.** ([jakub.sistek@fit.cvut.cz](mailto:jakub.sistek@fit.cvut.cz))
